stages:
  - build
  - check
  - docker-package
  - deploy

image: bellsoft/liberica-openjdk-alpine:11.0.8

variables:
  CI_IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA
  HEROKU_IMAGE_TAG: $HEROKU_REGISTRY/$HEROKU_APP/$HEROKU_PROCESS_TYPE
  DOCKER_HOST: "tcp://docker:2375"
  DOCKER_DRIVER: overlay2

cache:
  paths:
    - .gradle/wrapper
    - .gradle/caches

before_script:
  - chmod +x gradlew
  - export GRADLE_USER_HOME=`pwd`/.gradle

Build:
  stage: build
  when: on_success
  script:
    - ./gradlew bootJar
  artifacts:
    paths:
      - 'build'
      - 'Dockerfile'
    expire_in: 2 weeks

Code quality check:
  stage: check
  script:
    - ./gradlew detekt

Unit tests:
  stage: check
  script:
    - ./gradlew test
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print "Total:", covered, "/", instructions,
      " instructions covered"; print "Total:", 100*covered/instructions, "% covered" }' build/reports/jacoco/test/jacocoTestReport.csv
  coverage: '/Total: (\d+\.+\d+\s{1}\%{1}\s{1})covered/'
  artifacts:
    reports:
      junit: 'build/test-results/test/TEST-*.xml'
    expire_in: 2 weeks

Integration tests:
  stage: check
  services:
    - docker:20.10.7-dind
  script:
    - ./gradlew integrationTest

Docker package:
  stage: docker-package
  image: docker:20.10.7
  services:
    - docker:20.10.7-dind
  when: on_success
  cache: { }
  variables:
    GIT_STRATEGY: none
  before_script:
    - docker login -u $CI_REGISTRY_USR -p $CI_REGISTRY_PWD $CI_REGISTRY
  script:
    - docker build -t $CI_IMAGE_TAG .
    - docker push $CI_IMAGE_TAG
  only:
    - master

Heroku Deploy:
  stage: deploy
  image: registry.gitlab.com/uaky/docker-images/heroku-cli-docker
  when: on_success
  services:
    - docker:20.10.7-dind
  cache: { }
  variables:
    GIT_STRATEGY: none
  before_script:
    - docker login -u $CI_REGISTRY_USR -p $CI_REGISTRY_PWD $CI_REGISTRY
    - docker login -u $HEROKU_REGISTRY_USR -p $HEROKU_API_KEY $HEROKU_REGISTRY
  script:
    - docker pull $CI_IMAGE_TAG
    - docker tag $CI_IMAGE_TAG $HEROKU_IMAGE_TAG
    - docker push $HEROKU_IMAGE_TAG
    - heroku container:release $HEROKU_PROCESS_TYPE -a $HEROKU_APP
  only:
    - master
