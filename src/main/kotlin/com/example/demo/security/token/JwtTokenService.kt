package com.example.demo.security.token

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import com.example.demo.config.JwtConfig
import com.example.demo.converter.toLegacyDate
import com.example.demo.converter.toLocalDateTime
import com.example.demo.security.data.TokenScope
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class JwtTokenService(
    private val jwtConfig: JwtConfig
) {

    fun createAccessToken(subject: String) = createToken(
        subject,
        TokenScope.ACCESS_TOKEN,
        jwtConfig.timeToLive
    )

    fun createRefreshToken(subject: String) = createToken(
        subject,
        TokenScope.REFRESH_TOKEN,
        jwtConfig.refreshTokenTimeToLive
    )

    fun extractSubject(token: String) = token.decodedJwt.subject

    fun extractScope(token: String) = TokenScope.valueOf(token.decodedJwt.getClaim("scope").asString())

    fun extractExpiredAt(token: String) = token.decodedJwt.expiresAt.toLocalDateTime()

    private fun createToken(subject: String, scope: TokenScope, timeToLive: Long): String {
        val now = LocalDateTime.now()
        return JWT.create()
            .withSubject(subject)
            .withIssuedAt(now.toLegacyDate())
            .withExpiresAt(now.plusSeconds(timeToLive).toLegacyDate())
            .withClaim("scope", scope.name)
            .sign(Algorithm.HMAC512(jwtConfig.secret))
    }

    private val String.decodedJwt: DecodedJWT
        get() = JWT.require(Algorithm.HMAC512(jwtConfig.secret))
            .build()
            .verify(this)

}