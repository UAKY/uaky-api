package com.example.demo.security.token

import com.example.demo.security.authentication.AuthenticationTokenProvider
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtTokenFilter(
    private val authenticationTokenProvider: AuthenticationTokenProvider,
    private val tokenValidator: AccessTokenValidator
) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val jwtToken = request.resolveJwtToken()

        if (tokenValidator.isValid(jwtToken)) {
            authenticationTokenProvider.getAuthentication(jwtToken)
                ?.let { SecurityContextHolder.getContext().authentication = it }
        }

        filterChain.doFilter(request, response)
    }
}