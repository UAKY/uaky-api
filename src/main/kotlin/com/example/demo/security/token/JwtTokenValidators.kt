package com.example.demo.security.token

import com.auth0.jwt.exceptions.JWTVerificationException
import com.example.demo.security.data.TokenScope
import com.example.demo.service.UserService
import org.springframework.stereotype.Component
import java.time.LocalDateTime

interface TokenValidator {
    fun isValid(token: String): Boolean
}

abstract class AbstractTokenValidator(
    protected val jwtTokenService: JwtTokenService
) : TokenValidator {

    override fun isValid(token: String): Boolean = try {
        token.isNotBlank() && isNotExpired(token) && validate(token)
    } catch (expected: JWTVerificationException) {
        false
    }

    protected abstract fun validate(token: String): Boolean

    private fun isNotExpired(s: String) = jwtTokenService.extractExpiredAt(s).isAfter(LocalDateTime.now())

}

@Component
class AccessTokenValidator(jwtTokenService: JwtTokenService) : AbstractTokenValidator(jwtTokenService) {
    override fun validate(token: String) = jwtTokenService.extractScope(token) == TokenScope.ACCESS_TOKEN
}

@Component
class RefreshTokenValidator(
    private val userService: UserService,
    jwtTokenService: JwtTokenService
) : AbstractTokenValidator(jwtTokenService) {

    override fun validate(token: String) = token.hasValidScope() && userOwnsToken(token)

    private fun userOwnsToken(token: String): Boolean {
        val user = userService.findUserByEmail(jwtTokenService.extractSubject(token))
        return (token in user.refreshTokens)
    }

    private fun String.hasValidScope() = jwtTokenService.extractScope(this) == TokenScope.REFRESH_TOKEN
}