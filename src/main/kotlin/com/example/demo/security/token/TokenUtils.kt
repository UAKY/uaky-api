package com.example.demo.security.token

import com.example.demo.constant.AUTH_HEADER
import com.example.demo.constant.AUTH_HEADER_PREFIX
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import javax.servlet.http.HttpServletRequest

fun HttpServletRequest.resolveJwtToken() = getHeader(AUTH_HEADER)
    ?.resolveJwtToken()
    .orEmpty()

fun String.resolveJwtToken() = substringAfter(AUTH_HEADER_PREFIX, missingDelimiterValue = "")

fun StompHeaderAccessor.resolveJwtToken() = getFirstNativeHeader(AUTH_HEADER)
    ?.resolveJwtToken()
    .orEmpty()
