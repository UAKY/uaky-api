package com.example.demo.security.authentication

import com.example.demo.dto.LoginRequestDto
import com.example.demo.dto.LoginResponseDto
import com.example.demo.exception.JwtAuthenticationException
import com.example.demo.security.token.JwtTokenService
import com.example.demo.security.token.RefreshTokenValidator
import com.example.demo.security.token.resolveJwtToken
import com.example.demo.service.UserService
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class AuthService(
    private val authenticationManager: AuthenticationManager,
    private val jwtTokenService: JwtTokenService,
    private val userService: UserService,
    private val tokenValidator: RefreshTokenValidator
) {

    fun authenticate(loginRequestDto: LoginRequestDto): LoginResponseDto {
        val (email, password) = loginRequestDto
        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(email, password))

        return createTokens(email).apply {
            userService.findUserByEmail(email).addRefreshToken(refreshToken)
        }
    }

    fun refreshTokens(authHeader: String): LoginResponseDto {
        val token = authHeader.resolveJwtToken()

        if (!tokenValidator.isValid(token)) {
            throw JwtAuthenticationException()
        }

        val email = jwtTokenService.extractSubject(token)

        return createTokens(email).apply {
            userService.findUserByEmail(email).also { user ->
                user.removeRefreshToken(token)
                user.addRefreshToken(refreshToken)
            }
        }
    }

    private fun createTokens(email: String): LoginResponseDto {
        val accessToken = jwtTokenService.createAccessToken(email)
        val refreshToken = jwtTokenService.createRefreshToken(email)

        return LoginResponseDto(accessToken, refreshToken)
    }
}