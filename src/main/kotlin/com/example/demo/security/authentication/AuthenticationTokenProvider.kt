package com.example.demo.security.authentication

import com.example.demo.security.token.JwtTokenService
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component

@Component
class AuthenticationTokenProvider(
    private val userDetailsService: UserDetailsService,
    private val jwtTokenService: JwtTokenService
) {

    fun getAuthentication(token: String): Authentication? {
        val email = jwtTokenService.extractSubject(token)
        val userDetails = userDetailsService.loadUserByUsername(email)
        return UsernamePasswordAuthenticationToken(userDetails, "", userDetails.authorities)
    }

}