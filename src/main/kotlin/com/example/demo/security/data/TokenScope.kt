package com.example.demo.security.data

enum class TokenScope {
    ACCESS_TOKEN,
    REFRESH_TOKEN,
}