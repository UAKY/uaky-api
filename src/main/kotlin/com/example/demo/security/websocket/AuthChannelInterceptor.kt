package com.example.demo.security.websocket

import com.example.demo.exception.JwtAuthenticationException
import com.example.demo.security.authentication.AuthenticationTokenProvider
import com.example.demo.security.data.JwtUser
import com.example.demo.security.token.AccessTokenValidator
import com.example.demo.security.token.resolveJwtToken
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.messaging.support.ChannelInterceptor
import org.springframework.messaging.support.MessageHeaderAccessor
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class AuthChannelInterceptor(
    private val authenticationTokenProvider: AuthenticationTokenProvider,
    private val tokenValidator: AccessTokenValidator
) : ChannelInterceptor {

    override fun preSend(message: Message<*>, channel: MessageChannel): Message<*> {
        val accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor::class.java)!!

        when (accessor.command) {
            StompCommand.CONNECT -> handleConnect(accessor)
            StompCommand.SUBSCRIBE -> handleSubscribe(accessor)
            else -> {}
        }

        return message
    }

    private fun handleConnect(accessor: StompHeaderAccessor) {
        val jwtToken = accessor.resolveJwtToken()

        if (tokenValidator.isValid(jwtToken).not()) {
            throw JwtAuthenticationException()
        }
        authenticationTokenProvider.getAuthentication(jwtToken)?.let {
            SecurityContextHolder.getContext().authentication = it
            accessor.user = it
        }
    }

    private fun handleSubscribe(accessor: StompHeaderAccessor) {
        val currentUserId = (SecurityContextHolder.getContext().authentication.principal as JwtUser).id
        val subscribeUserId = accessor.getHeader("simpDestination")?.toString()?.split("/")?.get(2)

        if (currentUserId != subscribeUserId) {
            throw JwtAuthenticationException()
        }
    }
}
