package com.example.demo.security.service

import com.example.demo.constant.USER_NOT_FOUND_MSG
import com.example.demo.mapper.toJwtUser
import com.example.demo.persistence.repository.UserRepository
import com.example.demo.security.data.JwtUser
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserDetailsServiceImpl(
    private val userRepository: UserRepository
) : UserDetailsService {

    override fun loadUserByUsername(username: String): JwtUser {
        val user = userRepository.findByEmail(username) ?: throw UsernameNotFoundException(USER_NOT_FOUND_MSG)
        return user.toJwtUser()
    }

}
