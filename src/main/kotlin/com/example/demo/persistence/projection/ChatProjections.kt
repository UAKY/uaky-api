package com.example.demo.persistence.projection

import com.example.demo.persistence.entity.MessageStatus
import java.time.LocalDateTime

interface ChatMessageListProjection {
    val id: String
    val text: String
    val messageStatus: MessageStatus
    val createdDate: LocalDateTime
    val userFromId: String
}

interface LastChatMessageProjection {
    val chatId: String
    val chatTitle: String
    val chatImage: String
    val messageText: String
    val messageCreatedDate: LocalDateTime
    val lastMessageUnread: Boolean
}

interface UnreadMessagesCountProjection {
    val chatId: String
    val unreadMessagesCount: Int
}
