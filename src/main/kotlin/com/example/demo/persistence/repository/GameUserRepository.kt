package com.example.demo.persistence.repository

import com.example.demo.constant.LIKES_FROM_USERS
import com.example.demo.persistence.entity.GameUser
import com.example.demo.persistence.entity.GameUserId
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository

interface GameUserRepository : JpaRepository<GameUser, GameUserId> {

    // @EntityGraph(VIEWED_USERS_COMMON)
    fun findViewsByUserIdAndGameId(userId: String, gameId: String): GameUser?

    @EntityGraph(LIKES_FROM_USERS)
    fun findLikesByUserIdAndGameId(userId: String, gameId: String): GameUser?

    @EntityGraph(LIKES_FROM_USERS)
    fun findLikesByUserId(userId: String): List<GameUser>

    fun deleteByUserIdAndGameId(userId: String, gameId: String)

    fun existsByUserIdAndGameId(userId: String, gameId: String): Boolean

    fun findByUserIdAndGameId(userId: String, gameId: String): GameUser?
}
