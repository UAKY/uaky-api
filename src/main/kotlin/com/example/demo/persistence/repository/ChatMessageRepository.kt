package com.example.demo.persistence.repository

import com.example.demo.persistence.entity.Chat
import com.example.demo.persistence.entity.ChatMessage
import com.example.demo.persistence.entity.MessageStatus
import com.example.demo.persistence.entity.MessageStatus.VIEWED
import com.example.demo.persistence.projection.ChatMessageListProjection
import com.example.demo.persistence.projection.LastChatMessageProjection
import com.example.demo.persistence.projection.UnreadMessagesCountProjection
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query

interface ChatMessageRepository : JpaRepository<ChatMessage, String> {

    @Query(
        """
        SELECT
          cm.chat.id AS chatId,
          SUM(CASE WHEN cm.messageStatus <> :messageStatus AND cm.userFrom.id <> :userId THEN 1 ELSE 0 END) AS unreadMessagesCount
        FROM ChatMessage AS cm
        JOIN cm.chat.participants p
        WHERE p.id = :userId
        GROUP BY cm.chat.id
        """
    )
    fun findUnreadMessagesCountInChatsByUserId(
        userId: String,
        messageStatus: MessageStatus = VIEWED
    ): List<UnreadMessagesCountProjection>

    @Query(
        """
        SELECT 
          c1.id AS chatId, 
          p2.nickname AS chatTitle, 
          p2.image AS chatImage, 
          cm1.text AS messageText, 
          cm1.createdDate AS messageCreatedDate,
          (CASE WHEN cm1.messageStatus <> :messageStatus AND cm1.userFrom.id = :userId THEN true ELSE false END) AS lastMessageUnread
        FROM ChatMessage cm1
        JOIN cm1.chat c1
        JOIN c1.participants p1
        JOIN cm1.chat.participants p2
        WHERE p1.id = :userId
        AND p2.id <> :userId
        AND cm1.createdDate = (SELECT MAX(cm2.createdDate) FROM ChatMessage cm2 WHERE cm2.chat = c1)
        ORDER BY cm1.createdDate DESC 
        """
    )
    fun findLastMessagesChatsInUserId(
        userId: String,
        messageStatus: MessageStatus = VIEWED
    ): List<LastChatMessageProjection>

    @Query(
        """
        SELECT 
          cm1.id AS id,
          cm1.text AS text,
          cm1.messageStatus AS messageStatus,
          cm1.createdDate AS createdDate,
          cm1.userFrom.id AS userFromId
        FROM ChatMessage cm1
        WHERE cm1.createdDate < (SELECT cm2.createdDate FROM ChatMessage cm2 WHERE cm2.id = :lastMessageId)
        AND cm1.chat.id = :chatId
        ORDER BY cm1.createdDate DESC
        """
    )
    fun getChatMessagesAfterSpecificId(
        chatId: String,
        lastMessageId: String,
        pageable: Pageable
    ): List<ChatMessageListProjection>

    @Query(
        """
        SELECT 
          cm1.id AS id,
          cm1.text AS text,
          cm1.messageStatus AS messageStatus,
          cm1.createdDate AS createdDate,
          cm1.userFrom.id AS userFromId
        FROM ChatMessage cm1
        WHERE cm1.chat.id = :chatId
        ORDER BY cm1.createdDate DESC
        """
    )
    fun getChatMessages(
        chatId: String,
        pageable: Pageable
    ): List<ChatMessageListProjection>

    fun findAllByIdInAndChat(messageIds: List<String>, chat: Chat): List<ChatMessage>

    @Modifying
    @Query(
        """
        UPDATE ChatMessage 
        SET messageStatus = :messageStatus
        WHERE chat = :chat
        AND userFrom.id = :userFromId
        AND id IN :messageIds
        """
    )
    fun markMessagesAsViewed(
        userFromId: String,
        chat: Chat,
        messageIds: List<String>,
        messageStatus: MessageStatus = VIEWED
    )
}
