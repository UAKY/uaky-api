package com.example.demo.persistence.repository

import com.example.demo.constant.USER_ONLINE_MINUTES
import com.example.demo.persistence.entity.User
import org.hibernate.annotations.QueryHints.PASS_DISTINCT_THROUGH
import org.hibernate.annotations.QueryHints.READ_ONLY
import org.hibernate.jpa.QueryHints.HINT_CACHEABLE
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.jpa.repository.QueryHints
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import java.time.LocalDateTime
import javax.persistence.QueryHint

interface UserRepository : JpaRepository<User, String>, PagingAndSortingRepository<User, String> {

    @Query(
        """
        SELECT DISTINCT u FROM User u
        JOIN FETCH u.games gu
        JOIN FETCH gu.id.game
        WHERE u.id = :id
        """
    )
    @QueryHints(QueryHint(name = HINT_CACHEABLE, value = "true"))
    fun findByIdWithGames(@Param("id") id: String): User?

    @QueryHints(QueryHint(name = HINT_CACHEABLE, value = "true"))
    fun findByEmail(email: String): User?

    fun existsByEmail(email: String): Boolean

    @Query(
        """
        SELECT u.id FROM User u
        JOIN u.games gu
        WHERE gu.gameId = :gameId
        AND u.id NOT IN :skipUserIds
        ORDER BY RANDOM()
        """
    )
    fun findRandomUserIds(
        @Param("gameId") gameId: String,
        @Param("skipUserIds") skipUserIds: Collection<String?>,
        pageable: Pageable
    ): List<String>

    @Query(
        """
        SELECT u.id FROM User u
        JOIN u.games gu
        WHERE gu.gameId = :gameId
        AND u.id NOT IN :skipUserIds
        AND u.lastVisitedDate > :allowableDate
        ORDER BY RANDOM()
        """
    )
    fun findRandomOnlineUserIds(
        @Param("gameId") gameId: String,
        @Param("skipUserIds") skipUserIds: Collection<String?>,
        pageable: Pageable,
        @Param("allowableDate") allowableDate: LocalDateTime = LocalDateTime.now().minusMinutes(USER_ONLINE_MINUTES)
    ): List<String>

    @QueryHints(
        QueryHint(name = READ_ONLY, value = "true"),
        QueryHint(name = PASS_DISTINCT_THROUGH, value = "false")
    )
    @Query(
        """
        SELECT DISTINCT u FROM User u
        JOIN FETCH u.games gu
        JOIN FETCH gu.id.game
        WHERE u.id in :ids 
        """
    )
    fun findAllByIds(ids: Iterable<String>): List<User>

}
