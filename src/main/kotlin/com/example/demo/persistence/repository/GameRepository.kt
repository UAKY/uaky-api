package com.example.demo.persistence.repository

import com.example.demo.persistence.entity.Game
import org.hibernate.jpa.QueryHints.HINT_CACHEABLE
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.QueryHints
import javax.persistence.QueryHint

interface GameRepository : JpaRepository<Game, String> {

    @QueryHints(QueryHint(name = HINT_CACHEABLE, value = "true"))
    override fun findAll(): List<Game>

}