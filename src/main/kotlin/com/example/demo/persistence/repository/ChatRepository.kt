package com.example.demo.persistence.repository

import com.example.demo.persistence.entity.Chat
import com.example.demo.persistence.entity.User
import org.hibernate.jpa.QueryHints.HINT_CACHEABLE
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.jpa.repository.QueryHints
import javax.persistence.QueryHint

interface ChatRepository : JpaRepository<Chat, String> {

    @Query(
        """
        SELECT c
        FROM Chat c
        JOIN c.participants p1
        JOIN c.participants p2
        WHERE p1 = :userFrom
        AND p2 = :userTo
        """
    )
    @QueryHints(QueryHint(name = HINT_CACHEABLE, value = "true"))
    fun findByParticipants(userFrom: User, userTo: User): Chat?

    @Query(
        """
        SELECT c
        FROM Chat c
        JOIN FETCH c.participants p
        WHERE c.id = :chatId
        AND p.id = :userId
        """
    )
    @QueryHints(QueryHint(name = HINT_CACHEABLE, value = "true"))
    fun findByIdAndParticipantId(chatId: String, userId: String): Chat?

    @Query(
        """
        SELECT (CASE WHEN COUNT(c) = 1 THEN true ELSE false END)
        FROM Chat c
        JOIN c.participants p
        WHERE c.id = :chatId
        AND p.id = :userId
        """
    )
    fun existsByChatIdAndParticipantId(chatId: String, userId: String): Boolean

}
