package com.example.demo.persistence.repository

import com.example.demo.persistence.entity.Notification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.time.LocalDateTime

interface NotificationRepository : JpaRepository<Notification, String> {

    @Query("SELECT n.event FROM Notification n WHERE n.userTo.id = :userId AND n.createdDate > :date")
    fun findAllEventsByUserAndCreatedDateAfter(userId: String, date: LocalDateTime): List<Any>

}
