package com.example.demo.persistence.entity

import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.Immutable
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "games")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Immutable
class Game(
    val title: String,
    val icon: String,
    val imageSmall: String,
    val imageLarge: String,

    @OneToMany(mappedBy = "pk.game")
    val users: MutableList<GameUser> = mutableListOf()
) : BaseEntity() {

    constructor(
        id: String,
        title: String = "",
        icon: String = "",
        imageSmall: String = "",
        imageLarge: String = "",
    ) : this(
        title = title,
        icon = icon,
        imageSmall = imageSmall,
        imageLarge = imageLarge
    ) {
        this.id = id
    }
}
