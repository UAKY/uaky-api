package com.example.demo.persistence.entity

import com.example.demo.constant.LIKES_FROM_USERS
import javax.persistence.Column
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.NamedAttributeNode
import javax.persistence.NamedEntityGraph
import javax.persistence.NamedEntityGraphs
import javax.persistence.Table

@Entity
@Table(name = "game_user")
@NamedEntityGraphs(
    value = [
//        NamedEntityGraph(
//            name = VIEWED_USERS_COMMON,
//            attributeNodes = [
//                NamedAttributeNode("viewedUsersCommon"),
//            ],
//        ),
        NamedEntityGraph(
            name = LIKES_FROM_USERS,
            attributeNodes = [
                NamedAttributeNode("likesFromUsers"),
            ],
        ),
    ]
)
data class GameUser(

    @EmbeddedId
    val pk: GameUserId,

    var profileLink: String,

    var description: String?

) {

    @Column(name = "user_id", insertable = false, updatable = false)
    val userId: String = ""

    @Column(name = "game_id", insertable = false, updatable = false)
    val gameId: String = ""

    @ManyToMany
    @JoinTable(
        name = "user_views_common",
        joinColumns = [JoinColumn(name = "game_id"), JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "viewed_user_id")]
    )
    val viewedUsersCommon: MutableSet<User> = mutableSetOf()

    @ManyToMany
    @JoinTable(
        name = "user_views_online",
        joinColumns = [JoinColumn(name = "game_id"), JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "viewed_user_id")]
    )
    val viewedUsersOnline: MutableSet<User> = mutableSetOf()

    @ManyToMany
    @JoinTable(
        name = "user_likes",
        joinColumns = [JoinColumn(name = "game_id"), JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "user_from_id")]
    )
    val likesFromUsers: MutableSet<User> = mutableSetOf()

    constructor(
        profileLink: String,
        description: String?,
        user: User,
        game: Game
    ) : this(
        GameUserId(user, game),
        profileLink,
        description
    )

    fun addViewedUsersCommon(users: Collection<User>) {
        viewedUsersCommon.addAll(users)
    }

    fun clearViewedUsersCommon() {
        viewedUsersCommon.clear()
    }

    fun addViewedUsersOnline(users: Collection<User>) {
        viewedUsersOnline.addAll(users)
    }

    fun cleanViewedUsersOnline() {
        viewedUsersOnline.clear()
    }

    fun addLike(user: User) {
        likesFromUsers.add(user)
    }

}
