package com.example.demo.persistence.entity

import org.hibernate.annotations.DynamicUpdate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "messages")
@DynamicUpdate
@EntityListeners(AuditingEntityListener::class)
class ChatMessage(

    var text: String,

    @ManyToOne(fetch = FetchType.LAZY)
    val userFrom: User,

    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH], fetch = FetchType.LAZY)
    val chat: Chat,

    @Enumerated(EnumType.STRING)
    var messageStatus: MessageStatus = MessageStatus.DELIVERED,

    @CreatedDate
    var createdDate: LocalDateTime? = null

) : BaseEntity()

enum class MessageStatus {
    DELIVERED,
    VIEWED,
}
