package com.example.demo.persistence.entity

import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.Type
import java.time.LocalDateTime
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToMany
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "users")
@DynamicUpdate
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
class User(
    val email: String,
    var password: String,
    var nickname: String,

    @Column(name = "is_opened_status")
    var openedStatus: Boolean,

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    val roles: Set<Role>,

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    val refreshTokens: MutableSet<String> = mutableSetOf(),

    @OneToMany(
        mappedBy = "pk.user",
        cascade = [CascadeType.PERSIST, CascadeType.MERGE],
        orphanRemoval = true
    )
    val games: MutableList<GameUser> = mutableListOf(),

    var lastVisitedDate: LocalDateTime = LocalDateTime.now(),

    var image: String

) : BaseEntity() {

    @ManyToMany(mappedBy = "likesFromUsers")
    val likedUsers: MutableSet<GameUser> = mutableSetOf()

    fun addLikedUser(gameUser: GameUser) = likedUsers.add(gameUser)

    fun addRefreshToken(token: String) = refreshTokens.add(token)

    fun removeRefreshToken(token: String) = refreshTokens.remove(token)

    fun addGame(profileLink: String, description: String?, gameId: String) {
        val gameUser = GameUser(profileLink, description, this, Game(gameId))
        games.add(gameUser)
    }

    fun getGame(gameId: String) = games.first { it.pk.game.id == gameId }

}

enum class Role(
    val value: String
) {
    ROLE_USER(ROLE_USER_VALUE),
    ROLE_ADMIN(ROLE_ADMIN_VALUE);
}

const val ROLE_USER_VALUE = "ROLE_USER"
const val ROLE_ADMIN_VALUE = "ROLE_ADMIN"
