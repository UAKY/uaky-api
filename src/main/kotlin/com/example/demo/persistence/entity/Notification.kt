package com.example.demo.persistence.entity

import org.hibernate.annotations.Immutable
import org.hibernate.annotations.Type
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.FetchType
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "notifications")
@Immutable
@EntityListeners(AuditingEntityListener::class)
class Notification(

    @ManyToOne(fetch = FetchType.LAZY)
    val userTo: User,

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    val event: Any,

    @CreatedDate
    var createdDate: LocalDateTime? = null

) : BaseEntity()
