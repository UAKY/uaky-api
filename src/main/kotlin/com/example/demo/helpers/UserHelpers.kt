package com.example.demo.helpers

import com.example.demo.dto.GameUserCreateDto
import com.example.demo.persistence.entity.User

fun User.addGames(games: List<GameUserCreateDto>) {
    games.forEach {
        addGame(
            profileLink = it.profileLink,
            description = it.description,
            gameId = it.id
        )
    }
}

fun User.hasGame(id: String): Boolean {
    return games.any { it.pk.game.id == id }
}
