package com.example.demo.service

import com.example.demo.constant.DUPLICATE_GAME_ID
import com.example.demo.dto.GameUserCreateDto
import com.example.demo.dto.GameUserUpdateDto
import com.example.demo.exception.DuplicateEntryException
import com.example.demo.exception.GameNotFoundException
import com.example.demo.exception.UserNotFoundException
import com.example.demo.helpers.hasGame
import com.example.demo.persistence.entity.GameUser
import com.example.demo.persistence.entity.User
import com.example.demo.persistence.repository.GameUserRepository
import com.example.demo.persistence.repository.UserRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class GameUserService(
    private val userRepository: UserRepository,
    private val gameUserRepository: GameUserRepository
) {

    @Transactional
    fun addGame(id: String, gameUserCreateDto: GameUserCreateDto) {
        val user = findUserById(id)

        if (user.hasGame(gameUserCreateDto.id)) {
            throw DuplicateEntryException(DUPLICATE_GAME_ID)
        }

        user.addGame(
            gameUserCreateDto.profileLink,
            gameUserCreateDto.description,
            gameUserCreateDto.id
        )
    }

    @Transactional
    fun removeGame(id: String, gameId: String) {
        if (!gameUserRepository.existsByUserIdAndGameId(id, gameId)) {
            throw GameNotFoundException()
        }
        gameUserRepository.deleteByUserIdAndGameId(id, gameId)
    }

    @Transactional
    fun editGame(id: String, gameId: String, gameUserUpdateDto: GameUserUpdateDto) {
        gameUserRepository.findByUserIdAndGameId(id, gameId)
            ?.editInfo(gameUserUpdateDto)
            ?: throw GameNotFoundException()
    }

    private fun findUserById(id: String): User {
        return userRepository.findById(id).orElseThrow { UserNotFoundException() }
    }

    private fun GameUser.editInfo(gameUserUpdateDto: GameUserUpdateDto) {
        profileLink = gameUserUpdateDto.profileLink
        description = gameUserUpdateDto.description
    }
}
