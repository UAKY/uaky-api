package com.example.demo.service

import com.cloudinary.Cloudinary
import com.example.demo.constant.DEFAULT_USER_IMAGES
import com.example.demo.constant.enums.ImageExtension
import com.example.demo.constant.enums.UploadFolderName
import com.example.demo.dto.ImageUploadResponse
import com.example.demo.exception.InvalidImageFormatException
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class CloudinaryService(
    private val cloudinary: Cloudinary
) {

    fun upload(folder: UploadFolderName, file: MultipartFile): ImageUploadResponse {
        if (ImageExtension.isValid(file.getExtension()).not()) throw InvalidImageFormatException()

        val uploadResponse = cloudinary.uploader().upload(file.bytes, mapOf("folder" to folder.folderName))

        return ImageUploadResponse(uploadResponse["url"].toString())
    }

    @Cacheable(DEFAULT_USER_IMAGES)
    fun findAllDefaultUserImages(): List<String> {
        val response = cloudinary.search().expression("folder:users/defaults/*").execute()
        val resources = response.getValue("resources") as Iterable<Map<String, String>>
        return resources.map { it.getValue("url") }
    }

    private fun MultipartFile.getExtension() = originalFilename?.substringAfterLast(".")

}
