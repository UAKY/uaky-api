package com.example.demo.service

import com.example.demo.constant.ACTUAL_EVENTS_MAX_DAYS
import com.example.demo.dto.ListWrapperDto
import com.example.demo.dto.UserLikeEvent
import com.example.demo.mapper.toUserLikeDto
import com.example.demo.persistence.entity.Notification
import com.example.demo.persistence.repository.NotificationRepository
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
@Transactional(readOnly = true)
class LikeNotificationService(
    private val sseService: SseService,
    private val userService: UserService,
    private val notificationRepository: NotificationRepository,
) {

    @Async
    @Transactional
    fun notify(userToId: String, userFromId: String, gameId: String, isMutual: Boolean) {

        val userFrom = userService.findUserById(userFromId)
        val userTo = userService.findUserById(userToId)

        val userFromLikeDto = userFrom.toUserLikeDto(gameId, isMutual)
        val eventUserFrom = UserLikeEvent(userFromLikeDto)

        notificationRepository.save(Notification(userTo, eventUserFrom))
        sseService.sendEvent(userToId, eventUserFrom)

    }

    fun getNotifications(userId: String): ListWrapperDto<Any> {
        val actualEventsMaxDate = LocalDateTime.now().minusDays(ACTUAL_EVENTS_MAX_DAYS)
        val notifications = notificationRepository
            .findAllEventsByUserAndCreatedDateAfter(userId, actualEventsMaxDate)

        return ListWrapperDto(notifications)
    }

}
