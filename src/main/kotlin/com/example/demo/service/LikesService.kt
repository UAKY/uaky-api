package com.example.demo.service

import com.example.demo.dto.LikeResponseDto
import com.example.demo.dto.UserLikeDto
import com.example.demo.exception.GameNotFoundException
import com.example.demo.mapper.toUserLikeDtos
import com.example.demo.persistence.entity.GameUser
import com.example.demo.persistence.repository.GameUserRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class LikesService(
    private val gameUserRepository: GameUserRepository,
    private val userService: UserService,
    private val likeNotificationService: LikeNotificationService,
) {

    fun getUserLikes(userId: String): List<UserLikeDto> {
        return gameUserRepository.findLikesByUserId(userId)
            .map(GameUser::toUserLikeDtos)
            .flatten()
    }

    @Transactional
    fun addLikeToUser(userToId: String, userFromId: String, gameId: String): LikeResponseDto {
        val userFrom = userService.findUserById(userFromId)
        val userTo = userService.findUserById(userToId)

        gameUserRepository.findLikesByUserIdAndGameId(userToId, gameId)
            ?.addLike(userFrom)
            ?: throw GameNotFoundException()

        val isMutual = userFrom.getGame(gameId) in userTo.likedUsers

        likeNotificationService.notify(userToId, userFromId, gameId, isMutual)

        return LikeResponseDto(isMutual)
    }

}
