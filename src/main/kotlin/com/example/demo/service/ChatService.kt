package com.example.demo.service

import com.example.demo.constant.WsDestinations
import com.example.demo.constant.enums.SendStatus
import com.example.demo.dto.ChatMessageDto
import com.example.demo.dto.ChatProjectionMappingDto
import com.example.demo.dto.ChatResponseDto
import com.example.demo.dto.IncomingMessageDto
import com.example.demo.dto.OutgoingMessageDto
import com.example.demo.dto.PageResult
import com.example.demo.exception.ChatNotFoundException
import com.example.demo.mapper.toChatMessageDto
import com.example.demo.mapper.toChatResponseDto
import com.example.demo.mapper.toOutgoingMessageDto
import com.example.demo.persistence.entity.Chat
import com.example.demo.persistence.entity.ChatMessage
import com.example.demo.persistence.entity.User
import com.example.demo.persistence.repository.ChatMessageRepository
import com.example.demo.persistence.repository.ChatRepository
import org.springframework.data.domain.PageRequest
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class ChatService(
    private val messageSendingTemplate: SimpMessagingTemplate,
    private val userService: UserService,
    private val chatMessageRepository: ChatMessageRepository,
    private val chatRepository: ChatRepository
) {

    @Transactional
    fun handleWsMessage(wsIncomingMessage: IncomingMessageDto, userFromId: String) {
        val userFrom = userService.findUserById(userFromId)
        val userTo = userService.findUserById(wsIncomingMessage.userToId)

        val chat = chatRepository.findByParticipants(userFrom, userTo) ?: Chat(setOf(userFrom, userTo))

        val chatMessage = chatMessageRepository.save(
            ChatMessage(
                wsIncomingMessage.text,
                userFrom,
                chat
            )
        )

        val outgoingMessageDto = chatMessage.toOutgoingMessageDto(SendStatus.NEW)

        sendMessages(
            participants = listOf(userFrom, userTo),
            messages = listOf(outgoingMessageDto)
        )
    }

    fun getChatList(userId: String): List<ChatResponseDto> {
        val lastChatMessages = chatMessageRepository.findLastMessagesChatsInUserId(userId)
        val unreadMessagesCount = chatMessageRepository.findUnreadMessagesCountInChatsByUserId(userId)

        return ChatProjectionMappingDto(
            lastChatMessages,
            unreadMessagesCount
        ).toChatResponseDto()
    }

    fun getChatMessages(currentUserId: String, lastMessageId: String?, size: Int, chatId: String): PageResult<ChatMessageDto> {
        val chatExists = chatRepository.existsByChatIdAndParticipantId(chatId, currentUserId)
        if (chatExists.not()) throw ChatNotFoundException()

        val chatMessages = if (lastMessageId != null) {
            chatMessageRepository.getChatMessagesAfterSpecificId(chatId, lastMessageId, PageRequest.of(0, size))
        } else {
            chatMessageRepository.getChatMessages(chatId, PageRequest.of(0, size))
        }

        return PageResult(
            content = chatMessages.map { it.toChatMessageDto() },
            pageSize = chatMessages.count()
        )
    }

    @Transactional
    fun markMessagesAsViewed(currentUserId: String, chatId: String, messageIds: List<String>) {
        val chat = chatRepository.findByIdAndParticipantId(chatId, currentUserId)
            ?: throw ChatNotFoundException()

        chatMessageRepository.markMessagesAsViewed(currentUserId, chat, messageIds)

        val messages = chatMessageRepository
            .findAllByIdInAndChat(messageIds, chat)
            .map { it.toOutgoingMessageDto(SendStatus.UPDATED) }

        sendMessages(
            participants = chat.participants,
            messages = messages
        )
    }

    private fun sendMessages(
        participants: Collection<User>,
        messages: Collection<OutgoingMessageDto>
    ) {
        participants
            .map { it.id }
            .forEach { userId ->
                messages.forEach { messageDto ->
                    messageSendingTemplate.convertAndSendToUser(userId!!, WsDestinations.CHATS, messageDto)
                }
            }
    }

}
