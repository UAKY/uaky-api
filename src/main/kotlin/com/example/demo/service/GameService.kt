package com.example.demo.service

import com.example.demo.dto.GameResponseDto
import com.example.demo.exception.GameNotFoundException
import com.example.demo.mapper.toDto
import com.example.demo.persistence.repository.GameRepository
import org.springframework.stereotype.Service

@Service
class GameService(
    private val gameRepository: GameRepository
) {

    fun findAllGames(): List<GameResponseDto> = gameRepository.findAll().toDto()

    fun findGameById(id: String): GameResponseDto =
        gameRepository.findById(id).orElseThrow { GameNotFoundException() }.toDto()
}
