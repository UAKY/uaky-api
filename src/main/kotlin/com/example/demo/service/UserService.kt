package com.example.demo.service

import com.example.demo.dto.UserChangeImageRequestDto
import com.example.demo.dto.UserChangeNicknameRequestDto
import com.example.demo.dto.UserChangePasswordDto
import com.example.demo.dto.UserRegistrationDto
import com.example.demo.dto.UserResponseDto
import com.example.demo.exception.UserNotFoundException
import com.example.demo.helpers.addGames
import com.example.demo.mapper.toDto
import com.example.demo.persistence.entity.Role
import com.example.demo.persistence.entity.User
import com.example.demo.persistence.repository.UserRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
@Transactional(readOnly = true)
class UserService(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder,
    private val cloudinaryService: CloudinaryService
) {

    @Transactional
    fun register(userRegistrationDto: UserRegistrationDto) {
        val randomImage = cloudinaryService.findAllDefaultUserImages().random()

        val user = User(
            email = userRegistrationDto.email,
            password = passwordEncoder.encode(userRegistrationDto.password),
            roles = setOf(Role.ROLE_USER),
            nickname = userRegistrationDto.nickname,
            openedStatus = true,
            image = randomImage
        )

        user.addGames(userRegistrationDto.games)

        userRepository.save(user)
    }

    fun findById(id: String): UserResponseDto {
        val user = userRepository.findByIdWithGames(id) ?: throw UserNotFoundException()
        return user.toDto()
    }

    fun findUserByEmail(email: String): User {
        return userRepository.findByEmail(email) ?: throw UserNotFoundException()
    }

    @Transactional
    fun changeNickname(id: String, userChangeNicknameRequestDto: UserChangeNicknameRequestDto) {
        findUserById(id).nickname = userChangeNicknameRequestDto.nickname
    }

    @Transactional
    fun switchAccountStatus(id: String) {
        findUserById(id).apply { openedStatus = openedStatus.not() }
    }

    @Transactional
    fun changePassword(id: String, userChangePasswordDto: UserChangePasswordDto) {
        findUserById(id).password = passwordEncoder.encode(userChangePasswordDto.newPassword)
    }

    @Transactional
    fun updateLastVisitedDate(id: String) {
        findUserById(id).lastVisitedDate = LocalDateTime.now()
    }

    @Transactional
    fun changeImage(id: String, userChangeImageRequestDto: UserChangeImageRequestDto) {
        findUserById(id).image = userChangeImageRequestDto.image
    }

    fun findUserById(id: String): User {
        return userRepository.findById(id).orElseThrow(::UserNotFoundException)
    }

}
