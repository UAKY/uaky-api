package com.example.demo.service

import com.example.demo.dto.AbstractEvent
import com.example.demo.dto.PingEvent
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

interface SseService {

    fun createEmitter(userId: String): SseEmitter

    fun sendEvent(destinationUserId: String, event: AbstractEvent<*>)

}

@Service
class SseServiceImpl : SseService {

    private val emitters = ConcurrentHashMap<String, SseEmitter>()
    private val log = KotlinLogging.logger {}

    override fun createEmitter(userId: String): SseEmitter {
        log.info { "Creating emitter for $userId" }
        return emitters.computeIfAbsent(userId) {
            SseEmitter(TimeUnit.HOURS.toMillis(2)).apply {
                onCompletion {
                    emitters.remove(userId)
                    log.info { "Emitter for $userId completed" }
                }
                onError {
                    emitters.remove(userId)
                    log.error { "Emitter error for user $userId" }
                }
                onTimeout {
                    complete()
                    emitters.remove(userId)
                    log.warn { "Emitter timeout for user $userId" }
                }
            }.apply { sendEvent(PingEvent()) }
        }
    }

    override fun sendEvent(destinationUserId: String, event: AbstractEvent<*>) {
        log.info { "Sending event... destinationUserId: $destinationUserId; event: $event" }
        emitters[destinationUserId]?.sendEvent(event) ?: log.warn { "Emitter for $destinationUserId not found" }
    }

    @Scheduled(fixedRate = 3000) // each 3 seconds
    fun pingEmitters() {
        log.info { "Ping emitters..." }
        emitters.values.forEach { it.sendEvent(PingEvent()) }
    }

    private fun SseEmitter.sendEvent(event: AbstractEvent<*>) {
        send(event.toSseEventBuilder())
    }

}
