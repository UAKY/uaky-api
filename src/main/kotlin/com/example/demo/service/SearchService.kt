package com.example.demo.service

import com.example.demo.dto.PageResult
import com.example.demo.dto.UserSearchResultDto
import com.example.demo.exception.GameNotFoundException
import com.example.demo.mapper.toSearchResultDto
import com.example.demo.persistence.entity.GameUser
import com.example.demo.persistence.entity.User
import com.example.demo.persistence.repository.GameUserRepository
import com.example.demo.persistence.repository.UserRepository
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class SearchService(
    private val userRepository: UserRepository,
    private val gameUserRepository: GameUserRepository
) {

    @Transactional
    fun commonSearchByGameId(gameId: String, currentUserId: String, size: Int): PageResult<UserSearchResultDto> {
        return getUsersByGameId(
            gameId,
            currentUserId,
            size,
            userRepository::findRandomUserIds,
            GameUser::viewedUsersCommon,
            GameUser::addViewedUsersCommon
        )
    }

    @Transactional
    fun getOnlineUsersByGameId(gameId: String, currentUserId: String, size: Int): PageResult<UserSearchResultDto> {
        return getUsersByGameId(
            gameId,
            currentUserId,
            size,
            userRepository::findRandomOnlineUserIds,
            GameUser::viewedUsersOnline,
            GameUser::addViewedUsersOnline
        )
    }

    @Transactional
    fun clearViewsCommon(gameId: String, userId: String) {
        findGameUser(userId, gameId).clearViewedUsersCommon()
    }

    @Transactional
    fun clearViewsOnline(gameId: String, userId: String) {
        findGameUser(userId, gameId).cleanViewedUsersOnline()
    }

    private fun findGameUser(userId: String, gameId: String): GameUser {
        return gameUserRepository.findViewsByUserIdAndGameId(userId, gameId) ?: throw GameNotFoundException()
    }

    // todo: what about hardcode size? yes
    private fun getUsersByGameId(
        gameId: String,
        currentUserId: String,
        size: Int,
        getRandomUserIdsFunc: (String, Collection<String?>, Pageable) -> List<String>,
        getViewedUsersFunc: GameUser.() -> Collection<User>,
        addViewedUsersFunc: GameUser.(Collection<User>) -> Unit,
    ): PageResult<UserSearchResultDto> {
        val gameUser = findGameUser(currentUserId, gameId)

        val viewedUserIds = gameUser.getViewedUsersFunc().map(User::id)
        val likedUserIds = gameUser.pk.user.likedUsers.filter { it.gameId == gameId }.map(GameUser::userId)

        val skipUserIds = (viewedUserIds + likedUserIds + currentUserId).toSet()

        val randomIds = getRandomUserIdsFunc(gameId, skipUserIds, PageRequest.of(0, size))
        val randomUsers = userRepository.findAllByIds(randomIds)

        gameUser.addViewedUsersFunc(randomUsers)

        return PageResult(
            content = randomUsers.map { it.toSearchResultDto(gameId) }.shuffled(),
            pageSize = randomUsers.count()
        )
    }

}
