package com.example.demo.exception

import com.example.demo.constant.UNSUPPORTED_IMAGE_FORMAT_MSG

class InvalidImageFormatException : RuntimeException(UNSUPPORTED_IMAGE_FORMAT_MSG)
