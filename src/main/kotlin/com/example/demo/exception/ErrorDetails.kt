package com.example.demo.exception

import com.example.demo.constant.VALIDATION_FAILED_MSG
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import javax.validation.ConstraintViolation

open class ErrorDetails(
    val status: Int,
    val message: String?,
    val error: String
) {

    constructor(
        httpStatus: HttpStatus,
        message: String?
    ) : this(
        httpStatus.value(),
        message,
        httpStatus.reasonPhrase
    )

}

class ValidationErrorDetails(fieldErrors: List<FieldError>) : ErrorDetails(HttpStatus.UNPROCESSABLE_ENTITY, VALIDATION_FAILED_MSG) {

    val validationErrors = fieldErrors.groupBy({ it.field }, { it.defaultMessage })

    constructor(constraintViolations: Set<ConstraintViolation<*>>) : this(
        constraintViolations.map { violation ->
            val pathIterator = violation.propertyPath.iterator()
            FieldError(
                pathIterator.next().name,
                pathIterator.next().name,
                violation.message
            )
        }
    )

}

fun errorDetails(httpStatus: HttpStatus, message: String?) = ErrorDetails(httpStatus, message).responseEntity

fun badRequestErrorDetails(message: String?): ResponseEntity<ErrorDetails> = errorDetails(HttpStatus.BAD_REQUEST, message)

fun validationErrorDetails(fieldErrors: List<FieldError>) = ValidationErrorDetails(fieldErrors).responseEntity

fun validationErrorDetails(constraintViolations: Set<ConstraintViolation<*>>) =
    ValidationErrorDetails(constraintViolations).responseEntity

private val ErrorDetails.responseEntity: ResponseEntity<ErrorDetails>
    get() = ResponseEntity.status(status).body(this)