package com.example.demo.exception

import com.example.demo.constant.INVALID_TOKEN_MSG
import org.springframework.security.core.AuthenticationException

class JwtAuthenticationException : AuthenticationException(INVALID_TOKEN_MSG)