package com.example.demo.exception

class DuplicateEntryException(message: String) : RuntimeException(message)