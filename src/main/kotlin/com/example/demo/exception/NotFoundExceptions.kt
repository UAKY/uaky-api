package com.example.demo.exception

import com.example.demo.constant.CHAT_NOT_FOUND_MSG
import com.example.demo.constant.GAME_NOT_FOUND_MSG
import com.example.demo.constant.USER_NOT_FOUND_MSG

sealed class EntityNotFoundException(message: String) : RuntimeException(message)

class GameNotFoundException : EntityNotFoundException(GAME_NOT_FOUND_MSG)

class UserNotFoundException : EntityNotFoundException(USER_NOT_FOUND_MSG)

class ChatNotFoundException : EntityNotFoundException(CHAT_NOT_FOUND_MSG)
