package com.example.demo.validation

import com.example.demo.constant.INVALID_GAME_ID_MSG
import com.example.demo.persistence.repository.GameRepository
import org.springframework.stereotype.Component
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import kotlin.reflect.KClass

@MustBeDocumented
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [ValidGameIdValidator::class])
annotation class ValidGameId(

    val message: String = INVALID_GAME_ID_MSG,

    val groups: Array<KClass<out Any>> = [],

    val payload: Array<KClass<out Any>> = []
)

@Component
class ValidGameIdValidator(
    private val gameRepository: GameRepository
) : ConstraintValidator<ValidGameId, String?> {
    override fun isValid(id: String?, context: ConstraintValidatorContext?): Boolean {
        return if (id.isNullOrBlank()) {
            false
        } else gameRepository.existsById(id)
    }
}
