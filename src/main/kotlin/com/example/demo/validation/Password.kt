package com.example.demo.validation

import com.example.demo.constant.INVALID_PASSWORD
import javax.validation.Constraint
import javax.validation.constraints.Pattern
import kotlin.reflect.KClass

@MustBeDocumented
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}\$", message = INVALID_PASSWORD)
@Constraint(validatedBy = [])
annotation class Password(

    val message: String = INVALID_PASSWORD,

    val groups: Array<KClass<out Any>> = [],

    val payload: Array<KClass<out Any>> = []
)
