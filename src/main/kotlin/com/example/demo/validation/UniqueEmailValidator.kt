package com.example.demo.validation

import com.example.demo.constant.EMAIL_IS_TAKEN_MSG
import com.example.demo.persistence.repository.UserRepository
import org.springframework.stereotype.Component
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import kotlin.reflect.KClass

@MustBeDocumented
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [UniqueEmailValidator::class])
annotation class UniqueEmail(

    val message: String = EMAIL_IS_TAKEN_MSG,

    val groups: Array<KClass<out Any>> = [],

    val payload: Array<KClass<out Any>> = []
)

@Component
class UniqueEmailValidator(
    private val userRepository: UserRepository
) : ConstraintValidator<UniqueEmail, String?> {
    override fun isValid(email: String?, context: ConstraintValidatorContext?): Boolean {
        return if (email.isNullOrBlank()) {
            false
        } else !userRepository.existsByEmail(email)
    }
}