package com.example.demo.validation

import com.example.demo.constant.WRONG_PASSWORD_MSG
import com.example.demo.security.data.JwtUser
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import kotlin.reflect.KClass

@MustBeDocumented
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [PasswordOwnerValidator::class])
annotation class PasswordOwner(

    val message: String = WRONG_PASSWORD_MSG,

    val groups: Array<KClass<out Any>> = [],

    val payload: Array<KClass<out Any>> = []
)

@Component
class PasswordOwnerValidator(
    private val passwordEncoder: PasswordEncoder
) : ConstraintValidator<PasswordOwner, String> {
    override fun isValid(password: String, context: ConstraintValidatorContext?): Boolean {
        val user = SecurityContextHolder.getContext().authentication.principal as JwtUser
        return passwordEncoder.matches(password, user.password)
    }
}