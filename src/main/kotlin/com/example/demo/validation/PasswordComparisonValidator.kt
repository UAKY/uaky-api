package com.example.demo.validation

import com.example.demo.constant.PASSWORDS_NOT_EQUALS_MSG
import com.example.demo.dto.UserChangePasswordDto
import org.springframework.stereotype.Component
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import kotlin.reflect.KClass

@MustBeDocumented
@Target(AnnotationTarget.CLASS, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [PasswordComparisonValidator::class])
annotation class ComparePassword(

    val message: String = PASSWORDS_NOT_EQUALS_MSG,

    val groups: Array<KClass<out Any>> = [],

    val payload: Array<KClass<out Any>> = []
)

@Component
class PasswordComparisonValidator : ConstraintValidator<ComparePassword, UserChangePasswordDto> {
    override fun isValid(dto: UserChangePasswordDto, context: ConstraintValidatorContext): Boolean {
        context.buildConstraintViolationWithTemplate(context.defaultConstraintMessageTemplate)
            .addPropertyNode(UserChangePasswordDto::repeatedNewPassword.name)
            .addConstraintViolation()

        return dto.newPassword == dto.repeatedNewPassword
    }
}