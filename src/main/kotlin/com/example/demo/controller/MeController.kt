package com.example.demo.controller

import com.example.demo.constant.ACCOUNTS
import com.example.demo.dto.GameUserCreateDto
import com.example.demo.dto.GameUserUpdateDto
import com.example.demo.dto.UserChangeImageRequestDto
import com.example.demo.dto.UserChangeNicknameRequestDto
import com.example.demo.dto.UserChangePasswordDto
import com.example.demo.persistence.entity.ROLE_ADMIN_VALUE
import com.example.demo.persistence.entity.ROLE_USER_VALUE
import com.example.demo.security.data.JwtUser
import com.example.demo.service.GameUserService
import com.example.demo.service.UserService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed
import javax.validation.Valid

@Tag(name = "User Account API")
@RestController
@RequestMapping("v1/me")
class MeController(
    private val userService: UserService,
    private val gameUserService: GameUserService
) {

    @Cacheable(cacheNames = [ACCOUNTS], key = "#jwtUser.id")
    @Operation(summary = "Get current user's account info")
    @GetMapping
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun getMe(@AuthenticationPrincipal jwtUser: JwtUser) = userService.findById(jwtUser.id)

    @CacheEvict(cacheNames = [ACCOUNTS], key = "#jwtUser.id")
    @Operation(summary = "Change nickname")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping("nickname")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun changeNickname(
        @AuthenticationPrincipal jwtUser: JwtUser,
        @RequestBody @Valid userChangeNicknameRequestDto: UserChangeNicknameRequestDto
    ) = userService.changeNickname(jwtUser.id, userChangeNicknameRequestDto)

    @CacheEvict(cacheNames = [ACCOUNTS], key = "#jwtUser.id")
    @Operation(summary = "Add game to current user")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("games")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun addGameToUser(
        @AuthenticationPrincipal jwtUser: JwtUser,
        @RequestBody @Valid gameUserCreateDto: GameUserCreateDto
    ) = gameUserService.addGame(jwtUser.id, gameUserCreateDto)

    @CacheEvict(cacheNames = [ACCOUNTS], key = "#jwtUser.id")
    @Operation(summary = "Remove game from current user")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("games/{gameId}")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun removeGameFromUser(
        @AuthenticationPrincipal jwtUser: JwtUser,
        @PathVariable gameId: String
    ) = gameUserService.removeGame(jwtUser.id, gameId)

    @CacheEvict(cacheNames = [ACCOUNTS], key = "#jwtUser.id")
    @Operation(summary = "Update game in current user")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("games/{gameId}")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun updateGameInUser(

        @AuthenticationPrincipal jwtUser: JwtUser,

        @PathVariable gameId: String,

        @RequestBody @Valid gameUserUpdateDto: GameUserUpdateDto

    ) = gameUserService.editGame(jwtUser.id, gameId, gameUserUpdateDto)

    @CacheEvict(cacheNames = [ACCOUNTS], key = "#jwtUser.id")
    @Operation(summary = "Switch account status in current user")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping("status")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun switchAccountStatus(

        @AuthenticationPrincipal jwtUser: JwtUser

    ) = userService.switchAccountStatus(jwtUser.id)

    @Operation(summary = "Change account password in current user")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping("password")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun changeAccountPassword(

        @AuthenticationPrincipal jwtUser: JwtUser,

        @RequestBody @Valid userChangePasswordDto: UserChangePasswordDto

    ) = userService.changePassword(jwtUser.id, userChangePasswordDto)

    @Operation(summary = "Update last visited date")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping("last-visited-date")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun updateLastVisitedDate(
        @AuthenticationPrincipal jwtUser: JwtUser
    ) = userService.updateLastVisitedDate(jwtUser.id)

    @CacheEvict(cacheNames = [ACCOUNTS], key = "#jwtUser.id")
    @Operation(summary = "Change account image")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping("image")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun changeImage(
        @AuthenticationPrincipal jwtUser: JwtUser,
        @RequestBody @Valid userChangeImageRequestDto: UserChangeImageRequestDto
    ) = userService.changeImage(jwtUser.id, userChangeImageRequestDto)

}
