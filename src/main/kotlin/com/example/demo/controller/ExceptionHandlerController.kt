package com.example.demo.controller

import com.example.demo.constant.MAXIMUM_UPLOAD_SIZE_MSG
import com.example.demo.constant.VALUE_IS_INVALID
import com.example.demo.exception.DuplicateEntryException
import com.example.demo.exception.EntityNotFoundException
import com.example.demo.exception.ErrorDetails
import com.example.demo.exception.InvalidImageFormatException
import com.example.demo.exception.badRequestErrorDetails
import com.example.demo.exception.errorDetails
import com.example.demo.exception.validationErrorDetails
import mu.KotlinLogging
import org.springframework.core.convert.ConversionFailedException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import org.springframework.web.multipart.MaxUploadSizeExceededException
import javax.validation.ConstraintViolationException

@ControllerAdvice
class ExceptionHandlerController {

    private val log = KotlinLogging.logger {}

    @ExceptionHandler(EntityNotFoundException::class)
    fun entityNotFoundHandler(ex: EntityNotFoundException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return errorDetails(HttpStatus.NOT_FOUND, ex.message)
    }

    @ExceptionHandler(AuthenticationException::class)
    fun authenticationExceptionHandler(ex: AuthenticationException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return errorDetails(HttpStatus.UNAUTHORIZED, ex.message)
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun methodArgumentNotValidHandler(ex: MethodArgumentNotValidException): ResponseEntity<ErrorDetails> {
        return validationErrorDetails(ex.bindingResult.fieldErrors)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    fun methodArgumentNotValidHandler(ex: ConstraintViolationException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return validationErrorDetails(ex.constraintViolations)
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException::class)
    fun methodArgumentTypeMismatchHandler(ex: MethodArgumentTypeMismatchException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return badRequestErrorDetails(VALUE_IS_INVALID.format(ex.value))
    }

    @ExceptionHandler(ConversionFailedException::class)
    fun conversionFailedHandler(ex: ConversionFailedException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return badRequestErrorDetails(ex.cause?.message)
    }

    // all custom exceptions that returns 400 should be handled here
    @ExceptionHandler(InvalidImageFormatException::class)
    fun customBadRequestExceptionHandler(ex: RuntimeException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return badRequestErrorDetails(ex.message)
    }

    @ExceptionHandler(AccessDeniedException::class)
    fun accessDeniedHandler(ex: AccessDeniedException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return errorDetails(HttpStatus.FORBIDDEN, ex.message)
    }

    @ExceptionHandler(DuplicateEntryException::class)
    fun duplicateEntryHandler(ex: DuplicateEntryException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return errorDetails(HttpStatus.CONFLICT, ex.message)
    }

    @ExceptionHandler(MaxUploadSizeExceededException::class)
    fun maxUploadSizeExceededHandler(ex: MaxUploadSizeExceededException): ResponseEntity<ErrorDetails> {
        log.warn(ex.message, ex)
        return errorDetails(HttpStatus.PAYLOAD_TOO_LARGE, MAXIMUM_UPLOAD_SIZE_MSG)
    }

    // TODO: MissingServletRequestPartException
    // TODO: MultipartException
    // TODO: handle all spring's exception that should return 4xx

}
