package com.example.demo.controller

import com.example.demo.constant.AUTH_HEADER
import com.example.demo.dto.LoginRequestDto
import com.example.demo.security.authentication.AuthService
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Tag(name = "Auth API")
@RestController
@RequestMapping("v1/auth")
class AuthController(
    private val authService: AuthService
) {

    @PostMapping("login")
    fun login(@RequestBody @Valid loginRequestDto: LoginRequestDto) = authService.authenticate(loginRequestDto)

    @PostMapping("refresh")
    fun refreshToken(@Parameter(hidden = true) @RequestHeader(AUTH_HEADER) authHeader: String) = authService.refreshTokens(authHeader)

}