package com.example.demo.controller

import com.example.demo.dto.UserRegistrationDto
import com.example.demo.service.UserService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Tag(name = "Registration API")
@RestController
@RequestMapping("v1")
class RegistrationController(
    private val userService: UserService
) {

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("register")
    fun register(@RequestBody @Valid userRegistrationDto: UserRegistrationDto) = userService.register(userRegistrationDto)

}