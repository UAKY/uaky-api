package com.example.demo.controller

import com.example.demo.constant.MIN_SEARCH_USERS_REQUEST_SIZE
import com.example.demo.persistence.entity.ROLE_ADMIN_VALUE
import com.example.demo.persistence.entity.ROLE_USER_VALUE
import com.example.demo.security.data.JwtUser
import com.example.demo.service.SearchService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Tag(name = "Common search API")
@Validated
@RestController
@RequestMapping("v1/common-search")
class CommonSearchController(
    private val searchService: SearchService
) {

    @Operation(summary = "Get random users by game id")
    @GetMapping("{gameId}")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun commonSearchByGameId(

        @PathVariable
        gameId: String,

        @RequestParam(defaultValue = MIN_SEARCH_USERS_REQUEST_SIZE.toString())
        @Min(MIN_SEARCH_USERS_REQUEST_SIZE) @Max(100)
        size: Int,

        @AuthenticationPrincipal
        jwtUser: JwtUser

    ) = searchService.commonSearchByGameId(gameId, jwtUser.id, size)

    @Operation(summary = "Clear list of viewed users for game")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{gameId}/views")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun clearViews(

        @PathVariable
        gameId: String,

        @AuthenticationPrincipal
        jwtUser: JwtUser

    ) = searchService.clearViewsCommon(gameId, jwtUser.id)

}
