package com.example.demo.controller

import com.example.demo.dto.ListWrapperDto
import com.example.demo.persistence.entity.ROLE_ADMIN_VALUE
import com.example.demo.persistence.entity.ROLE_USER_VALUE
import com.example.demo.security.data.JwtUser
import com.example.demo.service.LikeNotificationService
import com.example.demo.service.SseService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import javax.annotation.security.RolesAllowed

@Tag(name = "Notifications API")
@RestController
@RequestMapping("v1")
class NotificationController(
    private val sseService: SseService,
    private val likeNotificationService: LikeNotificationService,
) {

    @Operation(summary = "Subscribe to notifications")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    @GetMapping("me/notifications/subscribe")
    fun subscribeToEvents(@AuthenticationPrincipal jwtUser: JwtUser): SseEmitter {
        return sseService.createEmitter(jwtUser.id)
    }

    @Operation(summary = "Get last notifications")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    @GetMapping("me/notifications")
    fun getLastNotifications(@AuthenticationPrincipal jwtUser: JwtUser): ListWrapperDto<Any> {
        return likeNotificationService.getNotifications(jwtUser.id)
    }

}
