package com.example.demo.controller

import com.example.demo.service.GameService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Tag(name = "Game API")
@RestController
@RequestMapping("v1/game")
class GameController(
    private val gameService: GameService
) {

    @GetMapping("{id}")
    fun findById(@PathVariable id: String) = gameService.findGameById(id)

    @GetMapping
    fun findAll() = gameService.findAllGames()
}