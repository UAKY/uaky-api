package com.example.demo.controller

import com.example.demo.constant.enums.UploadFolderName
import com.example.demo.persistence.entity.ROLE_ADMIN_VALUE
import com.example.demo.persistence.entity.ROLE_USER_VALUE
import com.example.demo.service.CloudinaryService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import javax.annotation.security.RolesAllowed

@Tag(name = "Image upload API")
@RestController
@RequestMapping("v1")
class ImageUploadController(
    private val cloudinaryService: CloudinaryService
) {

    @Operation(summary = "Upload image")
    @PostMapping("images/{folder}", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun upload(
        @PathVariable folder: UploadFolderName,
        @RequestPart file: MultipartFile
    ) = cloudinaryService.upload(folder, file)

}
