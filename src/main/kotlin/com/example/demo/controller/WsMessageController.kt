package com.example.demo.controller

import com.example.demo.constant.WsDestinations
import com.example.demo.dto.IncomingMessageDto
import com.example.demo.security.data.JwtUser
import com.example.demo.service.ChatService
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.RestController

@RestController
class WsMessageController(
    private val chatService: ChatService
) {

    @MessageMapping(WsDestinations.CHATS)
    fun handleWsMessage(
        @Payload wsIncomingMessage: IncomingMessageDto,
        @AuthenticationPrincipal user: JwtUser
    ) {
        chatService.handleWsMessage(wsIncomingMessage, user.id)
    }

}
