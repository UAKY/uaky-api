package com.example.demo.controller

import com.example.demo.constant.LIKES
import com.example.demo.persistence.entity.ROLE_ADMIN_VALUE
import com.example.demo.persistence.entity.ROLE_USER_VALUE
import com.example.demo.security.data.JwtUser
import com.example.demo.service.LikesService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

@Tag(name = "User API")
@RestController
@RequestMapping("v1")
class LikesController(
    private val likesService: LikesService
) {

    @CacheEvict(cacheNames = [LIKES], key = "#userId")
    @Operation(summary = "Add like to user by game id")
    @ResponseStatus(HttpStatus.CREATED)
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    @PostMapping("users/{userId}/likes/{gameId}")
    fun addLikeToUser(

        @AuthenticationPrincipal
        jwtUser: JwtUser,

        @PathVariable userId: String,

        @PathVariable gameId: String

    ) = likesService.addLikeToUser(userToId = userId, userFromId = jwtUser.id, gameId = gameId)

    @Cacheable(cacheNames = [LIKES], key = "#jwtUser.id")
    @Operation(summary = "Get likes")
    @GetMapping("me/likes")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun getLikes(@AuthenticationPrincipal jwtUser: JwtUser) = likesService.getUserLikes(jwtUser.id)

}
