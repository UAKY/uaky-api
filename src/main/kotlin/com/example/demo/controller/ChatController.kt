package com.example.demo.controller

import com.example.demo.constant.DEFAULT_CHAT_MESSAGES_REQUEST_SIZE
import com.example.demo.dto.ChatMessageDto
import com.example.demo.dto.ChatResponseDto
import com.example.demo.dto.PageResult
import com.example.demo.persistence.entity.ROLE_ADMIN_VALUE
import com.example.demo.persistence.entity.ROLE_USER_VALUE
import com.example.demo.security.data.JwtUser
import com.example.demo.service.ChatService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Size

@Tag(name = "Chats API")
@Validated
@RestController
@RequestMapping("v1/chats")
class ChatController(
    private val chatService: ChatService
) {

    @Operation(summary = "Get all available chats for current user")
    @GetMapping
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun getChatList(@AuthenticationPrincipal user: JwtUser): List<ChatResponseDto> {
        return chatService.getChatList(user.id)
    }

    @Operation(summary = "Get messages for chat")
    @GetMapping("{chatId}/messages")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun getChatMessages(
        @AuthenticationPrincipal user: JwtUser,

        @PathVariable chatId: String,

        @RequestParam(required = false) lastMessageId: String?,

        @RequestParam(defaultValue = DEFAULT_CHAT_MESSAGES_REQUEST_SIZE)
        @Min(1) @Max(100) size: Int
    ): PageResult<ChatMessageDto> {
        return chatService.getChatMessages(user.id, lastMessageId, size, chatId)
    }

    @Operation(summary = "Mark specific messages in chat as viewed")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping("{chatId}/messages/status")
    @RolesAllowed(ROLE_USER_VALUE, ROLE_ADMIN_VALUE)
    fun markMessagesAsViewed(
        @AuthenticationPrincipal user: JwtUser,
        @PathVariable chatId: String,
        @RequestBody @Size(min = 1) messageIds: List<String>
    ) {
        chatService.markMessagesAsViewed(user.id, chatId, messageIds)
    }

}
