package com.example.demo.constant

const val MIN_SEARCH_USERS_REQUEST_SIZE = 5L
const val USER_ONLINE_MINUTES = 5L
const val ACTUAL_EVENTS_MAX_DAYS = 3L

const val DEFAULT_CHAT_MESSAGES_REQUEST_SIZE = "30"
