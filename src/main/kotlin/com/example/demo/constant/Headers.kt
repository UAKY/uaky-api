package com.example.demo.constant

const val AUTH_HEADER = "Authorization"
const val AUTH_HEADER_PREFIX = "Bearer "