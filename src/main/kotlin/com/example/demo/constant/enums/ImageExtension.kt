package com.example.demo.constant.enums

enum class ImageExtension {
    JPG,
    JPEG,
    PNG;

    companion object {
        fun isValid(extension: String?) = values().any { it.name.equals(extension, ignoreCase = true) }
    }

}
