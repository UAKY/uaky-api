package com.example.demo.constant.enums

enum class UploadFolderName(val folderName: String) {
    USERS("users"),
}
