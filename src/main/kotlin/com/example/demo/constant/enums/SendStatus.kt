package com.example.demo.constant.enums

enum class SendStatus {
    NEW,
    UPDATED,
}
