package com.example.demo.constant

const val INVALID_TOKEN_MSG = "JWT token is expired or invalid"

const val USER_NOT_FOUND_MSG = "User not found."
const val GAME_NOT_FOUND_MSG = "Game not found."
const val CHAT_NOT_FOUND_MSG = "Chat not found."

const val VALIDATION_FAILED_MSG = "Validation failed"

const val VALUE_IS_INVALID = "Value '%s' is invalid"

const val INVALID_PASSWORD = "must have minimum 6 characters, including UPPER/lowercase and numbers"

const val EMAIL_IS_TAKEN_MSG = "email is taken"

const val INVALID_GAME_ID_MSG = "invalid game id"

const val DUPLICATE_GAME_ID = "Duplicate game id"

const val WRONG_PASSWORD_MSG = "wrong password"

const val PASSWORDS_NOT_EQUALS_MSG = "passwords must be equals"

const val MAXIMUM_UPLOAD_SIZE_MSG = "Maximum upload size exceeded"

const val UNSUPPORTED_IMAGE_FORMAT_MSG = "Unsupported image format"
