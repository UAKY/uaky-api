package com.example.demo.dto

import javax.validation.constraints.NotBlank

data class LoginRequestDto(
    @get:NotBlank
    val email: String,

    @get:NotBlank
    val password: String,
)

data class LoginResponseDto(
    val accessToken: String,
    val refreshToken: String,
)