package com.example.demo.dto

import com.example.demo.persistence.projection.LastChatMessageProjection
import com.example.demo.persistence.projection.UnreadMessagesCountProjection

data class ChatProjectionMappingDto(
    val lastChatMessageProjections: List<LastChatMessageProjection>,
    val unreadMessagesCountProjections: List<UnreadMessagesCountProjection>,
)
