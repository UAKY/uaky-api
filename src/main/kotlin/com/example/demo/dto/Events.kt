package com.example.demo.dto

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter

abstract class AbstractEvent<T>(
    val content: T?,
    val type: String
) {
    fun toSseEventBuilder() = SseEmitter.event().name(type).data(this)
}

class PingEvent : AbstractEvent<Nothing>(null, "ping")

class UserLikeEvent(
    content: UserLikeDto,
) : AbstractEvent<UserLikeDto>(content, "like")
