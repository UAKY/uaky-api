package com.example.demo.dto

data class ImageUploadResponse(
    val imageLink: String
)
