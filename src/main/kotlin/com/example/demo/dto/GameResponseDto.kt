package com.example.demo.dto

data class GameResponseDto(
    val id: String?,
    val title: String?,
    val icon: String,
    val imageSmall: String,
    val imageLarge: String,
)