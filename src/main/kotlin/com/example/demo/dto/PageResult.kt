package com.example.demo.dto

data class PageResult<E>(
    val content: List<E>,
    val pageSize: Int,
)
