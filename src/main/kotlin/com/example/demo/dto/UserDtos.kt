package com.example.demo.dto

import com.example.demo.annotations.NoArgsConstructor
import com.example.demo.persistence.entity.Role
import com.example.demo.validation.ComparePassword
import com.example.demo.validation.Password
import com.example.demo.validation.PasswordOwner
import com.example.demo.validation.UniqueEmail
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

data class UserResponseDto(
    val id: String?,
    val email: String,
    val nickname: String,
    val roles: Set<Role>,
    val games: List<GameUserResponseDto>,
    val openedStatus: Boolean,
    val image: String,
)

data class UserSearchResultDto(
    val id: String?,
    val email: String,
    val nickname: String,
    val profileLink: String,
    val description: String?,
    val image: String,
)

@NoArgsConstructor
data class UserLikeDto(
    val id: String?,
    val nickname: String,
    val gameTitle: String,
    val userImage: String,
    val profileLink: String,
    val description: String?,
    val gameLargeImage: String,
    val isMutual: Boolean? = null
)

data class LikeResponseDto(
    val isMutual: Boolean
)

data class UserRegistrationDto(
    @get:Email
    @get:UniqueEmail
    @get:NotBlank
    val email: String,

    @get:Password
    @get:NotBlank
    val password: String,

    @get:NotBlank
    val nickname: String,

    @get:Valid
    @get:NotEmpty
    val games: List<GameUserCreateDto> = emptyList()
)

@ComparePassword
data class UserChangePasswordDto(
    @get:PasswordOwner
    @get:NotBlank
    val oldPassword: String,

    @get:Password
    @get:NotBlank
    val newPassword: String,

    @get:Password
    @get:NotBlank
    val repeatedNewPassword: String,
)

data class UserChangeNicknameRequestDto(
    @get:NotBlank
    val nickname: String
)

data class UserChangeImageRequestDto(
    @get:NotBlank
    val image: String
)
