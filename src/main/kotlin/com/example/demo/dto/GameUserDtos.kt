package com.example.demo.dto

import com.example.demo.validation.ValidGameId
import javax.validation.constraints.NotBlank

data class GameUserResponseDto(
    val id: String?,
    val title: String,
    val profileLink: String,
    val description: String?
)

data class GameUserCreateDto(
    @get:ValidGameId
    @get:NotBlank
    val id: String,

    @get:NotBlank
    val profileLink: String,

    val description: String?
)

data class GameUserUpdateDto(
    @get:NotBlank
    val profileLink: String,

    val description: String?
)