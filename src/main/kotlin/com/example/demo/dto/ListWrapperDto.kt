package com.example.demo.dto

data class ListWrapperDto<T>(
    val content: List<T>
)
