package com.example.demo.dto

import com.example.demo.constant.enums.SendStatus
import com.example.demo.persistence.entity.MessageStatus
import java.time.LocalDateTime

data class IncomingMessageDto(
    val text: String,
    val userToId: String
)

data class OutgoingMessageDto(
    val id: String,
    val chatId: String,
    val text: String,
    val userFrom: String,
    val messageStatus: MessageStatus,
    val sendStatus: SendStatus,
    val createdDate: LocalDateTime
)

data class ChatResponseDto(
    val chat: ChatDto,
    val lastMessage: LastMessageDto,
)

data class ChatDto(
    val id: String?,
    val image: String,
    val title: String,
    val unreadMessagesCount: Int?,
)

data class LastMessageDto(
    val text: String,
    val createdDate: LocalDateTime,
    val isUnread: Boolean,
)

data class ChatMessageDto(
    val id: String,
    val text: String,
    val userFromId: String,
    val messageStatus: MessageStatus,
    val createdDate: LocalDateTime,
)
