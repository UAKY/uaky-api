package com.example.demo.mapper

import com.example.demo.dto.ChatDto
import com.example.demo.dto.ChatMessageDto
import com.example.demo.dto.ChatProjectionMappingDto
import com.example.demo.dto.ChatResponseDto
import com.example.demo.dto.LastMessageDto
import com.example.demo.persistence.projection.ChatMessageListProjection
import com.example.demo.persistence.projection.LastChatMessageProjection
import com.example.demo.persistence.projection.UnreadMessagesCountProjection
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper
interface ChatMapper {

    fun toChatResponseDto(chatProjectionMappingDto: ChatProjectionMappingDto): List<ChatResponseDto> {
        val chatIdByChat = chatProjectionMappingDto.lastChatMessageProjections.associateBy { it.chatId }

        return chatProjectionMappingDto.unreadMessagesCountProjections
            .map { unreadMessagesCountProjection ->
                val chatMessageProjection = chatIdByChat.getValue(unreadMessagesCountProjection.chatId)
                toChatResponseDto(chatMessageProjection, unreadMessagesCountProjection)
            }
    }

    fun toChatResponseDto(
        lastChatMessage: LastChatMessageProjection,
        unreadMessagesCount: UnreadMessagesCountProjection
    ): ChatResponseDto {
        return ChatResponseDto(
            toChatDto(lastChatMessage, unreadMessagesCount),
            toLastMessageDto(lastChatMessage)
        )
    }

    @Mapping(target = "id", source = "chatMessage.chatId")
    @Mapping(target = "title", source = "chatMessage.chatTitle")
    @Mapping(target = "image", source = "chatMessage.chatImage")
    @Mapping(target = "unreadMessagesCount", source = "unreadMessagesCount.unreadMessagesCount")
    fun toChatDto(chatMessage: LastChatMessageProjection, unreadMessagesCount: UnreadMessagesCountProjection): ChatDto

    @Mapping(target = "text", source = "chatMessage.messageText")
    @Mapping(target = "createdDate", source = "chatMessage.messageCreatedDate")
    @Mapping(target = "isUnread", source = "chatMessage.lastMessageUnread")
    fun toLastMessageDto(chatMessage: LastChatMessageProjection): LastMessageDto

    fun toChatMessageDto(chatMessage: ChatMessageListProjection): ChatMessageDto
}
