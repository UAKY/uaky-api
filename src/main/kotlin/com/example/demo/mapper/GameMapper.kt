package com.example.demo.mapper

import com.example.demo.dto.GameResponseDto
import com.example.demo.dto.GameUserResponseDto
import com.example.demo.persistence.entity.Game
import com.example.demo.persistence.entity.GameUser
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper
interface GameMapper {

    fun toDto(game: Game): GameResponseDto

    fun toDto(games: List<Game>): List<GameResponseDto>

    @Mapping(target = "title", source = "pk.game.title")
    @Mapping(target = "id", source = "pk.game.id")
    fun toGameUserResponseDto(gameUser: GameUser): GameUserResponseDto

}
