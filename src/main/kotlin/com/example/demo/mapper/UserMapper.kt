package com.example.demo.mapper

import com.example.demo.dto.UserLikeDto
import com.example.demo.dto.UserResponseDto
import com.example.demo.dto.UserSearchResultDto
import com.example.demo.persistence.entity.GameUser
import com.example.demo.persistence.entity.Role
import com.example.demo.persistence.entity.User
import com.example.demo.security.data.JwtUser
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.security.core.authority.SimpleGrantedAuthority

@Mapper(uses = [GameMapper::class])
interface UserMapper {

    fun toDto(user: User): UserResponseDto

    @Mapping(target = "authorities", source = "roles")
    fun toJwtUser(user: User): JwtUser

    fun toSimpleGrantedAuthority(role: Role) = SimpleGrantedAuthority(role.name)

    @Mapping(target = "profileLink", expression = "java(user.getGame(gameId).getProfileLink())")
    @Mapping(target = "description", expression = "java(user.getGame(gameId).getDescription())")
    fun toSearchResultDto(user: User, gameId: String): UserSearchResultDto

    fun toUserLikeDtos(gameUser: GameUser) = gameUser.likesFromUsers.map { it.toUserLikeDto(gameUser.pk.game.id!!) }

}

fun User.toUserLikeDto(gameId: String, isMutual: Boolean? = null): UserLikeDto {
    val gameUser = getGame(gameId)
    val game = gameUser.pk.game

    return UserLikeDto(
        id = id,
        nickname = nickname,
        gameTitle = game.title,
        userImage = image,
        profileLink = gameUser.profileLink,
        description = gameUser.description,
        gameLargeImage = game.imageLarge,
        isMutual = isMutual
    )
}
