package com.example.demo.mapper

import com.example.demo.constant.enums.SendStatus
import com.example.demo.dto.OutgoingMessageDto
import com.example.demo.persistence.entity.ChatMessage
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper
interface MessageMapper {

    @Mapping(target = "userFrom", source = "chatMessage.userFrom.id")
    @Mapping(target = "chatId", source = "chatMessage.chat.id")
    @Mapping(target = "sendStatus", source = "sendStatus")
    fun toOutgoingMessageDto(chatMessage: ChatMessage, sendStatus: SendStatus): OutgoingMessageDto

}
