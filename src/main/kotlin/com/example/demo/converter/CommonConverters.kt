package com.example.demo.converter

import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date

fun LocalDateTime.toLegacyDate(): Date = Timestamp.valueOf(this)

fun Date.toLocalDateTime(): LocalDateTime = LocalDateTime.ofInstant(toInstant(), ZoneId.systemDefault())
