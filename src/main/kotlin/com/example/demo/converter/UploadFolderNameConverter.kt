package com.example.demo.converter

import com.example.demo.constant.VALUE_IS_INVALID
import com.example.demo.constant.enums.UploadFolderName
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class UploadFolderNameConverter : Converter<String, UploadFolderName> {

    override fun convert(source: String): UploadFolderName {
        return UploadFolderName.values().find { it.folderName.equals(source, ignoreCase = false) }
            ?: throw IllegalArgumentException(VALUE_IS_INVALID.format(source))
    }

}
