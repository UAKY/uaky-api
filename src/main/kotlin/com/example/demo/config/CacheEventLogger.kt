package com.example.demo.config

import mu.KotlinLogging
import org.ehcache.event.CacheEvent
import org.ehcache.event.CacheEventListener

class CacheEventLogger : CacheEventListener<Any, Any> {

    private val log = KotlinLogging.logger {}

    override fun onEvent(cacheEvent: CacheEvent<out Any, out Any>) {
        log.info(
            "CacheLogger Key: {} | EventType: {} | Old value: {} | New value: {}",
            cacheEvent.key, cacheEvent.type, cacheEvent.oldValue, cacheEvent.newValue
        )
    }

}