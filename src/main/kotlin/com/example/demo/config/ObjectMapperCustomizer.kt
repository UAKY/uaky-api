package com.example.demo.config

import com.fasterxml.jackson.annotation.JsonSetter
import com.fasterxml.jackson.annotation.Nulls
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ObjectMapperCustomizer {

    @Bean
    fun jsonCustomizer() = Jackson2ObjectMapperBuilderCustomizer { builder ->
        builder.postConfigurer { it.setDefaultSetterInfo(JsonSetter.Value.forValueNulls(Nulls.AS_EMPTY)) }
    }

}