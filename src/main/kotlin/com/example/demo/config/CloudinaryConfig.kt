package com.example.demo.config

import com.cloudinary.Cloudinary
import mu.KotlinLogging
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Validated
@ConstructorBinding
@ConfigurationProperties("cloudinary")
data class CloudinaryProperties(
    @get:NotBlank
    val cloudName: String,
    @get:NotNull
    val api: CloudinaryApiProperties,
)

data class CloudinaryApiProperties(
    @get:NotBlank
    val key: String,
    @get:NotBlank
    val secret: String,
)

@Configuration
class CloudinaryConfig {

    private val log = KotlinLogging.logger { }

    @Bean
    fun cloudinary(cloudinaryProperties: CloudinaryProperties): Cloudinary {
        log.info { "Creating Cloudinary configuration: $cloudinaryProperties" }

        return Cloudinary(
            mapOf(
                "cloud_name" to cloudinaryProperties.cloudName,
                "api_key" to cloudinaryProperties.api.key,
                "api_secret" to cloudinaryProperties.api.secret
            )
        )
    }

}
