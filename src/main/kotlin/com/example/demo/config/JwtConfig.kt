package com.example.demo.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank

@Validated
@ConstructorBinding
@ConfigurationProperties("jwt-token")
class JwtConfig(
    @NotBlank
    val secret: String,
    @Min(1)
    val timeToLive: Long,
    @Min(1)
    val refreshTokenTimeToLive: Long
)
