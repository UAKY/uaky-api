package com.example.demo.config

import com.example.demo.constant.AUTH_HEADER
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Configuration
class CorsConfiguration {
    @Bean
    fun corsConfigurationSource() =
        UrlBasedCorsConfigurationSource().apply {
            registerCorsConfiguration(
                "/**",
                CorsConfiguration().apply {
                    addAllowedOrigin("*")

                    addAllowedMethod(HttpMethod.POST)
                    addAllowedMethod(HttpMethod.GET)
                    addAllowedMethod(HttpMethod.PUT)
                    addAllowedMethod(HttpMethod.PATCH)
                    addAllowedMethod(HttpMethod.DELETE)
                    addAllowedMethod(HttpMethod.OPTIONS)

                    addAllowedHeader("x-token")
                    addAllowedHeader("Content-Type")
                    addAllowedHeader("Accept")
                    addAllowedHeader("Origin")
                    addAllowedHeader("Access-Control-Expose-Headers")
                    addAllowedHeader(AUTH_HEADER)

                    addExposedHeader("x-token")
                    addExposedHeader(AUTH_HEADER)
                }
            )
        }
}