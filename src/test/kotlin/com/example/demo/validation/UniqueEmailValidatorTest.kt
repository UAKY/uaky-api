package com.example.demo.validation

import com.example.demo.persistence.repository.UserRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.EMAIL
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock

class UniqueEmailValidatorTest : AbstractUnitTest() {

    @Mock
    lateinit var userRepository: UserRepository

    @InjectMocks
    lateinit var uniqueEmailValidator: UniqueEmailValidator

    @Test
    fun `Should return true if user by email not found`() {
        // GIVEN
        val email = EMAIL

        given(userRepository.existsByEmail(email)).willReturn(false)

        // WHEN
        val valid = uniqueEmailValidator.isValid(email, null)

        // THEN
        assertTrue(valid)
    }

    @Test
    fun `Should return false if user by email found`() {
        // GIVEN
        val email = EMAIL

        given(userRepository.existsByEmail(email)).willReturn(true)
        // WHEN
        val valid = uniqueEmailValidator.isValid(email, null)

        // THEN
        assertFalse(valid)
    }

    @Test
    fun `Should return false if email is null`() {
        // GIVEN
        val email = null

        // WHEN
        val valid = uniqueEmailValidator.isValid(email, null)

        // THEN
        assertFalse(valid)
    }

    @Test
    fun `Should return false if email is empty`() {
        // GIVEN
        val email = ""

        // WHEN
        val valid = uniqueEmailValidator.isValid(email, null)

        // THEN
        assertFalse(valid)
    }
}