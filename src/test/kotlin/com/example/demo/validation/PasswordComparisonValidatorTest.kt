package com.example.demo.validation

import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.PASSWORD
import com.example.demo.testutils.mockUserChangePasswordDto
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.Answers
import org.mockito.InjectMocks
import org.mockito.Mock
import javax.validation.ConstraintValidatorContext

class PasswordComparisonValidatorTest : AbstractUnitTest() {

    @InjectMocks
    lateinit var passwordComparisonValidator: PasswordComparisonValidator

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    lateinit var constraintValidatorContext: ConstraintValidatorContext

    @Test
    fun `Should return true if passwords are equals`() {
        // GIVEN
        val userChangePasswordDto = mockUserChangePasswordDto()

        // WHEN
        val valid = passwordComparisonValidator.isValid(userChangePasswordDto, constraintValidatorContext)

        // THEN
        assertTrue(valid)
    }

    @Test
    fun `Should return false if passwords are not equals`() {
        // GIVEN
        val userChangePasswordDto = mockUserChangePasswordDto(PASSWORD)

        // WHEN
        val valid = passwordComparisonValidator.isValid(userChangePasswordDto, constraintValidatorContext)

        // THEN
        assertFalse(valid)
    }
}