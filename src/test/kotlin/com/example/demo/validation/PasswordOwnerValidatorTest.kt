package com.example.demo.validation

import com.example.demo.security.data.JwtUser
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.PASSWORD
import com.example.demo.testutils.PASSWORD_WRONG
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.Answers
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder

class PasswordOwnerValidatorTest : AbstractUnitTest() {

    @InjectMocks
    lateinit var passwordOwnerValidator: PasswordOwnerValidator

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    lateinit var securityContext: SecurityContext

    @Mock
    lateinit var jwtUser: JwtUser

    @Mock
    lateinit var passwordEncoder: PasswordEncoder

    @Test
    fun `Should return true if password is owned by user`() {
        // GIVEN
        SecurityContextHolder.setContext(securityContext)
        given(securityContext.authentication.principal).willReturn(jwtUser)
        given(jwtUser.password).willReturn(PASSWORD)

        given(passwordEncoder.matches(PASSWORD, PASSWORD)).willReturn(true)

        // WHEN
        val valid = passwordOwnerValidator.isValid(PASSWORD, null)

        // THEN
        assertTrue(valid)
    }

    @Test
    fun `Should return false if password is not owned by user`() {
        // GIVEN
        SecurityContextHolder.setContext(securityContext)
        given(securityContext.authentication.principal).willReturn(jwtUser)
        given(jwtUser.password).willReturn(PASSWORD_WRONG)

        given(passwordEncoder.matches(PASSWORD_WRONG, PASSWORD)).willReturn(false)

        // WHEN
        val valid = passwordOwnerValidator.isValid(PASSWORD, null)

        // THEN
        assertFalse(valid)
    }
}