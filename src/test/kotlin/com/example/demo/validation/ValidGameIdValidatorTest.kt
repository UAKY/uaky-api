package com.example.demo.validation

import com.example.demo.persistence.repository.GameRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.GAME_ID_DOTA_2
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock

class ValidGameIdValidatorTest : AbstractUnitTest() {

    @Mock
    lateinit var gameRepository: GameRepository

    @InjectMocks
    lateinit var validGameIdValidator: ValidGameIdValidator

    @Test
    fun `Should return false if game by id not found`() {
        // GIVEN
        val id = GAME_ID_DOTA_2

        given(gameRepository.existsById(id)).willReturn(false)

        // WHEN
        val valid = validGameIdValidator.isValid(id, null)

        // THEN
        assertFalse(valid)
    }

    @Test
    fun `Should return true if game by id found`() {
        // GIVEN
        val id = GAME_ID_DOTA_2

        given(gameRepository.existsById(id)).willReturn(true)

        // WHEN
        val valid = validGameIdValidator.isValid(id, null)

        // THEN
        assertTrue(valid)
    }

    @Test
    fun `Should return false if id is null`() {
        // GIVEN
        val id = null

        // WHEN
        val valid = validGameIdValidator.isValid(id, null)

        // THEN
        assertFalse(valid)
    }

    @Test
    fun `Should return true if id is empty`() {
        // GIVEN
        val id = ""

        // WHEN
        val valid = validGameIdValidator.isValid(id, null)

        // THEN
        assertFalse(valid)
    }
}