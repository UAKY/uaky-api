package com.example.demo.service

import com.example.demo.dto.LikeResponseDto
import com.example.demo.dto.UserLikeDto
import com.example.demo.exception.GameNotFoundException
import com.example.demo.persistence.repository.GameUserRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.DESCRIPTION_DOTA
import com.example.demo.testutils.DOTA_2
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.IMAGE_URL_FORTNITE
import com.example.demo.testutils.NICKNAME
import com.example.demo.testutils.PROFILE_LINK_DOTA
import com.example.demo.testutils.USER_ID
import com.example.demo.testutils.mockGameUser
import com.example.demo.testutils.mockPersistentUser
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.times
import org.mockito.BDDMockito.verify
import org.mockito.InjectMocks
import org.mockito.Mock
import java.util.UUID

class LikesServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var gameUserRepository: GameUserRepository

    @Mock
    lateinit var userService: UserService

    @Mock
    lateinit var likeNotificationService: LikeNotificationService

    @InjectMocks
    lateinit var likesService: LikesService

    @Test
    fun `Should return user likes`() {
        // GIVEN
        val user = mockPersistentUser()
        val gameUser = mockGameUser(user = user)

        val randomUserIds = generateSequence { UUID.randomUUID().toString() }.take(5).toList()

        randomUserIds.forEach { gameUser.addLike(mockPersistentUser(id = it)) }

        val expectedUserLikes = randomUserIds.map {
            UserLikeDto(
                id = it,
                nickname = NICKNAME,
                gameTitle = DOTA_2,
                userImage = IMAGE_LINK,
                profileLink = PROFILE_LINK_DOTA,
                description = DESCRIPTION_DOTA,
                gameLargeImage = IMAGE_URL_FORTNITE,
            )
        }

        given(gameUserRepository.findLikesByUserId(USER_ID)).willReturn(listOf(gameUser))

        // WHEN
        val actualUserLikes = likesService.getUserLikes(USER_ID)

        // THEN
        verify(gameUserRepository, times(1)).findLikesByUserId(USER_ID)
        assertThat(actualUserLikes).isEqualTo(expectedUserLikes)
    }

    @Test
    fun `Should add like to user`() {
        // GIVEN
        val toId = USER_ID
        val fromId = "ff23f6bd-faf4-47f1-bf68-8b16798f2f7e"
        val userFrom = mockPersistentUser(id = fromId)
        val userTo = mockPersistentUser(id = toId)
        val gameUser = mockGameUser(user = userTo)
        val expectedResponse = LikeResponseDto(isMutual = false)

        given(userService.findUserById(toId)).willReturn(userTo)
        given(userService.findUserById(fromId)).willReturn(userFrom)
        given(gameUserRepository.findLikesByUserIdAndGameId(toId, GAME_ID_DOTA_2)).willReturn(gameUser)

        // WHEN
        val actualResponse = likesService.addLikeToUser(toId, fromId, GAME_ID_DOTA_2)

        // THEN
        assertThat(gameUser.likesFromUsers).contains(userFrom)
        assertThat(actualResponse).isEqualTo(expectedResponse)
        verify(userService, times(1)).findUserById(fromId)
        verify(gameUserRepository, times(1)).findLikesByUserIdAndGameId(toId, GAME_ID_DOTA_2)
    }

    @Test
    fun `Should return that like is mutual`() {
        // GIVEN
        val toId = USER_ID
        val fromId = "ff23f6bd-faf4-47f1-bf68-8b16798f2f7e"
        val userFrom = mockPersistentUser(id = fromId)
        val userTo = mockPersistentUser(id = toId).apply { addLikedUser(userFrom.getGame(GAME_ID_DOTA_2)) }
        val gameUser = mockGameUser(user = userTo)
        val expectedResponse = LikeResponseDto(isMutual = true)

        given(userService.findUserById(toId)).willReturn(userTo)
        given(userService.findUserById(fromId)).willReturn(userFrom)
        given(gameUserRepository.findLikesByUserIdAndGameId(toId, GAME_ID_DOTA_2)).willReturn(gameUser)

        // WHEN
        val actualResponse = likesService.addLikeToUser(toId, fromId, GAME_ID_DOTA_2)

        // THEN
        assertThat(actualResponse).isEqualTo(expectedResponse)
    }

    @Test
    fun `Should throw GameNotFoundException if gameUser not found`() {
        // GIVEN
        val fromId = "ff23f6bd-faf4-47f1-bf68-8b16798f2f7e"
        val userFrom = mockPersistentUser(id = fromId)

        given(userService.findUserById(fromId)).willReturn(userFrom)
        given(gameUserRepository.findLikesByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)).willReturn(null)

        // WHEN THEN
        assertThrows<GameNotFoundException> { likesService.addLikeToUser(USER_ID, fromId, GAME_ID_DOTA_2) }
    }

}
