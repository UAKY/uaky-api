package com.example.demo.service

import com.example.demo.dto.ListWrapperDto
import com.example.demo.dto.UserLikeDto
import com.example.demo.dto.UserLikeEvent
import com.example.demo.persistence.repository.NotificationRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.DESCRIPTION_DOTA
import com.example.demo.testutils.DOTA_2
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.IMAGE_URL_FORTNITE
import com.example.demo.testutils.NICKNAME
import com.example.demo.testutils.PROFILE_LINK_DOTA
import com.example.demo.testutils.USER_ID
import com.example.demo.testutils.mockPersistentUser
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.kotlin.any
import org.mockito.kotlin.eq
import org.mockito.kotlin.given
import org.mockito.kotlin.verify

class LikeNotificationServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var sseService: SseService

    @Mock
    lateinit var userService: UserService

    @Mock
    lateinit var notificationRepository: NotificationRepository

    @InjectMocks
    lateinit var likeNotificationService: LikeNotificationService

    @Test
    fun `Should notify and save event`() {
        // GIVEN
        val toId = "00097135-3239-4589-bf8e-429624215d1c"
        val fromId = "4b49280b-d2a5-4323-9fc0-0575e9d6a4e8"
        val gameId = GAME_ID_DOTA_2
        val isMutual = false

        val toUser = mockPersistentUser(id = toId)
        val fromUser = mockPersistentUser(id = fromId)

        given(userService.findUserById(toId)).willReturn(toUser)
        given(userService.findUserById(fromId)).willReturn(fromUser)

        // WHEN
        likeNotificationService.notify(toId, fromId, gameId, isMutual)

        // THEN
        verify(sseService).sendEvent(eq(toId), any())
        verify(notificationRepository).save(any())
    }

    @Test
    fun `Should return notifications`() {
        // GIVEN
        val userId = USER_ID

        val event = UserLikeEvent(
            UserLikeDto(
                id = userId,
                nickname = NICKNAME,
                gameTitle = DOTA_2,
                userImage = IMAGE_LINK,
                profileLink = PROFILE_LINK_DOTA,
                description = DESCRIPTION_DOTA,
                gameLargeImage = IMAGE_URL_FORTNITE,
                isMutual = true
            )
        )

        val expectedResponse = ListWrapperDto(listOf(event))

        given(notificationRepository.findAllEventsByUserAndCreatedDateAfter(eq(userId), any()))
            .willReturn(
                listOf(event)
            )

        // WHEN
        val actualResponse = likeNotificationService.getNotifications(userId)

        // THEN
        verify(notificationRepository).findAllEventsByUserAndCreatedDateAfter(eq(userId), any())
        assertThat(expectedResponse).isEqualTo(actualResponse)
    }

}
