package com.example.demo.service

import com.example.demo.dto.UserSearchResultDto
import com.example.demo.exception.GameNotFoundException
import com.example.demo.mapper.toSearchResultDto
import com.example.demo.persistence.entity.Game
import com.example.demo.persistence.entity.User
import com.example.demo.persistence.repository.GameUserRepository
import com.example.demo.persistence.repository.UserRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.DESCRIPTION_DOTA
import com.example.demo.testutils.DESCRIPTION_FORTNITE
import com.example.demo.testutils.FORTNITE
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.GAME_ID_FORTNITE
import com.example.demo.testutils.PROFILE_LINK_DOTA
import com.example.demo.testutils.PROFILE_LINK_FORNITE
import com.example.demo.testutils.USER_ID
import com.example.demo.testutils.mockGameUser
import com.example.demo.testutils.mockPersistentUser
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.BDDMockito.anyString
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.verify
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.times
import org.springframework.data.domain.PageRequest
import java.util.UUID

class SearchServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var userRepository: UserRepository

    @Mock
    lateinit var gameUserRepository: GameUserRepository

    @InjectMocks
    lateinit var searchService: SearchService

    @Test
    fun `Should throw GameNotFoundException if gameUser not found`() {
        // GIVEN
        val pageSize = 1
        given(gameUserRepository.findViewsByUserIdAndGameId(anyString(), anyString())).willReturn(null)

        // WHEN
        assertThrows<GameNotFoundException> { searchService.commonSearchByGameId(GAME_ID_DOTA_2, USER_ID, pageSize) }
    }

    @Test
    fun `Should return all users if no users were viewed`() {
        // GIVEN
        val currentUser = mockPersistentUser()
        val mockGameUser = currentUser.games[0]
        val pageSize = 5

        val randomUserIds = generateSequence { UUID.randomUUID().toString() }.take(5).toList()
        val randomUsers = randomUserIds.map { mockPersistentUser(id = it) }

        val expectedSearchResult = usersToSearchResult(randomUsers)

        given(gameUserRepository.findViewsByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)).willReturn(mockGameUser)
        given(userRepository.findRandomUserIds(GAME_ID_DOTA_2, setOf(USER_ID), PageRequest.of(0, pageSize))).willReturn(randomUserIds)
        given(userRepository.findAllByIds(randomUserIds)).willReturn(randomUsers)

        // WHEN
        val result = searchService.commonSearchByGameId(GAME_ID_DOTA_2, USER_ID, pageSize)

        // THEN
        assertThat(mockGameUser.viewedUsersCommon).containsAll(randomUsers) // TODO add liked users
        assertThat(result.pageSize).isEqualTo(pageSize)
        assertThat(result.content).hasSameSizeAs(randomUsers)
        assertThat(result.content).containsAll(expectedSearchResult)
    }

    @Test
    fun `Should return only not viewed users`() {
        // GIVEN
        val viewedUserIds = generateSequence { UUID.randomUUID().toString() }.take(5).toList()
        val randomViewedUsers = viewedUserIds.map { mockPersistentUser(id = it) }

        val currentUser = mockPersistentUser()
        val mockGameUser = currentUser.games[0].apply { addViewedUsersCommon(randomViewedUsers) }

        val notViewedUserIds = generateSequence { UUID.randomUUID().toString() }.take(5).toList()
        val randomNotViewedUsers = notViewedUserIds.map { mockPersistentUser(id = it) }

        val pageSize = 5

        val expectedSearchResult = usersToSearchResult(randomNotViewedUsers)
        val skipUserIds = (viewedUserIds + USER_ID).toSet()

        given(gameUserRepository.findViewsByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)).willReturn(mockGameUser)
        given(userRepository.findRandomUserIds(GAME_ID_DOTA_2, skipUserIds, PageRequest.of(0, pageSize)))
            .willReturn(notViewedUserIds)
        given(userRepository.findAllByIds(notViewedUserIds)).willReturn(randomNotViewedUsers)

        // WHEN
        val result = searchService.commonSearchByGameId(GAME_ID_DOTA_2, USER_ID, pageSize)

        // THEN
        assertThat(mockGameUser.viewedUsersCommon).containsAll(randomViewedUsers + randomNotViewedUsers)
        assertThat(result.content).hasSameSizeAs(randomNotViewedUsers)
        assertThat(result.content).containsAll(expectedSearchResult)
    }

    @Test
    fun `Should convert to SearchResultDto with appropriate game`() {
        // GIVEN
        val user = mockPersistentUser(games = emptyList())

        val mockGameUserDota = mockGameUser(user = user)
        val mockGameUserFortnite = mockGameUser(
            user = user,
            game = Game(GAME_ID_FORTNITE, FORTNITE),
            profileLink = PROFILE_LINK_FORNITE,
            description = DESCRIPTION_FORTNITE
        )

        user.games.addAll(listOf(mockGameUserDota, mockGameUserFortnite))

        // WHEN
        val searchResultWithDota = user.toSearchResultDto(GAME_ID_DOTA_2)
        val searchResultWithFortnite = user.toSearchResultDto(GAME_ID_FORTNITE)

        // THEN
        assertThat(searchResultWithDota.profileLink).isEqualTo(PROFILE_LINK_DOTA)
        assertThat(searchResultWithDota.description).isEqualTo(DESCRIPTION_DOTA)

        assertThat(searchResultWithFortnite.profileLink).isEqualTo(PROFILE_LINK_FORNITE)
        assertThat(searchResultWithFortnite.description).isEqualTo(DESCRIPTION_FORTNITE)
    }

    @Test
    fun `Should clear views`() {
        // GIVEN
        val viewedUserIds = generateSequence { UUID.randomUUID().toString() }.take(5).toList()
        val randomViewedUsers = viewedUserIds.map { mockPersistentUser(id = it) }

        val currentUser = mockPersistentUser()
        val currentGameUser = mockGameUser(currentUser).apply { addViewedUsersCommon(randomViewedUsers) }

        given(gameUserRepository.findViewsByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)).willReturn(currentGameUser)

        // WHEN
        searchService.clearViewsCommon(GAME_ID_DOTA_2, USER_ID)

        // THEN
        assertThat(currentGameUser.viewedUsersCommon).isEmpty()

        verify(gameUserRepository, times(1)).findViewsByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)
    }

    private fun usersToSearchResult(randomUsers: List<User>): List<UserSearchResultDto> {
        return randomUsers.map {
            UserSearchResultDto(it.id, it.email, it.nickname, it.games[0].profileLink, it.games[0].description, it.image)
        }
    }
}
