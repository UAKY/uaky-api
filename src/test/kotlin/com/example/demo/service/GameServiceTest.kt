package com.example.demo.service

import com.example.demo.exception.GameNotFoundException
import com.example.demo.persistence.repository.GameRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.mockGameDto
import com.example.demo.testutils.mockGameDtos
import com.example.demo.testutils.mockPersistentGame
import com.example.demo.testutils.mockPersistentGames
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import java.util.Optional

class GameServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var gameRepository: GameRepository

    @InjectMocks
    lateinit var gameService: GameService

    @Test
    fun `Should return all games`() {
        // GIVEN
        val listOfGames = mockPersistentGames()
        val expectedGameDtos = mockGameDtos()

        given(gameRepository.findAll()).willReturn(listOfGames)

        // WHEN
        val actualGameDtos = gameService.findAllGames()

        // THEN
        assertThat(actualGameDtos).isEqualTo(expectedGameDtos)
    }

    @Test
    fun `Should return game by id`() {
        // GIVEN
        val gameToReturn = mockPersistentGame()
        val expectedGameDto = mockGameDto()

        given(gameRepository.findById(GAME_ID_DOTA_2)).willReturn(Optional.of(gameToReturn))

        // WHEN
        val actualGameDto = gameService.findGameById(GAME_ID_DOTA_2)

        // THEN
        assertThat(actualGameDto).isEqualTo(expectedGameDto)
    }

    @Test
    fun `Should throw GameNotFoundException if game by id not found`() {
        // GIVEN
        given(gameRepository.findById(GAME_ID_DOTA_2)).willReturn(Optional.empty())

        // WHEN THEN
        assertThrows<GameNotFoundException> { gameService.findGameById(GAME_ID_DOTA_2) }
    }
}