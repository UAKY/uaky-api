package com.example.demo.service

import com.cloudinary.Cloudinary
import com.example.demo.constant.UNSUPPORTED_IMAGE_FORMAT_MSG
import com.example.demo.constant.enums.UploadFolderName
import com.example.demo.exception.InvalidImageFormatException
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.mockImageUploadResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.kotlin.given
import org.mockito.kotlin.mock
import org.springframework.mock.web.MockMultipartFile

class CloudinaryServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var cloudinary: Cloudinary

    @InjectMocks
    lateinit var cloudinaryService: CloudinaryService

    @Test
    fun `Should throw InvalidImageFormatException if file has unsupported format`() {
        // GIVEN
        val file = MockMultipartFile("image", "image.gif", null, "image.gif".byteInputStream())

        // WHEN THEN
        assertThrows<InvalidImageFormatException>(UNSUPPORTED_IMAGE_FORMAT_MSG) {
            cloudinaryService.upload(UploadFolderName.USERS, file)
        }
    }

    @ParameterizedTest
    @MethodSource("validFiles")
    fun `Should upload valid file`(file: MockMultipartFile) {
        // GIVEN
        val folder = UploadFolderName.USERS
        val expectedResponse = mockImageUploadResponse()

        given(cloudinary.uploader()).willReturn(mock())
        given(cloudinary.uploader().upload(file.bytes, mapOf("folder" to folder.folderName))).willReturn(mapOf("url" to IMAGE_LINK))

        // WHEN
        val actualResponse = cloudinaryService.upload(folder, file)

        // THEN
        assertThat(actualResponse).isEqualTo(expectedResponse)
    }

    companion object {
        @JvmStatic
        fun validFiles() = listOf(
            MockMultipartFile("jpg", "image.jpg", null, "content".byteInputStream()),
            MockMultipartFile("JPEG", "image.JPEG", null, "content".byteInputStream()),
            MockMultipartFile("jpeg", "image.jpeg", null, "content".byteInputStream()),
            MockMultipartFile("png", "image.png", null, "content".byteInputStream()),
        )
    }

}
