package com.example.demo.service

import com.example.demo.constant.DUPLICATE_GAME_ID
import com.example.demo.constant.GAME_NOT_FOUND_MSG
import com.example.demo.exception.DuplicateEntryException
import com.example.demo.exception.GameNotFoundException
import com.example.demo.persistence.repository.GameUserRepository
import com.example.demo.persistence.repository.UserRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.DESCRIPTION_FORTNITE
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.GAME_ID_FORTNITE
import com.example.demo.testutils.PROFILE_LINK_FORNITE
import com.example.demo.testutils.USER_ID
import com.example.demo.testutils.mockGameUser
import com.example.demo.testutils.mockGameUserDto
import com.example.demo.testutils.mockGameUserUpdateDto
import com.example.demo.testutils.mockPersistentUser
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.AssertionsForInterfaceTypes
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import java.util.Optional

class GameUserServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var gameUserRepository: GameUserRepository

    @Mock
    lateinit var userRepository: UserRepository

    @InjectMocks
    lateinit var gameUserService: GameUserService

    @Test
    fun `Should add game`() {
        // GIVEN
        val user = mockPersistentUser(games = emptyList())
        val gameUserDto = mockGameUserDto(id = "4e4cdb29-a343-4e96-af1d-190ee04f5686")

        given(userRepository.findById(USER_ID)).willReturn(Optional.of(user))

        // WHEN
        gameUserService.addGame(USER_ID, gameUserDto)

        // THEN
        AssertionsForInterfaceTypes.assertThat(user.games).hasSize(1)
    }

    @Test
    fun `Should throw DuplicateEntryException if game id is duplicated`() {
        // GIVEN
        val user = mockPersistentUser()
        val gameUserDto = mockGameUserDto()

        given(userRepository.findById(USER_ID)).willReturn(Optional.of(user))

        // WHEN THEN
        assertThrows<DuplicateEntryException>(DUPLICATE_GAME_ID) { gameUserService.addGame(USER_ID, gameUserDto) }
    }

    @Test
    fun `Should remove game`() {
        // GIVEN
        given(gameUserRepository.existsByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)).willReturn(true)

        // WHEN
        gameUserService.removeGame(USER_ID, GAME_ID_DOTA_2)

        // THEN
        verify(gameUserRepository, times(1)).existsByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)
        verify(gameUserRepository, times(1)).deleteByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)
    }

    @Test
    fun `Should throw GameNotFoundException if game not found on delete`() {
        // GIVEN
        given(gameUserRepository.existsByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)).willReturn(false)

        // WHEN THEN
        assertThrows<GameNotFoundException>(GAME_NOT_FOUND_MSG) { gameUserService.removeGame(USER_ID, GAME_ID_FORTNITE) }
    }

    @Test
    fun `Should update game`() {
        // GIVEN
        val user = mockPersistentUser()
        val gameUser = mockGameUser(user = user)
        val gameUserUpdateDto = mockGameUserUpdateDto()

        given(gameUserRepository.findByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)).willReturn(gameUser)

        // WHEN
        gameUserService.editGame(USER_ID, GAME_ID_DOTA_2, gameUserUpdateDto)

        // THEN
        assertThat(gameUser.profileLink).isEqualTo(PROFILE_LINK_FORNITE)
        assertThat(gameUser.description).isEqualTo(DESCRIPTION_FORTNITE)

        verify(gameUserRepository, times(1)).findByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)
    }

    @Test
    fun `Should throw GameNotFoundException if game not found on update`() {
        // GIVEN
        val gameUserUpdateDto = mockGameUserUpdateDto()

        given(gameUserRepository.findByUserIdAndGameId(USER_ID, GAME_ID_DOTA_2)).willReturn(null)

        // WHEN THEN
        assertThrows<GameNotFoundException>(GAME_NOT_FOUND_MSG) {
            gameUserService.editGame(
                USER_ID,
                GAME_ID_FORTNITE,
                gameUserUpdateDto
            )
        }
    }
}