package com.example.demo.service

import com.example.demo.constant.USER_NOT_FOUND_MSG
import com.example.demo.dto.UserChangeNicknameRequestDto
import com.example.demo.exception.UserNotFoundException
import com.example.demo.persistence.entity.User
import com.example.demo.persistence.repository.UserRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.PASSWORD
import com.example.demo.testutils.PASSWORD_NEW
import com.example.demo.testutils.USER_ID
import com.example.demo.testutils.anyUser
import com.example.demo.testutils.assertUsersEquals
import com.example.demo.testutils.mockNewUser
import com.example.demo.testutils.mockPersistentUser
import com.example.demo.testutils.mockUserChangePasswordDto
import com.example.demo.testutils.mockUserRegistrationDto
import com.example.demo.testutils.mockUserResponseDto
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.ArgumentCaptor
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.times
import org.mockito.Captor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.springframework.security.crypto.password.PasswordEncoder
import java.util.Optional

class UserServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var userRepository: UserRepository

    @Mock
    lateinit var passwordEncoder: PasswordEncoder

    @Mock
    lateinit var cloudinaryService: CloudinaryService

    @InjectMocks
    lateinit var userService: UserService

    @Captor
    lateinit var userCaptor: ArgumentCaptor<User>

    @Test
    fun `Should register`() {
        // GIVEN
        val expectedUserToSave = mockNewUser()
        val userAfterSave = mockPersistentUser()

        val userRegistrationDto = mockUserRegistrationDto()

        given(passwordEncoder.encode(PASSWORD)).willReturn(PASSWORD)
        given(userRepository.save(anyUser())).willReturn(userAfterSave)
        given(cloudinaryService.findAllDefaultUserImages()).willReturn(listOf(IMAGE_LINK))

        // WHEN
        userService.register(userRegistrationDto)

        // THEN
        verify(passwordEncoder, times(1)).encode(PASSWORD)
        verify(userRepository, times(1)).save(userCaptor.capture())
        verify(cloudinaryService, times(1)).findAllDefaultUserImages()
        assertUsersEquals(userCaptor.value, expectedUserToSave)
    }

    @Test
    fun `Should find user by email`() {
        // GIVEN
        val userToReturn = mockPersistentUser()

        given(userRepository.findByEmail(EMAIL)).willReturn(userToReturn)

        // WHEN
        val actualUser = userService.findUserByEmail(EMAIL)

        // THEN
        assertUsersEquals(actualUser, userToReturn)
    }

    @Test
    fun `Should throw UserNotFoundException if user by email not found`() {
        // GIVEN
        given(userRepository.findByEmail(EMAIL)).willReturn(null)

        // WHEN THEN
        assertThrows<UserNotFoundException>(USER_NOT_FOUND_MSG) { userService.findUserByEmail(EMAIL) }
    }

    @Test
    fun `Should find by id`() {
        // GIVEN
        val mockPersistentUser = mockPersistentUser()
        val expectedResponseDto = mockUserResponseDto()

        given(userRepository.findByIdWithGames(USER_ID)).willReturn(mockPersistentUser)

        // WHEN
        val actualUserResponseDto = userService.findById(USER_ID)

        // THEN
        assertThat(actualUserResponseDto).isEqualTo(expectedResponseDto)
        verify(userRepository, times(1)).findByIdWithGames(USER_ID)
    }

    @Test
    fun `Should throw UserNotFoundException if user by id not found`() {
        // GIVEN
        given(userRepository.findByIdWithGames(USER_ID)).willReturn(null)

        // WHEN THEN
        assertThrows<UserNotFoundException>(USER_NOT_FOUND_MSG) { userService.findById(USER_ID) }
    }

    @Test
    fun `Should change nickname`() {
        // GIVEN
        val user = mockPersistentUser()
        val expectedNickname = "Olx"
        val nicknameRequestDto = UserChangeNicknameRequestDto(expectedNickname)

        given(userRepository.findById(USER_ID)).willReturn(Optional.of(user))

        // WHEN
        userService.changeNickname(USER_ID, nicknameRequestDto)

        // THEN
        assertThat(user.nickname).isEqualTo(expectedNickname)
    }

    @Test
    fun `Should switch account status`() {
        // GIVEN
        val user = mockPersistentUser(openedStatus = true)

        given(userRepository.findById(USER_ID)).willReturn(Optional.of(user))

        // WHEN
        userService.switchAccountStatus(USER_ID)

        // THEN
        assertThat(user.openedStatus).isFalse

        verify(userRepository, times(1)).findById(USER_ID)
    }

    @Test
    fun `Should throw UserNotFoundException if user not found on switch account status`() {
        // GIVEN
        given(userRepository.findById(USER_ID)).willThrow(UserNotFoundException())

        // WHEN THEN
        assertThrows<UserNotFoundException>(USER_NOT_FOUND_MSG) { userService.switchAccountStatus(USER_ID) }
    }

    @Test
    fun `Should change password`() {
        // GIVEN
        val user = mockPersistentUser()
        val passDto = mockUserChangePasswordDto()

        given(passwordEncoder.encode(PASSWORD_NEW)).willReturn(PASSWORD_NEW)
        given(userRepository.findById(USER_ID)).willReturn(Optional.of(user))

        // WHEN
        userService.changePassword(USER_ID, passDto)

        // THEN
        assertThat(user.password).isEqualTo(PASSWORD_NEW)

        verify(passwordEncoder, times(1)).encode(PASSWORD_NEW)
        verify(userRepository, times(1)).findById(USER_ID)
    }
}
