package com.example.demo.exceptions

import com.example.demo.exception.ValidationErrorDetails
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.RETURNS_DEEP_STUBS
import org.mockito.Mockito.mock
import org.springframework.validation.FieldError
import javax.validation.ConstraintViolation

const val OBJECT = "someObject"

const val FIELD_1 = "someField1"
const val FIELD_2 = "someField2"

const val MESSAGE_1 = "must be not null"
const val MESSAGE_2 = "cannot be empty"

class ValidationErrorDetailsTest {

    @Test
    fun `Should contain correct field name and error when there is only one field with one error`() {
        // GIVEN
        val fieldError = FieldError(OBJECT, FIELD_1, MESSAGE_1)

        val validationErrorDetails = ValidationErrorDetails(listOf(fieldError))

        // WHEN
        val validationErrors = validationErrorDetails.validationErrors

        // THEN
        assertThat(validationErrors).containsKey(FIELD_1)
        assertThat(validationErrors[FIELD_1]).containsOnly(MESSAGE_1)
    }

    @Test
    fun `Should contain correct field names and errors when there are two fields with one error for each`() {
        // GIVEN
        val fieldError1 = FieldError(OBJECT, FIELD_1, MESSAGE_1)
        val fieldError2 = FieldError(OBJECT, FIELD_2, MESSAGE_2)

        val validationErrorDetails = ValidationErrorDetails(listOf(fieldError1, fieldError2))

        // WHEN
        val validationErrors = validationErrorDetails.validationErrors

        // THEN
        assertThat(validationErrors).containsKeys(FIELD_1, FIELD_2)
        assertThat(validationErrors[FIELD_1]).containsOnly(MESSAGE_1)
        assertThat(validationErrors[FIELD_2]).containsOnly(MESSAGE_2)
    }

    @Test
    fun `Should contain correct field name and errors when there is only one field with multiple errors`() {
        // GIVEN
        val fieldError1 = FieldError(OBJECT, FIELD_1, MESSAGE_1)
        val fieldError2 = FieldError(OBJECT, FIELD_1, MESSAGE_2)

        val validationErrorDetails = ValidationErrorDetails(listOf(fieldError1, fieldError2))

        // WHEN
        val validationErrors = validationErrorDetails.validationErrors

        // THEN
        assertThat(validationErrors).containsKey(FIELD_1)
        assertThat(validationErrors[FIELD_1]).containsOnly(MESSAGE_1, MESSAGE_2)
    }

    @Test
    fun `Should work correctly with ConstraintViolation class`() {
        // GIVEN
        val constraintViolation = mock(ConstraintViolation::class.java, RETURNS_DEEP_STUBS)

        given(constraintViolation.propertyPath.iterator().next().name).willReturn(FIELD_1)
        given(constraintViolation.message).willReturn(MESSAGE_1)

        val validationErrorDetails = ValidationErrorDetails(setOf(constraintViolation))

        // WHEN
        val validationErrors = validationErrorDetails.validationErrors

        // THEN
        assertThat(validationErrors).containsKey(FIELD_1)
        assertThat(validationErrors[FIELD_1]).containsOnly(MESSAGE_1)
    }

}