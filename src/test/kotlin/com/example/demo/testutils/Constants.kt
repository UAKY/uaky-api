package com.example.demo.testutils

const val TOKEN = "wnBMkk93K0EzaO7Pvv31SfExuPnTC4"
const val REFRESH_TOKEN = "saM0vXfZdkfFAzl0y3r7ZuISfEx93K0"

const val USER_ID = "6f07af22-10df-4cd4-bb09-21c49bb542d9"
const val EMAIL = "uMezBF@gmail.com"
const val EMAIL_INVALID = "invalidmail.com"
const val PASSWORD = "Password@123"
const val PASSWORD_INVALID = "invpass"
const val PASSWORD_NEW = "Password@321"
const val PASSWORD_WRONG = "Passwordik1"
const val NICKNAME = "Nexor"

const val PROFILE_LINK_DOTA = "https://steam.com/nexor"
const val PROFILE_LINK_FORNITE = "https://epicgames.com/nexor"
const val DESCRIPTION_DOTA = "Looking for teammate for Dota 2"
const val DESCRIPTION_FORTNITE = "Looking for teammate for Fortnite"

const val GAME_ID_DOTA_2 = "15a841f9-dc30-4076-a62e-dfe9dc386d03"
const val GAME_ID_FORTNITE = "5e0d3e4f-44ab-40f1-ba91-b8e196ec6d9a"
const val GAME_ID_INVALID = "d91c2819-42be-4d8a-897b-7f6cf7dbfd91"
const val DOTA_2 = "Dota 2"
const val FORTNITE = "Fortnite"

const val IMAGE_URL_DOTA_2 = "https://radikal.ru/fp/fv1dy4fjx0ihl"
const val IMAGE_URL_FORTNITE = "https://ru.gecid.com/data/news/202008141107-60860/img/01_fortnite.jpg"

const val NOT_BLANK = "must not be blank"

const val IMAGE_LINK = "https://res.cloudinary.com/ortube/image/upload/v1620975563/users/orange.jpg"
