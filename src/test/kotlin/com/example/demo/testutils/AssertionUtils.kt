package com.example.demo.testutils

import com.example.demo.persistence.entity.GameUser
import com.example.demo.persistence.entity.User
import com.example.demo.security.data.JwtUser
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat

// since for Entities `equals` compares only `id` we have to manually compare all fields

fun assertUsersEquals(actual: User, expected: User) {
    assertThat(actual.id).isEqualTo(expected.id)
    assertThat(actual.email).isEqualTo(expected.email)
    assertThat(actual.password).isEqualTo(expected.password)
    assertThat(actual.nickname).isEqualTo(expected.nickname)
    assertThat(actual.roles).isEqualTo(expected.roles)
    assertThat(actual.refreshTokens).isEqualTo(expected.refreshTokens)
    assertGameUserEquals(actual.games, expected.games)
}

fun assertGameUserEquals(actual: List<GameUser>, expected: List<GameUser>) {
    assertThat(actual).hasSameSizeAs(expected)
    for (i in expected.indices) {
        assertThat(actual[i].profileLink).isEqualTo(expected[i].profileLink)
        assertThat(actual[i].description).isEqualTo(expected[i].description)
        assertThat(actual[i].pk.game.id).isEqualTo(expected[i].pk.game.id)
    }
}

fun assertJwtUsersEqual(actualJwtUser: JwtUser, expectedJwtUser: JwtUser) {
    assertThat(actualJwtUser.id).isEqualTo(expectedJwtUser.id)
    assertThat(actualJwtUser.username).isEqualTo(expectedJwtUser.username)
    assertThat(actualJwtUser.password).isEqualTo(expectedJwtUser.password)
    assertThat(actualJwtUser.authorities).isEqualTo(expectedJwtUser.authorities)
}
