package com.example.demo.testutils

import com.example.demo.dto.GameUserCreateDto
import com.example.demo.dto.GameUserResponseDto
import com.example.demo.dto.GameUserUpdateDto
import com.example.demo.persistence.entity.Game
import com.example.demo.persistence.entity.GameUser
import com.example.demo.persistence.entity.User

fun mockGameUserDto(
    id: String = GAME_ID_DOTA_2,
    profileLink: String = PROFILE_LINK_DOTA,
    description: String = DESCRIPTION_DOTA
) = GameUserCreateDto(
    id = id,
    profileLink = profileLink,
    description = description
)

fun mockGameUser(
    user: User,
    game: Game = Game(GAME_ID_DOTA_2, DOTA_2, imageLarge = IMAGE_URL_FORTNITE),
    profileLink: String = PROFILE_LINK_DOTA,
    description: String = DESCRIPTION_DOTA,
) = GameUser(
    profileLink = profileLink,
    description = description,
    user = user,
    game = game
)

fun mockGameUserResponseDto() = GameUserResponseDto(
    id = GAME_ID_DOTA_2,
    title = DOTA_2,
    profileLink = PROFILE_LINK_DOTA,
    description = DESCRIPTION_DOTA
)

fun mockGameUserUpdateDto() = GameUserUpdateDto(
    profileLink = PROFILE_LINK_FORNITE,
    description = DESCRIPTION_FORTNITE
)
