package com.example.demo.testutils

import com.example.demo.dto.LoginRequestDto
import com.example.demo.dto.LoginResponseDto
import com.example.demo.dto.UserChangePasswordDto
import com.example.demo.dto.UserRegistrationDto
import com.example.demo.dto.UserResponseDto
import com.example.demo.persistence.entity.GameUser
import com.example.demo.persistence.entity.Role
import com.example.demo.persistence.entity.User
import com.example.demo.security.data.JwtUser
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority

val ROLES = setOf(Role.ROLE_USER)

fun mockPersistentUser(
    id: String = USER_ID,
    email: String = EMAIL,
    password: String = PASSWORD,
    nickname: String = NICKNAME,
    roles: Set<Role> = ROLES,
    refreshTokens: Set<String> = mutableSetOf(),
    games: List<GameUser>? = null,
    openedStatus: Boolean = true,
    image: String = IMAGE_LINK
) = User(
    email = email,
    password = password,
    nickname = nickname,
    roles = roles,
    refreshTokens = refreshTokens.toMutableSet(),
    openedStatus = openedStatus,
    image = image
).apply {
    this.id = id
    this.games += games?.toMutableList() ?: listOf(mockGameUser(this))
}

fun mockPersistentUserWithToken() = User(
    email = EMAIL,
    password = PASSWORD,
    nickname = NICKNAME,
    roles = ROLES,
    refreshTokens = mutableSetOf(TOKEN),
    openedStatus = true,
    image = IMAGE_LINK
).apply { id = USER_ID }

fun mockNewUser() = User(
    email = EMAIL,
    password = PASSWORD,
    nickname = NICKNAME,
    roles = ROLES,
    refreshTokens = mutableSetOf(),
    openedStatus = true,
    image = IMAGE_LINK
).apply {
    games += mockGameUser(this)
}

fun mockUserRegistrationDto() = UserRegistrationDto(
    email = EMAIL,
    password = PASSWORD,
    nickname = NICKNAME,
    games = listOf(mockGameUserDto())
)

fun mockUserResponseDto() = UserResponseDto(
    id = USER_ID,
    email = EMAIL,
    nickname = NICKNAME,
    roles = ROLES,
    games = listOf(mockGameUserResponseDto()),
    openedStatus = true,
    image = IMAGE_LINK
)

fun mockUserChangePasswordDto(
    repeatedNewPassword: String = PASSWORD_NEW
) = UserChangePasswordDto(
    oldPassword = PASSWORD,
    newPassword = PASSWORD_NEW,
    repeatedNewPassword = repeatedNewPassword
)

fun mockJwtUser() = JwtUser(
    id = USER_ID,
    email = EMAIL,
    password = PASSWORD,
    authorities = listOf(SimpleGrantedAuthority(Role.ROLE_USER.name))
)

fun mockUserPasswordAuthTokenWithUser() =
    UsernamePasswordAuthenticationToken(mockJwtUser(), "", mockJwtUser().authorities)

fun mockUserPasswordAuthTokenWithEmailPassword() =
    UsernamePasswordAuthenticationToken(EMAIL, PASSWORD)

fun loginRequestDto() = LoginRequestDto(EMAIL, PASSWORD)

fun loginResponseDto() = LoginResponseDto(TOKEN, REFRESH_TOKEN)
