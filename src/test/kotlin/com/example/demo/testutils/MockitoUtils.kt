package com.example.demo.testutils

import com.example.demo.persistence.entity.User
import org.mockito.kotlin.any
import org.springframework.web.multipart.MultipartFile

fun anyUser() = any<User>()

fun anyFile() = any<MultipartFile>()
