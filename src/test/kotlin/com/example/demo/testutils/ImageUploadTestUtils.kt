package com.example.demo.testutils

import com.example.demo.dto.ImageUploadResponse

fun mockImageUploadResponse() = ImageUploadResponse(IMAGE_LINK)
