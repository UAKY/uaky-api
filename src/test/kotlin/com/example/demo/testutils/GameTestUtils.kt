package com.example.demo.testutils

import com.example.demo.dto.GameResponseDto
import com.example.demo.persistence.entity.Game

fun mockPersistentGames() = listOf(
    mockPersistentGame(),
    mockPersistentGame(GAME_ID_FORTNITE, FORTNITE, IMAGE_URL_FORTNITE, IMAGE_URL_FORTNITE, IMAGE_URL_FORTNITE)
)

fun mockGameDtos() = listOf(
    mockGameDto(),
    mockGameDto(GAME_ID_FORTNITE, FORTNITE, IMAGE_URL_FORTNITE, IMAGE_URL_FORTNITE, IMAGE_URL_FORTNITE)
)

fun mockPersistentGame(
    testId: String = GAME_ID_DOTA_2,
    title: String = DOTA_2,
    icon: String = IMAGE_URL_DOTA_2,
    imageSmall: String = IMAGE_URL_DOTA_2,
    imageLarge: String = IMAGE_URL_DOTA_2,
) = Game(
    testId,
    title,
    icon,
    imageSmall,
    imageLarge,
)

fun mockGameDto(
    testId: String = GAME_ID_DOTA_2,
    title: String = DOTA_2,
    icon: String = IMAGE_URL_DOTA_2,
    imageSmall: String = IMAGE_URL_DOTA_2,
    imageLarge: String = IMAGE_URL_DOTA_2,
) = GameResponseDto(
    testId,
    title,
    icon,
    imageSmall,
    imageLarge,
)
