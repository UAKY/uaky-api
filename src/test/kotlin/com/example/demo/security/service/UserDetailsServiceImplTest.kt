package com.example.demo.security.service

import com.example.demo.constant.USER_NOT_FOUND_MSG
import com.example.demo.persistence.repository.UserRepository
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.assertJwtUsersEqual
import com.example.demo.testutils.mockJwtUser
import com.example.demo.testutils.mockPersistentUser
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.security.core.userdetails.UsernameNotFoundException

class UserDetailsServiceImplTest : AbstractUnitTest() {

    @Mock
    lateinit var userRepository: UserRepository

    @InjectMocks
    lateinit var userDetailsService: UserDetailsServiceImpl

    @Test
    fun `Should load by username`() {
        // GIVEN
        val userToReturn = mockPersistentUser()
        val expectedJwtUser = mockJwtUser()

        given(userRepository.findByEmail(EMAIL)).willReturn(userToReturn)

        // WHEN
        val actualJwtUser = userDetailsService.loadUserByUsername(EMAIL)

        // THEN
        assertJwtUsersEqual(actualJwtUser, expectedJwtUser)
    }

    @Test
    fun `Should throw UsernameNotFoundException if user not found`() {
        // GIVEN
        given(userRepository.findByEmail(EMAIL)).willReturn(null)

        // WHEN THEN
        assertThrows<UsernameNotFoundException>(USER_NOT_FOUND_MSG) { userDetailsService.loadUserByUsername(EMAIL) }
    }

}