package com.example.demo.security.token

import com.auth0.jwt.exceptions.JWTVerificationException
import com.example.demo.security.data.TokenScope
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.TOKEN
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import java.time.LocalDateTime

class AccessTokenValidatorTest : AbstractUnitTest() {

    @Mock
    lateinit var jwtTokenService: JwtTokenService

    @InjectMocks
    lateinit var accessTokenValidator: AccessTokenValidator

    @Test
    fun `Should return isValid=true if access token is valid`() {
        // GIVEN
        val validToken = TOKEN
        val tokenScope = TokenScope.ACCESS_TOKEN
        val timeInFuture = LocalDateTime.now().plusMinutes(1)

        given(jwtTokenService.extractExpiredAt(validToken)).willReturn(timeInFuture)
        given(jwtTokenService.extractScope(validToken)).willReturn(tokenScope)

        // WHEN
        val result = accessTokenValidator.isValid(validToken)

        // THEN
        assertThat(result).isTrue
    }

    @Test
    fun `Should return isValid=false if access token is empty`() {
        // GIVEN
        val emptyToken = ""

        // WHEN
        val result = accessTokenValidator.isValid(emptyToken)

        // THEN
        assertThat(result).isFalse
    }

    @Test
    fun `Should return isValid=false if access token is expired`() {
        // GIVEN
        val expiredToken = TOKEN
        val timeInPast = LocalDateTime.now().minusMinutes(1)

        given(jwtTokenService.extractExpiredAt(expiredToken)).willReturn(timeInPast)

        // WHEN
        val result = accessTokenValidator.isValid(expiredToken)

        // THEN
        assertThat(result).isFalse
    }

    @Test
    fun `Should return isValid=false if token is refresh token`() {
        // GIVEN
        val refreshToken = TOKEN
        val timeInFuture = LocalDateTime.now().plusMinutes(1)
        val tokenScope = TokenScope.REFRESH_TOKEN

        given(jwtTokenService.extractExpiredAt(refreshToken)).willReturn(timeInFuture)
        given(jwtTokenService.extractScope(refreshToken)).willReturn(tokenScope)

        // WHEN
        val result = accessTokenValidator.isValid(refreshToken)

        // THEN
        assertThat(result).isFalse
    }

    @Test
    fun `Should return isValid=false if JWTVerificationException was thrown`() {
        // GIVEN
        val invalidToken = TOKEN
        given(jwtTokenService.extractExpiredAt(invalidToken)).willThrow(JWTVerificationException(""))

        // WHEN
        val result = accessTokenValidator.isValid(invalidToken)

        // THEN
        assertThat(result).isFalse
    }

}