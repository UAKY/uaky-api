package com.example.demo.security.token

import com.example.demo.config.JwtConfig
import com.example.demo.security.data.TokenScope
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.EMAIL
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import java.time.LocalDateTime

internal class JwtTokenServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var jwtConfig: JwtConfig

    @InjectMocks
    lateinit var jwtTokenService: JwtTokenService

    @BeforeEach
    fun initJwtConfig() {
        given(jwtConfig.secret).willReturn("some-secret")
        given(jwtConfig.timeToLive).willReturn(1)
        given(jwtConfig.refreshTokenTimeToLive).willReturn(2)
    }

    @Test
    fun `Should create access token`() {
        // WHEN
        val accessToken = jwtTokenService.createAccessToken(EMAIL)

        // THEN
        assertThat(accessToken).isNotNull
    }

    @Test
    fun `Should create refresh token`() {
        // WHEN
        val refreshToken = jwtTokenService.createRefreshToken(EMAIL)

        // THEN
        assertThat(refreshToken).isNotNull
    }

    @Test
    fun `Should extract subject from token`() {
        // GIVEN
        val accessToken = jwtTokenService.createAccessToken(EMAIL)

        // WHEN
        val actualSubject = jwtTokenService.extractSubject(accessToken)

        // THEN
        assertThat(actualSubject).isEqualTo(EMAIL)
    }

    @Test
    fun `Should extract ACCESS_TOKEN scope from access token`() {
        // GIVEN
        val accessToken = jwtTokenService.createAccessToken(EMAIL)

        // WHEN
        val actualScope = jwtTokenService.extractScope(accessToken)

        // THEN
        assertThat(actualScope).isEqualTo(TokenScope.ACCESS_TOKEN)
    }

    @Test
    fun `Should extract expiredAt time from access token`() {
        // GIVEN
        val accessToken = jwtTokenService.createAccessToken(EMAIL)

        // WHEN
        val expiredAt = jwtTokenService.extractExpiredAt(accessToken)

        // THEN
        assertThat(expiredAt).isAfter(LocalDateTime.now().minusSeconds(jwtConfig.timeToLive))
    }

    @Test
    fun `Should extract expiredAt time from refresh token`() {
        // GIVEN
        val refreshToken = jwtTokenService.createRefreshToken(EMAIL)

        // WHEN
        val expiredAt = jwtTokenService.extractExpiredAt(refreshToken)

        // THEN
        assertThat(expiredAt).isAfter(LocalDateTime.now().minusSeconds(jwtConfig.refreshTokenTimeToLive))
    }

}