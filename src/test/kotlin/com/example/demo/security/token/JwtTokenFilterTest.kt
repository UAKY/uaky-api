package com.example.demo.security.token

import com.example.demo.constant.AUTH_HEADER
import com.example.demo.constant.AUTH_HEADER_PREFIX
import com.example.demo.security.authentication.AuthenticationTokenProvider
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.TOKEN
import com.example.demo.testutils.mockUserPasswordAuthTokenWithEmailPassword
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.any
import org.mockito.BDDMockito.anyString
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.verify
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtTokenFilterTest : AbstractUnitTest() {

    @Mock
    lateinit var authenticationTokenProvider: AuthenticationTokenProvider

    @Mock
    lateinit var tokenValidator: AccessTokenValidator

    @Mock
    lateinit var request: HttpServletRequest

    @Mock
    lateinit var response: HttpServletResponse

    @Mock
    lateinit var filterChain: FilterChain

    @Mock
    lateinit var securityContext: SecurityContext

    @InjectMocks
    lateinit var jwtTokenFilter: JwtTokenFilter

    @Test
    fun `Should resolve and validate token and set authentication`() {
        // GIVEN
        val header = "$AUTH_HEADER_PREFIX$TOKEN"
        val mockAuthToken = mockUserPasswordAuthTokenWithEmailPassword()

        SecurityContextHolder.setContext(securityContext)
        given(request.getHeader(AUTH_HEADER)).willReturn(header)
        given(tokenValidator.isValid(TOKEN)).willReturn(true)
        given(authenticationTokenProvider.getAuthentication(TOKEN)).willReturn(mockAuthToken)

        // WHEN
        jwtTokenFilter.doFilter(request, response, filterChain)

        // THEN
        verify(request, times(1)).getHeader(AUTH_HEADER)
        verify(authenticationTokenProvider, times(1)).getAuthentication(TOKEN)
        verify(securityContext, times(1)).authentication = mockAuthToken
        verify(filterChain, times(1)).doFilter(request, response)
    }

    @Test
    fun `Should not set authentication to context if authentication is null`() {
        // GIVEN
        val header = "$AUTH_HEADER_PREFIX$TOKEN"

        given(request.getHeader(AUTH_HEADER)).willReturn(header)
        given(tokenValidator.isValid(TOKEN)).willReturn(true)
        given(authenticationTokenProvider.getAuthentication(TOKEN)).willReturn(null)

        // WHEN
        jwtTokenFilter.doFilter(request, response, filterChain)

        // THEN
        verify(request, times(1)).getHeader(AUTH_HEADER)
        verify(authenticationTokenProvider, times(1)).getAuthentication(TOKEN)
        verify(securityContext, never()).setAuthentication(any())
        verify(filterChain, times(1)).doFilter(request, response)
    }

    @Test
    fun `Should not trying to get authentication if token is not valid`() {
        // GIVEN
        val header = "$AUTH_HEADER_PREFIX$TOKEN"

        given(request.getHeader(AUTH_HEADER)).willReturn(header)
        given(tokenValidator.isValid(TOKEN)).willReturn(false)

        // WHEN
        jwtTokenFilter.doFilter(request, response, filterChain)

        // THEN
        verify(request, times(1)).getHeader(AUTH_HEADER)
        verify(authenticationTokenProvider, never()).getAuthentication(anyString())
        verify(securityContext, never()).setAuthentication(any())
        verify(filterChain, times(1)).doFilter(request, response)
    }

}