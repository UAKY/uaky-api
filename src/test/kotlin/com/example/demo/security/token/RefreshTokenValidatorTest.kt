package com.example.demo.security.token

import com.example.demo.security.data.TokenScope
import com.example.demo.service.UserService
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.TOKEN
import com.example.demo.testutils.mockPersistentUser
import com.example.demo.testutils.mockPersistentUserWithToken
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import java.time.LocalDateTime

class RefreshTokenValidatorTest : AbstractUnitTest() {

    @Mock
    lateinit var jwtTokenService: JwtTokenService

    @Mock
    lateinit var userService: UserService

    @InjectMocks
    lateinit var refreshTokenValidator: RefreshTokenValidator

    @Test
    fun `Should return isValid=true if refresh token is valid`() {
        // GIVEN
        val validToken = TOKEN
        val tokenScope = TokenScope.REFRESH_TOKEN
        val timeInFuture = LocalDateTime.now().plusMinutes(1)
        val mockUserWithToken = mockPersistentUserWithToken()

        given(jwtTokenService.extractSubject(validToken)).willReturn(EMAIL)
        given(jwtTokenService.extractExpiredAt(validToken)).willReturn(timeInFuture)
        given(jwtTokenService.extractScope(validToken)).willReturn(tokenScope)
        given(userService.findUserByEmail(EMAIL)).willReturn(mockUserWithToken)

        // WHEN
        val result = refreshTokenValidator.isValid(validToken)

        // THEN
        assertThat(result).isTrue
    }

    @Test
    fun `Should return isValid=false if refresh token is empty`() {
        // GIVEN
        val emptyToken = ""

        // WHEN
        val result = refreshTokenValidator.isValid(emptyToken)

        // THEN
        assertThat(result).isFalse
    }

    @Test
    fun `Should return isValid=false if refresh token is expired`() {
        // GIVEN
        val expiredToken = TOKEN
        val timeInPast = LocalDateTime.now().minusMinutes(1)

        given(jwtTokenService.extractExpiredAt(expiredToken)).willReturn(timeInPast)

        // WHEN
        val result = refreshTokenValidator.isValid(expiredToken)

        // THEN
        assertThat(result).isFalse
    }

    @Test
    fun `Should return isValid=false if token is access token`() {
        // GIVEN
        val accessToken = TOKEN
        val timeInFuture = LocalDateTime.now().plusMinutes(1)
        val tokenScope = TokenScope.ACCESS_TOKEN

        given(jwtTokenService.extractExpiredAt(accessToken)).willReturn(timeInFuture)
        given(jwtTokenService.extractScope(accessToken)).willReturn(tokenScope)

        // WHEN
        val result = refreshTokenValidator.isValid(accessToken)

        // THEN
        assertThat(result).isFalse
    }

    @Test
    fun `Should return isValid=false if user don't owns that token`() {
        // GIVEN
        val validToken = TOKEN
        val tokenScope = TokenScope.REFRESH_TOKEN
        val timeInFuture = LocalDateTime.now().plusMinutes(1)
        val mockUserWithToken = mockPersistentUser()

        given(jwtTokenService.extractSubject(validToken)).willReturn(EMAIL)
        given(jwtTokenService.extractExpiredAt(validToken)).willReturn(timeInFuture)
        given(jwtTokenService.extractScope(validToken)).willReturn(tokenScope)
        given(userService.findUserByEmail(EMAIL)).willReturn(mockUserWithToken)

        // WHEN
        val result = refreshTokenValidator.isValid(validToken)

        // THEN
        assertThat(result).isFalse
    }
}