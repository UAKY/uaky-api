package com.example.demo.security.authentication

import com.example.demo.security.token.JwtTokenService
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.TOKEN
import com.example.demo.testutils.mockJwtUser
import com.example.demo.testutils.mockUserPasswordAuthTokenWithUser
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.security.core.userdetails.UserDetailsService

class AuthenticationTokenProviderTest : AbstractUnitTest() {

    @Mock
    lateinit var userDetailsService: UserDetailsService

    @Mock
    lateinit var jwtTokenService: JwtTokenService

    @InjectMocks
    lateinit var authenticationTokenProvider: AuthenticationTokenProvider

    @Test
    fun `Should return UsernamePasswordAuthenticationToken`() {
        // GIVEN
        val userDetails = mockJwtUser()
        val expectedAuthentication = mockUserPasswordAuthTokenWithUser()

        given(jwtTokenService.extractSubject(TOKEN)).willReturn(EMAIL)
        given(userDetailsService.loadUserByUsername(EMAIL)).willReturn(userDetails)

        // WHEN
        val actualAuthentication = authenticationTokenProvider.getAuthentication(TOKEN)

        // THEN
        assertThat(actualAuthentication).isEqualTo(expectedAuthentication)
    }

}