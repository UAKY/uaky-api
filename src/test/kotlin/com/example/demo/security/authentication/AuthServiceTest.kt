package com.example.demo.security.authentication

import com.example.demo.constant.AUTH_HEADER_PREFIX
import com.example.demo.constant.INVALID_TOKEN_MSG
import com.example.demo.dto.LoginRequestDto
import com.example.demo.dto.LoginResponseDto
import com.example.demo.exception.JwtAuthenticationException
import com.example.demo.security.token.JwtTokenService
import com.example.demo.security.token.RefreshTokenValidator
import com.example.demo.service.UserService
import com.example.demo.testconfig.AbstractUnitTest
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.PASSWORD
import com.example.demo.testutils.TOKEN
import com.example.demo.testutils.mockPersistentUser
import com.example.demo.testutils.mockUserPasswordAuthTokenWithEmailPassword
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.springframework.security.authentication.AuthenticationManager

class AuthServiceTest : AbstractUnitTest() {

    @Mock
    lateinit var authenticationManager: AuthenticationManager

    @Mock
    lateinit var jwtTokenService: JwtTokenService

    @Mock
    lateinit var userService: UserService

    @Mock
    lateinit var refreshTokenValidator: RefreshTokenValidator

    @InjectMocks
    lateinit var authService: AuthService

    @Test
    fun `Should authenticate, add refresh token to user and return tokens`() {
        // GIVEN
        val loginRequestDto = LoginRequestDto(EMAIL, PASSWORD)
        val user = mockPersistentUser()
        val mockAuthenticationToken = mockUserPasswordAuthTokenWithEmailPassword()
        val expectedResponseDto = LoginResponseDto(TOKEN, TOKEN)

        given(jwtTokenService.createAccessToken(EMAIL)).willReturn(TOKEN)
        given(jwtTokenService.createRefreshToken(EMAIL)).willReturn(TOKEN)
        given(userService.findUserByEmail(EMAIL)).willReturn(user)

        // WHEN
        val actualResponseDto = authService.authenticate(loginRequestDto)

        // THEN
        assertThat(actualResponseDto).isEqualTo(expectedResponseDto)
        assertThat(user.refreshTokens).contains(TOKEN)
        verify(authenticationManager, times(1)).authenticate(mockAuthenticationToken)
        verify(userService, times(1)).findUserByEmail(EMAIL)
    }

    @Test
    fun `Should refresh tokens, add new refresh token to user and return new tokens`() {
        // GIVEN
        val header = "$AUTH_HEADER_PREFIX$TOKEN"
        val user = mockPersistentUser()
        val newToken = "NEW $TOKEN"
        val expectedResponseDto = LoginResponseDto(TOKEN, newToken)

        given(jwtTokenService.createAccessToken(EMAIL)).willReturn(TOKEN)
        given(jwtTokenService.createRefreshToken(EMAIL)).willReturn(newToken)
        given(jwtTokenService.extractSubject(TOKEN)).willReturn(EMAIL)
        given(userService.findUserByEmail(EMAIL)).willReturn(user)
        given(refreshTokenValidator.isValid(TOKEN)).willReturn(true)

        // THEN
        val actualResponseDto = authService.refreshTokens(header)

        // THEN
        assertThat(actualResponseDto).isEqualTo(expectedResponseDto)
        assertThat(user.refreshTokens).contains(newToken)
        assertThat(user.refreshTokens).doesNotContain(TOKEN)
        verify(userService, times(1)).findUserByEmail(EMAIL)
    }

    @Test
    fun `Should throw JwtAuthenticationException if refresh token is not valid`() {
        // GIVEN
        val header = "$AUTH_HEADER_PREFIX$TOKEN"
        given(refreshTokenValidator.isValid(TOKEN)).willReturn(false)

        // WHEN THEN
        assertThrows<JwtAuthenticationException>(INVALID_TOKEN_MSG) { authService.refreshTokens(header) }
    }

}