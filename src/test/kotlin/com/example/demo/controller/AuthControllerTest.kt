package com.example.demo.controller

import com.example.demo.constant.AUTH_HEADER
import com.example.demo.constant.AUTH_HEADER_PREFIX
import com.example.demo.constant.VALIDATION_FAILED_MSG
import com.example.demo.security.authentication.AuthService
import com.example.demo.testconfig.AbstractWebTest
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.NOT_BLANK
import com.example.demo.testutils.PASSWORD
import com.example.demo.testutils.REFRESH_TOKEN
import com.example.demo.testutils.TOKEN
import com.example.demo.testutils.loginRequestDto
import com.example.demo.testutils.loginResponseDto
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(AuthController::class)
class AuthControllerTest : AbstractWebTest() {

    @MockBean
    lateinit var authService: AuthService

    @Test
    fun `Should return tokens after successful login`() {
        // GIVEN
        val loginRequestDto = loginRequestDto()
        val loginResponseDto = loginResponseDto()

        val requestBody = """
            {
                "email": "$EMAIL",
                "password": "$PASSWORD"
            }
        """

        val expectedResponseBody = """
            {
                "accessToken": "$TOKEN",
                "refreshToken": "$REFRESH_TOKEN"
            }
        """

        given(authService.authenticate(loginRequestDto)).willReturn(loginResponseDto)

        // WHEN
        val request = post("/v1/auth/login")
            .content(requestBody)
            .contentType(APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedResponseBody, true))

    }

    @Test
    fun `Should return 401 if credentials are wrong`() {
        // GIVEN
        val loginRequestDto = loginRequestDto()

        val requestBody = """
            {
                "email": "$EMAIL",
                "password": "$PASSWORD"
            }
        """

        val expectedResponseBody = """
            {
                "status": 401,
                "error": "Unauthorized",
                "message": "Wrong credentials"
            }
        """

        val errorMessage = "Wrong credentials"
        given(authService.authenticate(loginRequestDto)).willThrow(BadCredentialsException(errorMessage))

        // WHEN
        val request = post("/v1/auth/login")
            .content(requestBody)
            .contentType(APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isUnauthorized)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    fun `Should return 422 if email and password are empty`() {
        // GIVEN
        val requestBody = """
            {
                "email": "",
                "password": ""
            }
        """

        val expectedResponseBody = """
            {
                "status": 422,
                "error": "Unprocessable Entity",
                "message": "$VALIDATION_FAILED_MSG",
                "validationErrors": {
                    "password": [
                        "$NOT_BLANK"
                    ],
                    "email": [
                        "$NOT_BLANK"
                    ]
                }
            }
        """

        // WHEN
        val request = post("/v1/auth/login")
            .content(requestBody)
            .contentType(APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isUnprocessableEntity)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    fun `Should return tokens after successful tokens refreshing`() {
        // GIVEN
        val header = "$AUTH_HEADER_PREFIX$REFRESH_TOKEN"
        val loginResponseDto = loginResponseDto()

        val expectedResponseBody = """
            {
                "accessToken": "$TOKEN",
                "refreshToken": "$REFRESH_TOKEN"
            }
        """

        given(authService.refreshTokens(header)).willReturn(loginResponseDto)

        // WHEN
        val request = post("/v1/auth/refresh")
            .header(AUTH_HEADER, header)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    fun `Should return 400 if Auth header missed`() {
        // WHEN
        val request = post("/v1/auth/refresh")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isBadRequest)
        // TODO: specify error response text
    }

}
