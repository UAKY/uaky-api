package com.example.demo.controller

import com.example.demo.constant.VALIDATION_FAILED_MSG
import com.example.demo.dto.PageResult
import com.example.demo.dto.UserSearchResultDto
import com.example.demo.service.SearchService
import com.example.demo.testconfig.AbstractWebTest
import com.example.demo.testconfig.UserDetailsConfig
import com.example.demo.testutils.DESCRIPTION_DOTA
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.NICKNAME
import com.example.demo.testutils.PROFILE_LINK_DOTA
import com.example.demo.testutils.USER_ID
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.times
import org.mockito.BDDMockito.verify
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(CommonSearchController::class)
@ContextConfiguration(classes = [UserDetailsConfig::class])
class SearchControllerTest : AbstractWebTest() {

    @MockBean
    lateinit var searchService: SearchService

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should search users`() {
        // GIVEN
        val size = 5

        val pageResultDto = PageResult(
            pageSize = size,
            content = listOf(
                UserSearchResultDto(
                    USER_ID,
                    EMAIL,
                    NICKNAME,
                    PROFILE_LINK_DOTA,
                    DESCRIPTION_DOTA,
                    IMAGE_LINK
                )
            )
        )

        val expectedResponse = """
            {
              "pageSize": $size,
              "content": [
                {
                  "id": "$USER_ID",
                  "email": "$EMAIL",
                  "nickname": "$NICKNAME",
                  "profileLink": "$PROFILE_LINK_DOTA",
                  "description": "$DESCRIPTION_DOTA",
                  "image": "$IMAGE_LINK"
                }
              ]
            }
        """

        given(searchService.commonSearchByGameId(GAME_ID_DOTA_2, USER_ID, size)).willReturn(pageResultDto)

        // WHEN
        val request = get("/v1/common-search/$GAME_ID_DOTA_2?size=$size")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedResponse, true))
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 422 if 'size' is invalid`() {
        // GIVEN
        val size = 0

        val expectedResponseBody = """
            {
                "status": 422,
                "message": "$VALIDATION_FAILED_MSG",
                "error": "Unprocessable Entity",
                "validationErrors": {
                    "size": [
                        "must be greater than or equal to 5"
                    ]
                }
            }
        """

        // WHEN
        val request = get("/v1/common-search/$GAME_ID_DOTA_2?size=$size")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isUnprocessableEntity)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    fun `Should return 400 if 'size' is not integer`() {
        // GIVEN
        val size = "invalid"

        val expectedResponseBody = """
            {
                "status": 400,
                "message": "Value '$size' is invalid",
                "error": "Bad Request"
            }
        """

        // WHEN
        val request = get("/v1/common-search/$GAME_ID_DOTA_2?size=$size")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isBadRequest)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should clear views`() {
        // WHEN
        val request = delete("/v1/common-search/$GAME_ID_DOTA_2/views")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNoContent)

        verify(searchService, times(1)).clearViewsCommon(GAME_ID_DOTA_2, USER_ID)
    }

}
