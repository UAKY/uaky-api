package com.example.demo.controller

import com.example.demo.dto.ListWrapperDto
import com.example.demo.dto.UserLikeDto
import com.example.demo.dto.UserLikeEvent
import com.example.demo.service.LikeNotificationService
import com.example.demo.service.SseService
import com.example.demo.testconfig.AbstractWebTest
import com.example.demo.testconfig.UserDetailsConfig
import com.example.demo.testutils.DESCRIPTION_DOTA
import com.example.demo.testutils.DOTA_2
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.IMAGE_URL_FORTNITE
import com.example.demo.testutils.NICKNAME
import com.example.demo.testutils.PROFILE_LINK_DOTA
import com.example.demo.testutils.USER_ID
import org.junit.jupiter.api.Test
import org.mockito.kotlin.given
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter

@WebMvcTest(NotificationController::class)
@ContextConfiguration(classes = [UserDetailsConfig::class])
class NotificationControllerTest : AbstractWebTest() {

    @MockBean
    lateinit var sseService: SseService

    @MockBean
    lateinit var likeNotificationService: LikeNotificationService

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return SseEmitter`() {
        // GIVEN
        val mockEmitter = mock<SseEmitter>()
        given(sseService.createEmitter(USER_ID)).willReturn(mockEmitter)

        // WHEN
        val request = get("/v1/me/notifications/subscribe")

        // THEN
        mockMvc.perform(request).andExpect(status().isOk)
        verify(sseService).createEmitter(USER_ID)
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return list notifications`() {
        // GIVEN
        val expectedJson = """
        {
          "content": [
            {
              "type": "like",
              "content": {
                "id": "$USER_ID",
                "isMutual": true,
                "nickname": "$NICKNAME",
                "gameTitle": "$DOTA_2",
                "userImage": "$IMAGE_LINK",
                "description": "$DESCRIPTION_DOTA",
                "profileLink": "$PROFILE_LINK_DOTA",
                "gameLargeImage": "$IMAGE_URL_FORTNITE"
              }
            }
          ]
        }
        """

        val listWrapperDto: ListWrapperDto<Any> = ListWrapperDto(
            listOf(
                UserLikeEvent(
                    UserLikeDto(
                        id = USER_ID,
                        nickname = NICKNAME,
                        gameTitle = DOTA_2,
                        userImage = IMAGE_LINK,
                        profileLink = PROFILE_LINK_DOTA,
                        description = DESCRIPTION_DOTA,
                        gameLargeImage = IMAGE_URL_FORTNITE,
                        isMutual = true
                    )
                )
            )
        )

        given(likeNotificationService.getNotifications(USER_ID)).willReturn(listWrapperDto)

        // WHEN
        val request = get("/v1/me/notifications")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedJson, true))
    }

}
