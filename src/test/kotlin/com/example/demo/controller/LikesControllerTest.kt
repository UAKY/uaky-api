package com.example.demo.controller

import com.example.demo.dto.UserLikeDto
import com.example.demo.service.LikesService
import com.example.demo.testconfig.AbstractWebTest
import com.example.demo.testconfig.UserDetailsConfig
import com.example.demo.testutils.DESCRIPTION_DOTA
import com.example.demo.testutils.DESCRIPTION_FORTNITE
import com.example.demo.testutils.DOTA_2
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.FORTNITE
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.IMAGE_URL_FORTNITE
import com.example.demo.testutils.PROFILE_LINK_DOTA
import com.example.demo.testutils.PROFILE_LINK_FORNITE
import com.example.demo.testutils.USER_ID
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(LikesController::class)
@ContextConfiguration(classes = [UserDetailsConfig::class])
class LikesControllerTest : AbstractWebTest() {

    @MockBean
    lateinit var likesService: LikesService

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should add like to user`() {
        // GIVEN
        val userIdToLike = "3a06c636-a90c-48cb-84f0-5b25506cb262"

        // WHEN
        val request = post("/v1/users/$userIdToLike/likes/$GAME_ID_DOTA_2")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isCreated)

        verify(likesService, times(1)).addLikeToUser(userIdToLike, USER_ID, GAME_ID_DOTA_2)
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return user likes`() {
        // GIVEN
        val userLikeDtos = listOf(
            UserLikeDto(
                id = "ab01a2e1-130e-4cc1-bf84-9ece3b1f856c",
                nickname = "relative",
                gameTitle = DOTA_2,
                userImage = IMAGE_LINK,
                profileLink = PROFILE_LINK_DOTA,
                description = DESCRIPTION_DOTA,
                gameLargeImage = IMAGE_URL_FORTNITE,
            ),
            UserLikeDto(
                id = "fe077ce5-525e-439b-9dbf-afc5f902e2b6",
                nickname = "quantity",
                gameTitle = FORTNITE,
                userImage = IMAGE_LINK,
                profileLink = PROFILE_LINK_FORNITE,
                description = DESCRIPTION_FORTNITE,
                gameLargeImage = IMAGE_URL_FORTNITE,
            )
        )

        val expectedResponse = """
            [
              {
                "id": "ab01a2e1-130e-4cc1-bf84-9ece3b1f856c",
                "nickname": "relative",
                "gameTitle": "$DOTA_2",
                "userImage": "$IMAGE_LINK",
                "profileLink": "$PROFILE_LINK_DOTA",
                "description": "$DESCRIPTION_DOTA",
                "gameLargeImage": "$IMAGE_URL_FORTNITE"
              },
              {
                "id": "fe077ce5-525e-439b-9dbf-afc5f902e2b6",
                "nickname": "quantity",
                "gameTitle": "$FORTNITE",
                "userImage": "$IMAGE_LINK",
                "profileLink": "$PROFILE_LINK_FORNITE",
                "description": "$DESCRIPTION_FORTNITE",
                "gameLargeImage": "$IMAGE_URL_FORTNITE"
              }
            ]
        """

        given(likesService.getUserLikes(USER_ID)).willReturn(userLikeDtos)

        // WHEN
        val request = get("/v1/me/likes")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedResponse, true))

        verify(likesService, times(1)).getUserLikes(USER_ID)
    }

}
