package com.example.demo.controller

import com.example.demo.constant.enums.UploadFolderName
import com.example.demo.exception.InvalidImageFormatException
import com.example.demo.service.CloudinaryService
import com.example.demo.testconfig.AbstractWebTest
import com.example.demo.testconfig.UserDetailsConfig
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.anyFile
import com.example.demo.testutils.mockImageUploadResponse
import org.junit.jupiter.api.Test
import org.mockito.kotlin.eq
import org.mockito.kotlin.given
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.web.multipart.MaxUploadSizeExceededException

@WebMvcTest(ImageUploadController::class)
@ContextConfiguration(classes = [UserDetailsConfig::class])
class ImageUploadControllerTest : AbstractWebTest() {

    @MockBean
    lateinit var cloudinaryService: CloudinaryService

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should upload image`() {
        // GIVEN
        val fileName = "image.jpg"
        val fileBytes = fileName.toByteArray()
        val file = MockMultipartFile("file", fileName, null, fileBytes)

        val imageUploadResponse = mockImageUploadResponse()

        given(cloudinaryService.upload(eq(UploadFolderName.USERS), anyFile())).willReturn(imageUploadResponse)

        val expectedResponse = """
            {
              "imageLink": "$IMAGE_LINK"
            }
        """

        // WHEN
        val request = multipart("/v1/images/users").file(file)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedResponse, true))
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 400 if image has invalid extension`() {
        // GIVEN
        val fileName = "image.gif"
        val file = MockMultipartFile("file", fileName, null, fileName.toByteArray())

        val expectedResponse = """
            {
              "status": 400,
              "message": "Unsupported image format",
              "error": "Bad Request"
            }
        """

        given(cloudinaryService.upload(eq(UploadFolderName.USERS), anyFile())).willThrow(InvalidImageFormatException())

        // WHEN
        val request = multipart("/v1/images/users").file(file)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isBadRequest)
            .andExpect(content().json(expectedResponse, true))
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 413 if file is too large`() {
        // GIVEN
        val fileName = "image.gif"
        val file = MockMultipartFile("file", fileName, null, fileName.toByteArray())

        val expectedResponse = """
            {
              "status": 413,
              "message": "Maximum upload size exceeded",
              "error": "Payload Too Large"
            }
        """

        given(cloudinaryService.upload(eq(UploadFolderName.USERS), anyFile())).willThrow(MaxUploadSizeExceededException::class.java)

        // WHEN
        val request = multipart("/v1/images/users").file(file)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isPayloadTooLarge)
            .andExpect(content().json(expectedResponse, true))

    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 400 if folder name is invalid`() {
        // GIVEN
        val fileName = "image.gif"
        val file = MockMultipartFile("file", fileName, null, fileName.toByteArray())

        val invalidFolder = "invalidFolder"

        val expectedResponse = """
            {
              "status": 400,
              "message": "Value '$invalidFolder' is invalid",
              "error": "Bad Request"
            }
        """

        // WHEN
        val request = multipart("/v1/images/$invalidFolder").file(file)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isBadRequest)
            .andExpect(content().json(expectedResponse, true))
    }

}
