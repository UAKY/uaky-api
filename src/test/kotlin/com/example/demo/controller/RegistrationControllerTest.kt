package com.example.demo.controller

import com.example.demo.constant.INVALID_GAME_ID_MSG
import com.example.demo.constant.INVALID_PASSWORD
import com.example.demo.constant.VALIDATION_FAILED_MSG
import com.example.demo.persistence.repository.GameRepository
import com.example.demo.persistence.repository.UserRepository
import com.example.demo.service.UserService
import com.example.demo.testconfig.AbstractWebTest
import com.example.demo.testutils.DESCRIPTION_DOTA
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.EMAIL_INVALID
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.GAME_ID_INVALID
import com.example.demo.testutils.NICKNAME
import com.example.demo.testutils.PASSWORD
import com.example.demo.testutils.PASSWORD_INVALID
import com.example.demo.testutils.PROFILE_LINK_DOTA
import com.example.demo.testutils.mockUserRegistrationDto
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(RegistrationController::class)
class RegistrationControllerTest : AbstractWebTest() {

    @MockBean
    lateinit var userRepository: UserRepository

    @MockBean
    lateinit var gameRepository: GameRepository

    @MockBean
    lateinit var userService: UserService

    @Test
    fun `Should register user`() {
        // GIVEN
        val userRegistrationDto = mockUserRegistrationDto()

        val requestBody = """
            {
              "email": "$EMAIL",
              "password": "$PASSWORD",
              "nickname": "$NICKNAME",
              "games": [
                {
                  "id": "$GAME_ID_DOTA_2",
                  "profileLink": "$PROFILE_LINK_DOTA",
                  "description": "$DESCRIPTION_DOTA"
                }
              ]
            }
        """

        given(gameRepository.existsById(GAME_ID_DOTA_2)).willReturn(true)

        // WHEN
        val request = post("/v1/register")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNoContent)

        verify(userService, times(1)).register(userRegistrationDto)
    }

    @Test
    fun `Should return 422 if email and password and game id are invalid`() {
        // GIVEN
        val requestBody = """
            {
              "email": "$EMAIL_INVALID",
              "password": "$PASSWORD_INVALID",
              "nickname": "$NICKNAME",
              "games": [
                {
                  "id": "$GAME_ID_INVALID",
                  "profileLink": "$PROFILE_LINK_DOTA",
                  "description": "$DESCRIPTION_DOTA"
                }
              ]
            }
        """

        val expectedResponseBody = """
            {
                "status": 422,
                "message": "$VALIDATION_FAILED_MSG",
                "error": "Unprocessable Entity",
                "validationErrors": {
                    "password": [
                        "$INVALID_PASSWORD"
                    ],
                    "email": [
                        "must be a well-formed email address"
                    ],
                    "games[0].id": [
                        "$INVALID_GAME_ID_MSG"
                    ]
                }
            }
        """

        given(gameRepository.existsById(GAME_ID_INVALID)).willReturn(false)

        // WHEN
        val request = post("/v1/register")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isUnprocessableEntity)
            .andExpect(content().json(expectedResponseBody, true))
    }
}