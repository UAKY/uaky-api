package com.example.demo.controller

import com.example.demo.constant.DUPLICATE_GAME_ID
import com.example.demo.constant.GAME_NOT_FOUND_MSG
import com.example.demo.constant.INVALID_GAME_ID_MSG
import com.example.demo.constant.PASSWORDS_NOT_EQUALS_MSG
import com.example.demo.constant.VALIDATION_FAILED_MSG
import com.example.demo.constant.WRONG_PASSWORD_MSG
import com.example.demo.exception.DuplicateEntryException
import com.example.demo.exception.GameNotFoundException
import com.example.demo.persistence.repository.GameRepository
import com.example.demo.service.GameUserService
import com.example.demo.service.UserService
import com.example.demo.testconfig.AbstractWebTest
import com.example.demo.testconfig.UserDetailsConfig
import com.example.demo.testutils.DESCRIPTION_DOTA
import com.example.demo.testutils.DESCRIPTION_FORTNITE
import com.example.demo.testutils.DOTA_2
import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.IMAGE_LINK
import com.example.demo.testutils.NICKNAME
import com.example.demo.testutils.NOT_BLANK
import com.example.demo.testutils.PASSWORD
import com.example.demo.testutils.PASSWORD_NEW
import com.example.demo.testutils.PASSWORD_WRONG
import com.example.demo.testutils.PROFILE_LINK_DOTA
import com.example.demo.testutils.PROFILE_LINK_FORNITE
import com.example.demo.testutils.USER_ID
import com.example.demo.testutils.mockGameUserDto
import com.example.demo.testutils.mockGameUserUpdateDto
import com.example.demo.testutils.mockUserChangePasswordDto
import com.example.demo.testutils.mockUserResponseDto
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.anyString
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.times
import org.mockito.BDDMockito.verify
import org.mockito.kotlin.any
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(MeController::class)
@ContextConfiguration(classes = [UserDetailsConfig::class])
class MeControllerTest : AbstractWebTest() {

    @MockBean
    lateinit var gameRepository: GameRepository

    @MockBean
    lateinit var userService: UserService

    @MockBean
    lateinit var gameUserService: GameUserService

    @MockBean
    lateinit var passwordEncoder: PasswordEncoder

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return user info for 'me' endpoint`() {
        // GIVEN
        val userResponseDto = mockUserResponseDto()

        given(userService.findById(USER_ID)).willReturn(userResponseDto)

        val expectedResponse = """
            {
                "id": "$USER_ID",
                "email": "$EMAIL",
                "nickname": "$NICKNAME",
                "roles": [
                    "ROLE_USER"
                ],
                "games": [
                    {
                        "id": "$GAME_ID_DOTA_2",
                        "title": "$DOTA_2",
                        "profileLink": "$PROFILE_LINK_DOTA",
                        "description": "$DESCRIPTION_DOTA"
                    }
                ],
                "openedStatus": true,
                "image": "$IMAGE_LINK"
            }
        """

        // WHEN
        val request = get("/v1/me")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedResponse, true))

        verify(userService, times(1)).findById(USER_ID)
    }

    @Test
    fun `Should return 403 if not authenticated user tries to get access to secured endpoint`() {
        // WHEN
        val request = get("/v1/me")

        val expectedResponseBody = """
            {
                "status": 403,
                "message": "Access is denied",
                "error": "Forbidden"
            }
        """

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isForbidden)
            .andExpect(content().json(expectedResponseBody, true))

        verify(userService, times(0)).findById(any())
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 204 if nickname was changed`() {
        // GIVEN
        val requestBody = """
            {
                "nickname": "$NICKNAME"
            }
        """

        // WHEN
        val request = patch("/v1/me/nickname")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNoContent)

        verify(userService, times(1)).changeNickname(any(), any())
    }

    @Test
    fun `Should return 422 if nickname is empty`() {
        // GIVEN
        val requestBody = """
            {
                "nickname": ""
            }
        """

        val expectedResponseBody = """
            {
                "status": 422,
                "message": "$VALIDATION_FAILED_MSG",
                "error": "Unprocessable Entity",
                "validationErrors": {
                    "nickname": [
                        "$NOT_BLANK"
                    ]
                }
            }
        """

        // WHEN
        val request = patch("/v1/me/nickname")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isUnprocessableEntity)
            .andExpect(content().json(expectedResponseBody, true))

        verify(userService, times(0)).changeNickname(any(), any())
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 204 if game was added`() {
        // GIVEN
        val gameUserDto = mockGameUserDto()

        val requestBody = """
            {
              "id": "$GAME_ID_DOTA_2",
              "profileLink": "$PROFILE_LINK_DOTA",
              "description": "$DESCRIPTION_DOTA"
            }
        """

        given(gameRepository.existsById(anyString())).willReturn(true)

        // WHEN
        val request = post("/v1/me/games")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNoContent)

        verify(gameUserService, times(1)).addGame(USER_ID, gameUserDto)
    }

    @Test
    fun `Should return 422 if game id is invalid`() {
        // GIVEN
        val requestBody = """
            {
              "id": "3d7dd97d-661e-4b1a-8e37-b9e1632a2f56",
              "profileLink": "https://epic.com/olxmute",
              "description": "Looking for teammate for Dota 2"
            }
        """

        val expectedResponseBody = """
            {
                "status": 422,
                "message": "$VALIDATION_FAILED_MSG",
                "error": "Unprocessable Entity",
                "validationErrors": {
                    "id": [
                        "$INVALID_GAME_ID_MSG"
                    ]
                }
            }
        """

        // WHEN
        val request = post("/v1/me/games")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isUnprocessableEntity)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 409 if game id is duplicated`() {
        // GIVEN
        val gameUserDto = mockGameUserDto()

        val requestBody = """
            {
              "id": "$GAME_ID_DOTA_2",
              "profileLink": "$PROFILE_LINK_DOTA",
              "description": "$DESCRIPTION_DOTA"
            }
        """

        val expectedResponseBody = """
            {
                "status": 409,
                "message": "$DUPLICATE_GAME_ID",
                "error": "Conflict"
            }
        """

        given(gameRepository.existsById(GAME_ID_DOTA_2)).willReturn(true)
        given(gameUserService.addGame(USER_ID, gameUserDto)).willThrow(DuplicateEntryException(DUPLICATE_GAME_ID))

        // WHEN
        val request = post("/v1/me/games")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isConflict)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 204 if game was removed from user`() {
        // GIVEN WHEN
        val request = delete("/v1/me/games/$GAME_ID_DOTA_2")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNoContent)

        verify(gameUserService, times(1)).removeGame(USER_ID, GAME_ID_DOTA_2)
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 404 if game not found on remove`() {
        // GIVEN
        given(gameUserService.removeGame(USER_ID, GAME_ID_DOTA_2)).willThrow(GameNotFoundException())

        val expectedResponseBody = """
            {
                "status": 404,
                "message": "$GAME_NOT_FOUND_MSG",
                "error": "Not Found"
            }
        """

        // WHEN
        val request = delete("/v1/me/games/$GAME_ID_DOTA_2")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNotFound)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 204 if game was updated in user`() {
        // GIVEN
        val gameUserUpdateDto = mockGameUserUpdateDto()

        val requestBody = """
            {
              "profileLink": "$PROFILE_LINK_FORNITE",
              "description": "$DESCRIPTION_FORTNITE"
            }
        """

        // WHEN
        val request = put("/v1/me/games/$GAME_ID_DOTA_2")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNoContent)

        verify(gameUserService, times(1)).editGame(USER_ID, GAME_ID_DOTA_2, gameUserUpdateDto)
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 404 if game not found on update`() {
        // GIVEN
        val gameUserUpdateDto = mockGameUserUpdateDto()

        val requestBody = """
            {
              "profileLink": "$PROFILE_LINK_FORNITE",
              "description": "$DESCRIPTION_FORTNITE"
            }
        """

        val expectedResponseBody = """
            {
                "status": 404,
                "message": "$GAME_NOT_FOUND_MSG",
                "error": "Not Found"
            }
        """

        given(gameUserService.editGame(USER_ID, GAME_ID_DOTA_2, gameUserUpdateDto)).willThrow(GameNotFoundException())

        // WHEN
        val request = put("/v1/me/games/$GAME_ID_DOTA_2")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(content().json(expectedResponseBody, true))
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 204 if account status was switched in user`() {
        // GIVEN WHEN
        val request = patch("/v1/me/status")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNoContent)

        verify(userService, times(1)).switchAccountStatus(USER_ID)
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 204 if account password was changed in user`() {
        // GIVEN
        val userChangePasswordDto = mockUserChangePasswordDto()

        val requestBody = """
            {
              "oldPassword": "$PASSWORD",
              "newPassword": "$PASSWORD_NEW",
              "repeatedNewPassword": "$PASSWORD_NEW"
            }
        """

        given(passwordEncoder.matches(PASSWORD, PASSWORD)).willReturn(true)

        // WHEN
        val request = patch("/v1/me/password")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNoContent)

        verify(userService, times(1)).changePassword(USER_ID, userChangePasswordDto)
        verify(passwordEncoder, times(1)).matches(PASSWORD, PASSWORD)
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 422 if passwords are not equals`() {
        // GIVEN
        val requestBody = """
            {
              "oldPassword": "$PASSWORD",
              "newPassword": "$PASSWORD_NEW",
              "repeatedNewPassword": "$PASSWORD_WRONG"
            }
        """

        given(passwordEncoder.matches(PASSWORD, PASSWORD)).willReturn(true)

        val expectedResponseBody = """
            {
                "status": 422,
                "message": "$VALIDATION_FAILED_MSG",
                "error": "Unprocessable Entity",
                "validationErrors": {
                    "repeatedNewPassword": [
                        "$PASSWORDS_NOT_EQUALS_MSG"
                    ]
                }
            }
        """

        // WHEN
        val request = patch("/v1/me/password")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isUnprocessableEntity)
            .andExpect(content().json(expectedResponseBody, true))

        verify(passwordEncoder, times(1)).matches(PASSWORD, PASSWORD)
    }

    @Test
    @WithUserDetails(value = EMAIL)
    fun `Should return 422 if wrong password on change password`() {
        // GIVEN
        val requestBody = """
            {
              "oldPassword": "$PASSWORD_WRONG",
              "newPassword": "$PASSWORD_NEW",
              "repeatedNewPassword": "$PASSWORD_NEW"
            }
        """

        val expectedResponseBody = """
            {
                "status": 422,
                "message": "$VALIDATION_FAILED_MSG",
                "error": "Unprocessable Entity",
                "validationErrors": {
                    "oldPassword": [
                        "$WRONG_PASSWORD_MSG"
                    ]
                }
            }
        """

        // WHEN
        val request = patch("/v1/me/password")
            .content(requestBody)
            .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isUnprocessableEntity)
            .andExpect(content().json(expectedResponseBody, true))
    }
}
