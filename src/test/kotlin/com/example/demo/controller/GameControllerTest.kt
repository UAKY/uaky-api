package com.example.demo.controller

import com.example.demo.exception.GameNotFoundException
import com.example.demo.service.GameService
import com.example.demo.testconfig.AbstractWebTest
import com.example.demo.testutils.DOTA_2
import com.example.demo.testutils.FORTNITE
import com.example.demo.testutils.GAME_ID_DOTA_2
import com.example.demo.testutils.GAME_ID_FORTNITE
import com.example.demo.testutils.IMAGE_URL_DOTA_2
import com.example.demo.testutils.IMAGE_URL_FORTNITE
import com.example.demo.testutils.mockGameDto
import com.example.demo.testutils.mockGameDtos
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(GameController::class)
class GameControllerTest : AbstractWebTest() {

    @MockBean
    lateinit var gameService: GameService

    @Test
    fun `Should find game by id`() {
        // GIVEN
        val game = mockGameDto()

        val expectedResponse = """
            {
                "id": "$GAME_ID_DOTA_2",
                "title": "$DOTA_2",
                "icon": "$IMAGE_URL_DOTA_2",
                "imageSmall": "$IMAGE_URL_DOTA_2",
                "imageLarge": "$IMAGE_URL_DOTA_2"
            }
        """

        given(gameService.findGameById(GAME_ID_DOTA_2)).willReturn(game)

        // WHEN
        val request = get("/v1/game/$GAME_ID_DOTA_2")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedResponse, true))
    }

    @Test
    fun `Should return 404 if game not found`() {
        // GIVEN
        given(gameService.findGameById(GAME_ID_DOTA_2)).willThrow(GameNotFoundException())

        // WHEN
        val request = get("/v1/game/$GAME_ID_DOTA_2")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isNotFound)
    }

    @Test
    fun `Should find all games`() {
        // GIVEN
        val games = mockGameDtos()

        val expectedResponse = """
            [
                {
                    "id": "$GAME_ID_DOTA_2",
                    "title": "$DOTA_2",
                    "icon": "$IMAGE_URL_DOTA_2",
                    "imageSmall": "$IMAGE_URL_DOTA_2",
                    "imageLarge": "$IMAGE_URL_DOTA_2"
                },
                {
                    "id": "$GAME_ID_FORTNITE",
                    "title": "$FORTNITE",
                    "icon": "$IMAGE_URL_FORTNITE",
                    "imageSmall": "$IMAGE_URL_FORTNITE",
                    "imageLarge": "$IMAGE_URL_FORTNITE"
                }
            ]
        """

        given(gameService.findAllGames()).willReturn(games)

        // WHEN
        val request = get("/v1/game")

        // THEN
        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(content().json(expectedResponse, true))
    }

}