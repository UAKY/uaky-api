package com.example.demo.testconfig

import com.example.demo.security.authentication.AuthenticationTokenProvider
import com.example.demo.security.token.AccessTokenValidator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc

abstract class AbstractWebTest {

    @MockBean
    lateinit var authenticationTokenProvider: AuthenticationTokenProvider

    @MockBean
    lateinit var accessTokenValidator: AccessTokenValidator

    @Autowired
    lateinit var mockMvc: MockMvc

}