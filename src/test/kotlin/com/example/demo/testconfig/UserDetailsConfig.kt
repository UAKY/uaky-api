package com.example.demo.testconfig

import com.example.demo.testutils.EMAIL
import com.example.demo.testutils.mockJwtUser
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.UserDetailsService

@TestConfiguration
class UserDetailsConfig {

    @Bean
    @Primary
    fun userDetailsService(): UserDetailsService {
        val db = mapOf(
            EMAIL to mockJwtUser()
        )

        return UserDetailsService { username -> db.getValue(username) }
    }

}