package com.example.demo.steps

import com.example.demo.dto.GameUserCreateDto
import com.example.demo.dto.GameUserUpdateDto
import com.example.demo.dto.UserChangeImageRequestDto
import com.example.demo.dto.UserChangeNicknameRequestDto
import com.example.demo.dto.UserChangePasswordDto
import com.example.demo.dto.UserRegistrationDto
import com.example.demo.dto.UserResponseDto
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testutils.contentJson
import com.example.demo.testutils.expectStatusIsNoContent
import com.example.demo.testutils.expectStatusIsOk
import com.example.demo.testutils.extractResponse
import com.example.demo.testutils.withToken
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

fun AbstractIntegrationTest.registerUser(userRegistrationDto: UserRegistrationDto) {
    val request = post("/v1/register")
        .contentJson(userRegistrationDto)

    mockMvc
        .perform(request)
        .expectStatusIsNoContent()
}

fun AbstractIntegrationTest.getUserAccount(accessToken: String): UserResponseDto {
    return getUserAccountResultActions(accessToken)
        .expectStatusIsOk()
        .extractResponse()
}

fun AbstractIntegrationTest.getUserAccountResultActions(accessToken: String): ResultActions {
    val request = get("/v1/me")
        .withToken(accessToken)

    return mockMvc.perform(request)
}

fun AbstractIntegrationTest.changeNickname(accessToken: String, nickname: String) {
    val request = patch("/v1/me/nickname")
        .contentJson(UserChangeNicknameRequestDto(nickname))
        .withToken(accessToken)

    mockMvc.perform(request).expectStatusIsNoContent()
}

fun AbstractIntegrationTest.changePassword(accessToken: String, oldPassword: String, newPassword: String) {
    val request = patch("/v1/me/password")
        .contentJson(UserChangePasswordDto(oldPassword, newPassword, newPassword))
        .withToken(accessToken)

    mockMvc.perform(request).expectStatusIsNoContent()
}

fun AbstractIntegrationTest.changeStatus(accessToken: String) {
    val request = patch("/v1/me/status")
        .withToken(accessToken)

    mockMvc.perform(request).expectStatusIsNoContent()
}

fun AbstractIntegrationTest.changeImage(accessToken: String, image: String) {
    val request = patch("/v1/me/image")
        .contentJson(UserChangeImageRequestDto(image))
        .withToken(accessToken)

    mockMvc.perform(request).expectStatusIsNoContent()
}

fun AbstractIntegrationTest.removeGame(accessToken: String, gameId: String) {
    val request = delete("/v1/me/games/$gameId")
        .withToken(accessToken)

    mockMvc.perform(request).expectStatusIsNoContent()
}

fun AbstractIntegrationTest.addGame(accessToken: String, gameId: String, profileLink: String, description: String?) {
    val request = post("/v1/me/games")
        .contentJson(GameUserCreateDto(gameId, profileLink, description))
        .withToken(accessToken)

    mockMvc.perform(request).expectStatusIsNoContent()
}

fun AbstractIntegrationTest.editGame(accessToken: String, gameId: String, profileLink: String, description: String) {
    val request = put("/v1/me/games/$gameId")
        .contentJson(GameUserUpdateDto(profileLink, description))
        .withToken(accessToken)

    mockMvc.perform(request).expectStatusIsNoContent()
}

fun AbstractIntegrationTest.updateLastVisitedDate(accessToken: String) {
    val request = patch("/v1/me/last-visited-date")
        .withToken(accessToken)

    mockMvc.perform(request).expectStatusIsNoContent()
}
