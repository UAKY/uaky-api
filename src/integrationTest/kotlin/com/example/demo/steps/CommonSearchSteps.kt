package com.example.demo.steps

import com.example.demo.dto.PageResult
import com.example.demo.dto.UserSearchResultDto
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testutils.expectStatusIsNoContent
import com.example.demo.testutils.expectStatusIsOk
import com.example.demo.testutils.extractResponse
import com.example.demo.testutils.withToken
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

fun AbstractIntegrationTest.commonSearchByGameId(
    accessToken: String,
    gameId: String,
    size: Int
): PageResult<UserSearchResultDto> {

    val request = get("/v1/common-search/$gameId")
        .param("size", size.toString())
        .withToken(accessToken)

    return mockMvc
        .perform(request)
        .expectStatusIsOk()
        .extractResponse()
}

fun AbstractIntegrationTest.clearCommonViews(accessToken: String, gameId: String) {
    val request = delete("/v1/common-search/$gameId/views")
        .withToken(accessToken)

    mockMvc
        .perform(request)
        .expectStatusIsNoContent()
}
