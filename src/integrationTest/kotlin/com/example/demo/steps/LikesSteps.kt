package com.example.demo.steps

import com.example.demo.dto.UserLikeDto
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testutils.expectStatusIsCreated
import com.example.demo.testutils.expectStatusIsOk
import com.example.demo.testutils.extractResponse
import com.example.demo.testutils.withToken
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

fun AbstractIntegrationTest.likeUser(accessToken: String, userId: String, gameId: String) {
    val request = post("/v1/users/$userId/likes/$gameId")
        .withToken(accessToken)

    mockMvc.perform(request)
        .expectStatusIsCreated()
}

fun AbstractIntegrationTest.getLikes(accessToken: String): List<UserLikeDto> {
    val request = get("/v1/me/likes")
        .withToken(accessToken)

    return mockMvc.perform(request)
        .expectStatusIsOk()
        .extractResponse()
}