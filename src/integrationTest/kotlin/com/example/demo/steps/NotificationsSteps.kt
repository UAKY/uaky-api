package com.example.demo.steps

import com.example.demo.dto.ListWrapperDto
import com.example.demo.dto.UserLikeEvent
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testutils.expectStatusIsOk
import com.example.demo.testutils.extractResponse
import com.example.demo.testutils.withToken
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

fun AbstractIntegrationTest.getNotifications(accessToken: String): ListWrapperDto<UserLikeEvent> {
    val request = get("/v1/me/notifications")
        .withToken(accessToken)

    return mockMvc.perform(request)
        .expectStatusIsOk()
        .extractResponse()
}
