package com.example.demo.steps

import com.example.demo.dto.LoginRequestDto
import com.example.demo.dto.LoginResponseDto
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testutils.contentJson
import com.example.demo.testutils.expectStatusIsOk
import com.example.demo.testutils.extractResponse
import com.example.demo.testutils.withToken
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

fun AbstractIntegrationTest.loginUser(email: String, password: String): LoginResponseDto {
    return loginUser(LoginRequestDto(email, password))
}

fun AbstractIntegrationTest.loginUser(loginRequestDto: LoginRequestDto): LoginResponseDto {
    return loginUserResultActions(loginRequestDto)
        .expectStatusIsOk()
        .extractResponse()
}

fun AbstractIntegrationTest.loginUserResultActions(loginRequestDto: LoginRequestDto): ResultActions {
    val request = post("/v1/auth/login")
        .contentJson(loginRequestDto)

    return mockMvc.perform(request)
}

fun AbstractIntegrationTest.refreshToken(refreshToken: String): LoginResponseDto {
    return refreshTokenResultActions(refreshToken)
        .expectStatusIsOk()
        .extractResponse()
}

fun AbstractIntegrationTest.refreshTokenResultActions(refreshToken: String): ResultActions {
    val request = post("/v1/auth/refresh")
        .withToken(refreshToken)

    return mockMvc.perform(request)
}