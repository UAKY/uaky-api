package com.example.demo

import com.example.demo.dto.GameUserCreateDto
import com.example.demo.dto.LoginRequestDto
import com.example.demo.dto.UserRegistrationDto
import com.example.demo.steps.addGame
import com.example.demo.steps.changeImage
import com.example.demo.steps.changeNickname
import com.example.demo.steps.changePassword
import com.example.demo.steps.changeStatus
import com.example.demo.steps.editGame
import com.example.demo.steps.getUserAccount
import com.example.demo.steps.getUserAccountResultActions
import com.example.demo.steps.loginUser
import com.example.demo.steps.loginUserResultActions
import com.example.demo.steps.refreshToken
import com.example.demo.steps.refreshTokenResultActions
import com.example.demo.steps.registerUser
import com.example.demo.steps.removeGame
import com.example.demo.steps.updateLastVisitedDate
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testutils.expectStatusIsForbidden
import com.example.demo.testutils.expectStatusIsOk
import com.example.demo.testutils.expectStatusIsUnauthorized
import com.example.demo.testutils.sleepSeconds
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.date.shouldBeAfter
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldNotBeBlank
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Value
import java.lang.Integer.MAX_VALUE

class UserAccountScenarios : AbstractIntegrationTest() {

    @Value("\${jwt-token.time-to-live}")
    private var ttl = 0

    private val loginRequestDto = LoginRequestDto(
        email = "user.email@gmail.com",
        password = "Password@123",
    )

    // Given new User registered
    // And logged in
    // When got access to /me endpoint
    // Then should see his personal info
    @Test
    @Order(1)
    fun `Should register user`() {
        val userRegistrationDto = UserRegistrationDto(
            email = "user.email@gmail.com",
            password = "Password@123",
            nickname = "user-nickname",
            games = listOf(
                GameUserCreateDto(
                    id = "00f6f50c-49d0-4564-99a3-eadbc15b5e00",
                    profileLink = "https://steam.com/my-dota",
                    description = "Looking for teammate for Dota 2"
                )
            )
        )

        registerUser(userRegistrationDto)

        val (accessToken) = loginUser(loginRequestDto)

        val userAccount = getUserAccount(accessToken)

        userAccount.apply {
            nickname shouldBe "user-nickname"
            openedStatus shouldBe true
            image.shouldNotBeBlank()
            games shouldHaveSize 1
            games[0].apply {
                id shouldBe "00f6f50c-49d0-4564-99a3-eadbc15b5e00"
                profileLink shouldBe "https://steam.com/my-dota"
                description shouldBe "Looking for teammate for Dota 2"
            }
        }

    }

    // Given User logged in
    // When he refreshes token
    // Then should get access to /me endpoint with new accessToken
    @Test
    @Order(2)
    fun `Should allow access with new token after token refreshing`() {
        val (_, refreshToken) = loginUser(loginRequestDto)

        val (newAccessToken) = refreshToken(refreshToken)
        val response = getUserAccount(newAccessToken)

        response.id.shouldNotBeBlank()
    }

    // Given User logged in
    // When he refreshes token
    // Then response status should be OK
    //
    // When he refreshes token with old token
    // Then response status should be UNAUTHORIZED
    @Test
    @Order(3)
    fun `Should not allow to use refresh token twice`() {
        val (_, refreshToken) = loginUser(loginRequestDto)
        sleepSeconds(1) // need to wait some time, otherwise same token will be generated =(

        refreshToken(refreshToken) // first refresh should be ok

        refreshTokenResultActions(refreshToken)
            .expectStatusIsUnauthorized() // second refresh with same token should fail
    }

    // Given User logged in
    // And accessToken expires
    // When got access to /me endpoint
    // Then response status should be FORBIDDEN
    @Test
    @Order(4)
    fun `Should not allow access with expired token`() {
        val (accessToken) = loginUser(loginRequestDto)

        sleepSeconds(ttl)

        getUserAccountResultActions(accessToken)
            .expectStatusIsForbidden()
    }

    // Given User logged in
    // And he changes nickname
    // And he changes status
    // And he edits Game info
    // When got access to /me endpoint
    // Then should see his updated personal info
    //
    // And he adds new Game
    // And he removes old Game
    // When got access to /me endpoint
    // Then should see only one new Game
    @Test
    @Order(5)
    fun `Should update user account information`() {
        // GIVEN
        val (accessToken) = loginUser(loginRequestDto)

        // WHEN
        changeNickname(accessToken, "new nickname")
        changeStatus(accessToken)
        changeImage(accessToken, "https://res.cloudinary.com/ortube/image/upload/v1622542025/users/defaults/5_rh8tzk.png")
        editGame(
            accessToken = accessToken,
            gameId = "00f6f50c-49d0-4564-99a3-eadbc15b5e00",
            profileLink = "https://steam.com/new-link",
            description = "Looking for best players"
        )

        val userAccount = getUserAccount(accessToken)

        // THEN
        userAccount.apply {
            nickname shouldBe "new nickname"
            openedStatus shouldBe false
            image shouldBe "https://res.cloudinary.com/ortube/image/upload/v1622542025/users/defaults/5_rh8tzk.png"
            games shouldHaveSize 1
            games[0].apply {
                id shouldBe "00f6f50c-49d0-4564-99a3-eadbc15b5e00"
                profileLink shouldBe "https://steam.com/new-link"
                description shouldBe "Looking for best players"
            }
        }

        // WHEN
        addGame(
            accessToken = accessToken,
            gameId = "c2340e65-d4a1-48d5-a677-fd68f46599fa",
            profileLink = "https://my-fortnite.com",
            description = "desc"
        )
        removeGame(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val userAccountAfterAddAndRemoveGame = getUserAccount(accessToken)

        // THEN
        userAccountAfterAddAndRemoveGame.games shouldHaveSize 1
        userAccountAfterAddAndRemoveGame.games[0].apply {
            id shouldBe "c2340e65-d4a1-48d5-a677-fd68f46599fa"
            profileLink shouldBe "https://my-fortnite.com"
            description shouldBe "desc"
        }
    }

    // Given User logged in
    // And he updates his last visit date
    // Then new visit date should be after old visit date
    @Test
    @Order(6)
    fun `Should update lastVisitedDate`() {
        // GIVEN
        val oldVisitedDate = userRepository.findByEmail("user.email@gmail.com")!!.lastVisitedDate
        val (accessToken) = loginUser(loginRequestDto)

        // WHEN
        updateLastVisitedDate(accessToken)

        val newVisitedDate = userRepository.findByEmail("user.email@gmail.com")!!.lastVisitedDate

        // THEN
        newVisitedDate shouldBeAfter oldVisitedDate
    }

    // Given User logged in
    // And he changes password
    // When he tries to log in with old password
    // Then response status should be UNAUTHORIZED
    //
    // When he tries to log in with new password
    // Then response status should be OK
    @Test
    @Order(MAX_VALUE)
    fun `Should update user password`() {
        // GIVEN
        val (accessToken) = loginUser(loginRequestDto)

        // WHEN
        changePassword(accessToken, "Password@123", "NewPassword@432")

        // THEN
        loginUserResultActions(LoginRequestDto("user.email@gmail.com", "Password@123"))
            .expectStatusIsUnauthorized()

        loginUserResultActions(LoginRequestDto("user.email@gmail.com", "NewPassword@432"))
            .expectStatusIsOk()

    }

}
