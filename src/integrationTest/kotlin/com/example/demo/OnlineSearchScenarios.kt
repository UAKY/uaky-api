package com.example.demo

import com.example.demo.dto.LoginRequestDto
import com.example.demo.steps.clearOnlineViews
import com.example.demo.steps.loginUser
import com.example.demo.steps.onlineSearchByGameId
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testhelpers.createOfflineUsersForGame
import com.example.demo.testhelpers.createUser
import com.example.demo.testhelpers.createUsersForGames
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.collections.shouldNotContain
import io.kotest.matchers.collections.shouldNotContainAll
import io.kotest.matchers.collections.shouldNotContainAnyOf
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test

class OnlineSearchScenarios : AbstractIntegrationTest() {

    val loginRequestDto = LoginRequestDto(
        email = "user.email@gmail.com",
        password = "Password@123",
    )

    // Given 17 registered users (8 offline, 8 online, 1 current)
    // And User logged in
    // When he requested online users by gameId (result1)
    // Then it should have 5 online users
    // Also should not contain current User
    // Also should not contain users from result2
    // Also should not contain any of offline users
    //
    // When he requested random users by gameId (result2)
    // Then it should have 3 online users
    // Also should not contain current User
    // Also should not contain users from result1
    // Also should not contain any of offline users
    //
    // When he requested random users by gameId (result3)
    // Then it should have no users
    @Test
    @Order(1)
    fun `Should return online users by game id`() {
        val currentUserId = createUser(
            email = "user.email@gmail.com",
            password = "Password@123",
            gameIds = listOf("00f6f50c-49d0-4564-99a3-eadbc15b5e00")
        )

        val randomUsersCount = 8

        val offlineUserIds = createOfflineUsersForGame("00f6f50c-49d0-4564-99a3-eadbc15b5e00", randomUsersCount)
        createUsersForGames(randomUsersCount, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val (accessToken) = loginUser(loginRequestDto)

        val result1 = onlineSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)
        val result2 = onlineSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)
        val result3 = onlineSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)

        result1.apply {
            content.apply {
                shouldHaveSize(5)
                map { it.id }.apply {
                    shouldNotContain(currentUserId)
                    shouldNotContainAnyOf(offlineUserIds)
                }
                shouldNotContainAnyOf(result2.content)
            }
            pageSize shouldBe 5
        }

        result2.apply {
            content.apply {
                shouldHaveSize(3)
                map { it.id }.apply {
                    shouldNotContain(currentUserId)
                    shouldNotContainAnyOf(offlineUserIds)
                }
                shouldNotContainAll(result1.content)
            }
            pageSize shouldBe 3
        }

        result3.apply {
            content.shouldBeEmpty()
            pageSize shouldBe 0
        }
    }

    // Given User logged in
    // And all online users are viewed
    // And user requests to clear online views
    // When he requested online users by gameId
    // Then result should not be empty
    @Test
    @Order(2)
    fun `Should clear views`() {
        val (accessToken) = loginUser(loginRequestDto)

        clearOnlineViews(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val foundUsers = onlineSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)

        foundUsers.content.shouldNotBeEmpty()
    }

}
