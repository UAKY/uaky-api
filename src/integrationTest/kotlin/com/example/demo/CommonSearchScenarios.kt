package com.example.demo

import com.example.demo.dto.LoginRequestDto
import com.example.demo.steps.clearCommonViews
import com.example.demo.steps.commonSearchByGameId
import com.example.demo.steps.loginUser
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testhelpers.createOfflineUsersForGame
import com.example.demo.testhelpers.createUser
import com.example.demo.testhelpers.createUsersForGames
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.collections.shouldNotContain
import io.kotest.matchers.collections.shouldNotContainAnyOf
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test

class CommonSearchScenarios : AbstractIntegrationTest() {

    private val loginRequestDto = LoginRequestDto(
        email = "user.email@gmail.com",
        password = "Password@123",
    )

    private lateinit var currentUserId: String

    @BeforeAll
    fun insertUsers() {
        val randomOnlineUsersCount = 7
        val randomOfflineUsersCount = 10
        createUsersForGames(randomOnlineUsersCount, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")
        createOfflineUsersForGame("00f6f50c-49d0-4564-99a3-eadbc15b5e00", randomOfflineUsersCount)

        currentUserId = createUser(
            email = "user.email@gmail.com",
            password = "Password@123",
            gameIds = listOf("00f6f50c-49d0-4564-99a3-eadbc15b5e00")
        )

    }

    // Given 18 registered users
    // And User logged in
    // When he requested random users by gameId (result1)
    // Then it should have 5 random users
    // Also should not contain current User
    // Also should not contain users from result2, result3 and result4
    //
    // When he requested random users by gameId (result2)
    // Then it should have 5 random users
    // Also should not contain current User
    // Also should not contain users from result1, result3 and result4
    //
    // When he requested random users by gameId (result3)
    // Then it should have 5 random users
    // Also should not contain current User
    // Also should not contain users from result1, result2 and result4
    //
    // When he requested random users by gameId (result4)
    // Then it should have 2 random users
    // Also should not contain current User
    // Also should not contain users from result1, result3 and result4
    //
    // When he requested random users by gameId (result5)
    // Then it should have no users
    @Test
    @Order(1)
    fun `Should return random users by game id`() {
        val (accessToken) = loginUser(loginRequestDto)

        val result1 = commonSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)
        val result2 = commonSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)
        val result3 = commonSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)
        val result4 = commonSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)
        val result5 = commonSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)

        result1.apply {
            content.apply {
                shouldHaveSize(5)
                map { it.id } shouldNotContain currentUserId
                shouldNotContainAnyOf(result2.content)
                shouldNotContainAnyOf(result3.content)
                shouldNotContainAnyOf(result4.content)
            }
            pageSize shouldBe 5
        }

        result2.apply {
            content.apply {
                shouldHaveSize(5)
                map { it.id } shouldNotContain currentUserId
                shouldNotContainAnyOf(result1.content)
                shouldNotContainAnyOf(result3.content)
                shouldNotContainAnyOf(result4.content)
            }
            pageSize shouldBe 5
        }

        result3.apply {
            content.apply {
                shouldHaveSize(5)
                map { it.id } shouldNotContain currentUserId
                shouldNotContainAnyOf(result1.content)
                shouldNotContainAnyOf(result2.content)
                shouldNotContainAnyOf(result4.content)
            }
            pageSize shouldBe 5
        }

        result4.apply {
            content.apply {
                shouldHaveSize(2)
                map { it.id } shouldNotContain currentUserId
                shouldNotContainAnyOf(result1.content)
                shouldNotContainAnyOf(result2.content)
                shouldNotContainAnyOf(result3.content)
            }
            pageSize shouldBe 2
        }

        result5.apply {
            content.shouldBeEmpty()
            pageSize shouldBe 0
        }

    }

    // Given User logged in
    // And all users are viewed
    // And user requests to clear views
    // When he requested random users by gameId
    // Then result should not be empty
    @Test
    @Order(2)
    fun `Should clear views`() {
        val (accessToken) = loginUser(loginRequestDto)

        clearCommonViews(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val foundUsers = commonSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 5)

        foundUsers.content.shouldNotBeEmpty()
    }

}
