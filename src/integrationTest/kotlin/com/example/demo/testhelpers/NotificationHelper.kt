package com.example.demo.testhelpers

import com.example.demo.dto.UserLikeDto
import com.example.demo.dto.UserLikeEvent
import com.example.demo.persistence.entity.Notification
import com.example.demo.testconfig.AbstractIntegrationTest
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import java.time.LocalDateTime
import java.util.UUID

fun AbstractIntegrationTest.createNotification(userToId: String): Notification {

    val userLikeEvent = UserLikeEvent(
        UserLikeDto(
            id = UUID.randomUUID().toString(),
            nickname = randomAlphabetic(10),
            gameTitle = randomAlphabetic(10),
            userImage = randomAlphabetic(10),
            profileLink = randomAlphabetic(10),
            description = randomAlphabetic(10),
            gameLargeImage = randomAlphabetic(10),
            isMutual = false
        )
    )

    val notification = Notification(userRepository.findById(userToId).get(), userLikeEvent)

    return notificationRepository.save(notification)
}

fun AbstractIntegrationTest.createOldNotification(userToId: String) {
    val createdDate = LocalDateTime.now().minusDays(10)
    val notification = createNotification(userToId)

    jdbcTemplate.execute("UPDATE notifications SET created_date = '$createdDate' WHERE id = '${notification.id}'")
}

fun AbstractIntegrationTest.createOldNotifications(count: Int, userToId: String) {
    generateSequence { createOldNotification(userToId) }.take(count).toList()
}
