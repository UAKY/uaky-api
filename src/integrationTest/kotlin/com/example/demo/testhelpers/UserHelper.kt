package com.example.demo.testhelpers

import com.example.demo.persistence.entity.Role
import com.example.demo.persistence.entity.User
import com.example.demo.testconfig.AbstractIntegrationTest
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.apache.commons.lang3.RandomStringUtils.randomNumeric
import java.time.LocalDateTime

fun AbstractIntegrationTest.createOfflineUser(
    email: String = "${randomAlphabetic(10)}@gmail.com",
    password: String = "${randomAlphabetic(5)}${randomNumeric(5)}",
    role: Role = Role.ROLE_USER,
    gameIds: List<String> = listOf("00f6f50c-49d0-4564-99a3-eadbc15b5e00"),
): String {
    return createUser(email, password, role, gameIds, LocalDateTime.now().minusMinutes(10))
}

fun AbstractIntegrationTest.createUser(
    email: String = "${randomAlphabetic(10)}@gmail.com",
    password: String = "${randomAlphabetic(5)}${randomNumeric(5)}",
    role: Role = Role.ROLE_USER,
    gameIds: List<String> = listOf("00f6f50c-49d0-4564-99a3-eadbc15b5e00"),
    lastVisitedDate: LocalDateTime = LocalDateTime.now(),
): String {

    val user = User(
        email = email,
        password = passwordEncoder.encode(password),
        nickname = randomAlphabetic(10),
        openedStatus = true,
        roles = setOf(role),
        lastVisitedDate = lastVisitedDate,
        image = "https://res.cloudinary.com/ortube/image/upload/v1622542025/users/defaults/8_ob8jkh.png"
    ).apply {
        gameIds.forEach {
            addGame(
                profileLink = randomAlphabetic(10),
                description = randomAlphabetic(10),
                gameId = it
            )
        }
    }

    return userRepository.save(user).id!!
}

fun AbstractIntegrationTest.createOfflineUsersForGame(gameId: String, count: Int): List<String> {
    return generateSequence { createOfflineUser(gameIds = listOf(gameId)) }.take(count).toList()
}

fun AbstractIntegrationTest.createUsersForGames(count: Int, vararg gameIds: String): List<String> {
    return generateSequence { createUser(gameIds = gameIds.toList()) }.take(count).toList()
}
