package com.example.demo.testconfig

import org.springframework.stereotype.Component
import java.sql.Connection
import java.sql.ResultSet
import javax.sql.DataSource

@Component
class DatabaseCleaner(dataSource: DataSource) {

    private val connection = dataSource.connection
    private val excludeTables = listOf(
        "databasechangelog",
        "databasechangeloglock",
        "games",
    )
    private val tablesToClear = connection.getTables().filterNot(excludeTables::contains)

    fun clear() {
        val resetStatement = connection.createStatement().apply {
            addBatch("SET session_replication_role = 'replica';") // disable foreign key checks
            tablesToClear.forEach { addBatch("DELETE FROM $it") }
            addBatch("SET session_replication_role = 'origin';") // enable foreign key checks
        }
        resetStatement.executeBatch()
    }

    private fun Connection.getTables(): List<String> {
        return metaData.getTables(connection.catalog, null, null, arrayOf("TABLE"))
            .tablesIterator()
            .asSequence()
            .toList()
    }

    private fun ResultSet.tablesIterator(): AbstractIterator<String> = object : AbstractIterator<String>() {
        override fun computeNext() {
            if (this@tablesIterator.next()) {
                setNext(getString("TABLE_NAME"))
            } else {
                done()
            }
        }
    }

}
