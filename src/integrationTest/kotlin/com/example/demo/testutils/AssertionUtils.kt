package com.example.demo.testutils

import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

fun ResultActions.expectStatusIsOk() = andExpect(status().isOk)
fun ResultActions.expectStatusIsCreated() = andExpect(status().isCreated)
fun ResultActions.expectStatusIsNoContent() = andExpect(status().isNoContent)
fun ResultActions.expectStatusIsUnauthorized() = andExpect(status().isUnauthorized)
fun ResultActions.expectStatusIsForbidden() = andExpect(status().isForbidden)
