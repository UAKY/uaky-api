package com.example.demo.testutils

import com.example.demo.constant.AUTH_HEADER
import com.example.demo.constant.AUTH_HEADER_PREFIX
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.context.ApplicationContext
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder

@Component
@SuppressWarnings("UnusedPrivateClass")
private class TestUtils(
    applicationContext: ApplicationContext
) {
    init {
        context = applicationContext
    }
}

private lateinit var context: ApplicationContext
private val objectMapper by lazy { context.getBean(ObjectMapper::class.java) }

fun MockHttpServletRequestBuilder.withToken(token: String) =
    header(AUTH_HEADER, "$AUTH_HEADER_PREFIX$token")

fun MockHttpServletRequestBuilder.contentJson(requestBody: Any) =
    contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(requestBody))

inline fun <reified T> ResultActions.extractResponse(): T =
    objectMapper().readValue(andReturn().response.contentAsString)

fun objectMapper() = objectMapper

fun sleepSeconds(seconds: Int) =
    Thread.sleep((seconds * 1000L))
