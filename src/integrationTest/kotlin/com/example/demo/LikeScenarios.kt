package com.example.demo

import com.example.demo.dto.LoginRequestDto
import com.example.demo.steps.commonSearchByGameId
import com.example.demo.steps.getLikes
import com.example.demo.steps.getNotifications
import com.example.demo.steps.likeUser
import com.example.demo.steps.loginUser
import com.example.demo.testconfig.AbstractIntegrationTest
import com.example.demo.testhelpers.createOldNotifications
import com.example.demo.testhelpers.createUser
import com.example.demo.testhelpers.createUsersForGames
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.collections.shouldNotContainAnyOf
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test

class LikeScenarios : AbstractIntegrationTest() {

    val currentUserLoginDto = LoginRequestDto(
        email = "user0@email.com",
        password = "Password@123",
    )

    lateinit var currentUserId: String
    lateinit var userFromLikeIds: List<String>

    // Given 6 registered users
    // And 5 of them liked first one
    // When first user got likes
    // Then he should see 5 likes from 5 other users
    @Test
    @Order(1)
    fun `Should get likes`() {
        currentUserId = createUser(
            email = "user0@email.com",
            password = "Password@123",
            gameIds = listOf("00f6f50c-49d0-4564-99a3-eadbc15b5e00", "c2340e65-d4a1-48d5-a677-fd68f46599fa")
        )

        userFromLikeIds = listOf(
            createUser(email = "user1@email.com", password = "Password@123", gameIds = listOf("00f6f50c-49d0-4564-99a3-eadbc15b5e00")),
            createUser(email = "user2@email.com", password = "Password@123", gameIds = listOf("00f6f50c-49d0-4564-99a3-eadbc15b5e00")),
            createUser(email = "user3@email.com", password = "Password@123", gameIds = listOf("00f6f50c-49d0-4564-99a3-eadbc15b5e00")),
            createUser(email = "user4@email.com", password = "Password@123", gameIds = listOf("c2340e65-d4a1-48d5-a677-fd68f46599fa")),
            createUser(email = "user5@email.com", password = "Password@123", gameIds = listOf("c2340e65-d4a1-48d5-a677-fd68f46599fa"))
        )

        val (accessToken1) = loginUser("user1@email.com", "Password@123")
        likeUser(accessToken1, currentUserId, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val (accessToken2) = loginUser("user2@email.com", "Password@123")
        likeUser(accessToken2, currentUserId, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val (accessToken3) = loginUser("user3@email.com", "Password@123")
        likeUser(accessToken3, currentUserId, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val (accessToken4) = loginUser("user4@email.com", "Password@123")
        likeUser(accessToken4, currentUserId, "c2340e65-d4a1-48d5-a677-fd68f46599fa")

        val (accessToken5) = loginUser("user5@email.com", "Password@123")
        likeUser(accessToken5, currentUserId, "c2340e65-d4a1-48d5-a677-fd68f46599fa")

        val (accessToken0) = loginUser(currentUserLoginDto)
        val likes = getLikes(accessToken0)

        likes.apply {
            shouldHaveSize(5)
            map { it.id } shouldContainAll userFromLikeIds
        }
    }

    // Given user has 5 new notifications (likes)
    // And 9 old ones
    // When he requests list of notifications
    // Then he should see just new ones
    @Test
    @Order(2)
    fun `Should get list of new notifications`() {
        createOldNotifications(count = 9, userToId = currentUserId)

        val (accessToken0) = loginUser(currentUserLoginDto)

        val notifications = getNotifications(accessToken0)

        notifications.content shouldHaveSize 5
        notifications.content.map { it.content!!.id!! } shouldContainAll userFromLikeIds
    }

    // Given 17 registered users
    // And first one liked 4 other users
    // When he requested random users by gameId
    // Then result should contain all not liked users
    // Also result should not contain any of liked users
    @Test
    @Order(3)
    fun `Should not search liked users`() {

        val usersToLike = createUsersForGames(4, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")
        val notLikedUsers = createUsersForGames(12, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val (accessToken) = loginUser(currentUserLoginDto)

        usersToLike.forEach {
            likeUser(accessToken, it, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")
        }

        val result = commonSearchByGameId(accessToken, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", 100)

        result.content.map { it.id }.apply {
            shouldContainAll(notLikedUsers)
            shouldNotContainAnyOf(usersToLike)
        }

    }

    // Given user with 2 games
    // When I like that user in one game
    // Then I should be able to like him in another
    @Test
    @Order(4)
    fun `Should search liked users but in different games`() {
        val (accessToken) = loginUser(currentUserLoginDto)

        val userId = createUsersForGames(1, "00f6f50c-49d0-4564-99a3-eadbc15b5e00", "c2340e65-d4a1-48d5-a677-fd68f46599fa").first()

        likeUser(accessToken, userId, "00f6f50c-49d0-4564-99a3-eadbc15b5e00")

        val result = commonSearchByGameId(accessToken, "c2340e65-d4a1-48d5-a677-fd68f46599fa", 10)

        result.content.map { it.id }.shouldContain(userId)
    }

}
