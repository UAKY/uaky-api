FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /app
COPY build/libs/*.jar /app/uaku.jar

EXPOSE 8079

CMD java $JAVA_OPTS -jar /app/uaku.jar
